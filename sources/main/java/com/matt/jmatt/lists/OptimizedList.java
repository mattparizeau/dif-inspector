package com.matt.jmatt.lists;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * An optimized list using indices
 * 
 * @author Matthieu Parizeau
 *
 * @param <T> the object type of this list
 */
public class OptimizedList<T> implements Iterable<T>
{
	/**
	 * List of objects
	 */
	protected List<T> list;
	
	/**
	 * List of indices
	 */
	protected List<Integer> indices;
	
	/**
	 * Does the list need to be optimized?
	 */
	protected boolean dirty;
	
	/**
	 * Initializes an OptimizedList
	 */
	public OptimizedList()
	{
		// Create the underlying lists
		this.list = new ArrayList<T>();
		this.indices = new ArrayList<Integer>();
		
		// The list does not require optimization as there are no objects, mark it as such!
		this.dirty = false;
	}
	
	/**
	 * Initializes an OptimizedList with an array and indices
	 * @param array array of type T
	 * @param indices array of indices
	 */
	public OptimizedList(T[] array, int[] indices)
	{
		this.list = new ArrayList<T>();
		
		// Add all the objects to the underlying list
		for (T obj : array)
			this.list.add(obj);
		
		this.indices = new ArrayList<Integer>();
		
		// Add all the indices to the underlying list
		for (int index : indices)
			this.indices.add(index);
	}
	
	/**
	 * Initializes an OptimizedList with a list and indices
	 * @param list list of type T
	 * @param indices list of indices
	 */
	public OptimizedList(List<T> list, List<Integer> indices)
	{
		this.list = list;
		this.indices = indices;
	}
	
	/**
	 * Adds an object to the list
	 * @param obj object to add
	 */
	public void add(T obj)
	{
		// Add the object
		this.list.add(obj);
		
		// Add the index of the object to the indices list
		this.indices.add(this.list.size() - 1);
		
		// The list needs to be optimized, mark it as such!
		this.dirty = true;
	}
	
	/**
	 * Gets the value using an index to the indices list
	 * @param index for the indices list
	 * @return value
	 */
	public T getObject(int index)
	{
		// Optimize the list first!
		this.optimize();
		
		// Get the index from the indices array
		int i = this.indices.get(index);
		
		// Return the value at that index
		return this.list.get(i);
	}
	
	/**
	 * Gets the value directly, ie: without using the indices list
	 * @param index of the object
	 * @return object
	 */
	public T getObjectDirect(int index)
	{
		// Return the value at the index
		return this.list.get(index);
	}
	
	/**
	 * Returns the last object
	 * @return last object
	 */
	public T lastObject()
	{
		// Optimize the list first!
		this.optimize();
		
		int index = this.lastIndex();
		if (index == -1)
			return null;
		return this.list.get(index);
	}
	
	/**
	 * Returns the last index
	 * @return last index
	 */
	public int lastIndex()
	{
		// Optimize the list first!
		this.optimize();
		
		if (this.indices.size() == 0)
			return -1;
		return this.indices.get(this.indices.size() - 1);
	}
	
	/**
	 * Gets the index from the indices list
	 * @param index for the indices list
	 * @return indices index
	 */
	public int getIndex(int index)
	{
		// Optimize the list first!
		this.optimize();
		
		// Get the index from the indices array
		return this.indices.get(index);
	}
	
	/**
	 * Gets the indices array index of an object or -1 if the object does not exist
	 * @param obj object to search for
	 * @return indices index
	 */
	public int indexOfIndex(T obj)
	{
		// Optimize the list first!
		this.optimize();
		
		// Loop through the indices list
		for (int i = 0; i < this.indices.size(); i++)
		{
			int index = this.indices.get(i);
			
			// Get the object at the index from the indices list
			T object = this.list.get(index);
			
			// If the object is equal to the object passed in, we found the index!
			if (obj == null ? object == null : obj.equals(object))
				return i;
		}
		
		// If the object could not be found, return -1
		return -1;
	}
	
	/**
	 * Gets the index of an object or -1 if the object does not exist
	 * @param obj object to search for
	 * @return object index
	 */
	public int indexOfObject(T obj)
	{
		// Optimize the list first!
		this.optimize();
		
		// Loop through the indices list
		for (int i = 0; i < this.indices.size(); i++)
		{
			int index = this.indices.get(i);
			
			// Get the object at the index from the indices list
			T object = this.list.get(index);
			
			// If the object is equal to the object passed in, we found the index!
			if (obj == null ? object == null : obj.equals(object))
				return index;
		}
		
		// If the object could not be found, return -1
		return -1;
	}
	
	/**
	 * Checks if the list contains the specified object
	 * @param obj object to search for
	 * @return true if the list contains the object or false if it doesn't
	 */
	public boolean contains(T obj)
	{
		// Pass the call to the underlying object list
		return this.list.contains(obj);
	}
	
	/**
	 * Optimizes the array
	 */
	protected void optimize()
	{
		// Only optimize if necessary
		if (!this.dirty)
			return;
		
		// Create new lists
		List<T> newList = new ArrayList<T>();
		List<Integer> newIndices = new ArrayList<Integer>();
		
		// Loop through the indices list
		for (int i = 0; i < this.indices.size(); i++)
		{
			// If the object list already contains the current object
			// Add the index of it to the indices list
			if (newList.contains(this.list.get(this.indices.get(i))))
			{
				newIndices.add(newList.indexOf(this.list.get(this.indices.get(i))));
				continue;
			}
			
			// Otherwise add the object and the index
			newList.add(this.list.get(this.indices.get(i)));
			newIndices.add(newList.size() - 1);
		}
		
		// Set the global lists to the new ones
		this.list = newList;
		this.indices = newIndices;
		
		// Mark it such that this list no longer needs optimization
		this.dirty = false;
	}
	
	/**
	 * Returns the number of objects
	 * @return number of objects
	 */
	public int sizeObjects()
	{
		// Optimize the list first!
		this.optimize();
		
		// Pass the call to the underlying object list
		return this.list.size();
	}
	
	/**
	 * Returns the number of indices
	 * @return number of indices
	 */
	public int sizeIndices()
	{
		// Optimize the list first!
		this.optimize();
		
		// Pass the call to the underlying indices list
		return this.indices.size();
	}
	
	/**
	 * Gets the underlying list of objects
	 * @return underlying list of objects
	 */
	public List<T> getObjects()
	{
		// Optimize the list before returning it
		this.optimize();
		
		return this.list;
	}
	
	/**
	 * Gets the underlying list of indices
	 * @return underlying list of indices
	 */
	public List<Integer> getIndices()
	{
		// Optimize the list before returning it
		this.optimize();
		
		return this.indices;
	}
	
	/**
	 * Converts this list to a standard unoptimized list
	 * @return standard list
	 */
	public List<T> toStandardList()
	{
		// Create a standard list
		List<T> result = new ArrayList<T>();
		
		// Loop through the indices
		for (int i = 0; i < this.sizeIndices(); i++)
		{
			// Get the index
			int index = this.getIndex(i);
			
			// Get the object at said index
			T object = this.getObject(index);
			
			// Add the object to the list
			result.add(object);
		}
		
		// Return the standard list
		return result;
	}

	@Override
	public Iterator<T> iterator()
	{
		return new Iterator<T>() {
			
			/**
			 * Current index
			 */
			protected int index;

			@Override
			public T next()
			{
				// Get the index of the object
				int i = OptimizedList.this.getIndex(this.index);
				
				// Get the object at said index
				T obj = OptimizedList.this.getObject(i);
				
				// Increment the current indices index
				this.index++;
				
				// Return the object
				return obj;
			}
			
			@Override
			public boolean hasNext()
			{
				// There are more elements if the current index is less the the number of indices
				return this.index < OptimizedList.this.sizeIndices();
			}
		};
	}
}
