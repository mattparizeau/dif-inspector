package com.matt.difinspector.math;

public class QuatF
{
	protected float x, y, z, w;
	
	public QuatF(float x, float y, float z, float w)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}
	
	public QuatF(AngAxisF angAxis)
	{
		this(angAxis.getAxis(), angAxis.getAngle());
	}
	
	public QuatF(Point3F axis, float angle)
	{
		float sinHalfAngle = (float)Math.sin(angle / 2);
		float cosHalfAngle = (float)Math.cos(angle / 2);
		
		this.x = axis.x * sinHalfAngle;
		this.y = axis.y * sinHalfAngle;
		this.z = axis.z * sinHalfAngle;
		this.w = cosHalfAngle;
	}
	
	public QuatF(Point3F euler)
	{
		float c1 = (float)Math.cos(euler.y * 0.5f);
		float s1 = (float)Math.sin(euler.y * 0.5f);
		float c2 = (float)Math.cos(euler.z * 0.5f);
		float s2 = (float)Math.sin(euler.z * 0.5f);
		float c3 = (float)Math.cos(euler.x * 0.5f);
		float s3 = (float)Math.sin(euler.x * 0.5f);
		
		float c1c2 = c1 * c2;
		float s1s2 = s1 * s2;
		
		this.w = c1c2 * c3 - s1s2 * s3;
		this.x = c1c2 * s3 + s1s2 * c3;
		this.y = s1 * c2 * c3 + c1 * s2 * s3;
		this.z = c1 * s2 * c3 - s1 * c2 * s3;
	}
	
	public QuatF(MatrixF rot)
	{
		float trace = rot.get(0, 0) + rot.get(1, 1) + rot.get(2, 2);
		
		if (trace > 0)
		{
			float s = 0.5f / (float)Math.sqrt(trace + 1.0f);
			this.w = 0.25f / s;
			this.x = (rot.get(1, 2) - rot.get(2, 1)) * s;
			this.y = (rot.get(2, 0) - rot.get(0, 2)) * s;
			this.z = (rot.get(0, 1) - rot.get(1, 0)) * s;
		} else
		{
			if (rot.get(0, 0) > rot.get(1, 1) && rot.get(0, 0) > rot.get(2, 2))
			{
				float s = 2.0f * (float)Math.sqrt(1.0f + rot.get(0, 0) - rot.get(1, 1) - rot.get(2, 2));
				this.w = (rot.get(1, 2) - rot.get(2, 1)) / s;
				this.x = 0.25f * s;
				this.y = (rot.get(1, 0) + rot.get(0, 1)) / s;
				this.z = (rot.get(2, 0) + rot.get(0, 2)) / s;
			} else if (rot.get(1, 1) > rot.get(2, 2))
			{
				float s = 2.0f * (float)Math.sqrt(1.0f + rot.get(1, 1) - rot.get(0, 0) - rot.get(2, 2));
				this.w = (rot.get(2, 0) - rot.get(0, 2)) / s;
				this.x = (rot.get(1, 0) + rot.get(0, 1)) / s;
				this.y = 0.25f * s;
				this.z = (rot.get(2, 1) + rot.get(1, 2)) / s;
			} else {
				float s = 2.0f * (float)Math.sqrt(1.0f + rot.get(2, 2) - rot.get(0, 0) - rot.get(1, 1));
				this.w = (rot.get(0, 1) - rot.get(1, 0)) / s;
				this.x = (rot.get(2, 0) + rot.get(0, 2)) / s;
				this.y = (rot.get(1, 2) + rot.get(2, 1)) / s;
				this.z = 0.25f * s;
			}
		}
		
		float length = (float)Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w);
		this.x /= length;
		this.y /= length;
		this.z /= length;
		this.w /= length;
	}
	
	public float length()
	{
		return (float)Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w);
	}
	
	public void normalize()
	{
		float length = this.length();
		
		this.x /= length;
		this.y /= length;
		this.z /= length;
		this.w /= length;
	}
	
	public QuatF normalized()
	{
		float length = this.length();
		
		return new QuatF(this.x / length, this.y / length, this.z / length, this.w / length);
	}
	
	public QuatF conjugate()
	{
		return new QuatF(-this.x, -this.y, -this.z, this.w);
	}
	
	public QuatF mul(float r)
	{
		return new QuatF(this.x * r, this.y * r, this.z * r, this.w * r);
	}
	
	public QuatF mul(QuatF r)
	{
		float w = this.w * r.w - this.x * r.x - this.y * r.y - this.z * r.z;
		float x = this.x * r.w + this.w * r.x + this.y * r.z - this.z * r.y;
		float y = this.y * r.w + this.w * r.y + this.z * r.x - this.x * r.z;
		float z = this.z * r.w + this.w * r.z + this.x * r.y - this.y * r.x;
		
		return new QuatF(x, y, z, w);
	}
	
	public QuatF mul(Point3F r)
	{
		float w = -this.x * r.x - this.y * r.y - this.z * r.z;
		float x = this.w * r.x + this.y * r.z - this.z * r.y;
		float y = this.w * r.y + this.z * r.x - this.x * r.z;
		float z = this.w * r.z + this.x * r.y - this.y * r.x;
		
		return new QuatF(x, y, z, w);
	}
	
	public QuatF sub(QuatF r)
	{
		return new QuatF(this.x - r.x, this.y - r.y, this.z - r.z, this.w - r.w);
	}
	
	public QuatF add(QuatF r)
	{
		return new QuatF(this.x + r.x, this.y + r.y, this.z + r.z, this.w + r.w);
	}
	
	public MatrixF toRotationMatrix()
	{
		Point3F forward = new Point3F(2.0f * (this.x * this.z - this.w * this.y), 2.0f * (this.y * this.z + this.w * this.x), 1.0f - 2.0f * (this.x * this.x + this.y * this.y));
		Point3F up = new Point3F(2.0f * (this.x * this.y + this.w * this.z), 1.0f - 2.0f * (this.x * this.x + this.z * this.z), 2.0f * (this.y * this.z - this.w * this.x));
		Point3F right = new Point3F(1.0f - 2.0f * (this.y * this.y + this.z * this.z), 2.0f * (this.x * this.y - this.w * this.z), 2.0f * (this.x * this.z + this.w * this.y));
		
		return new MatrixF().initRotation(forward, up, right);
	}
	
	public float dot(QuatF r)
	{
		return this.x * r.x + this.y * r.y + this.z * r.z + this.w * r.w;
	}
	
	public QuatF nLerp(QuatF dest, float lerpFactor, boolean shortest)
	{
		QuatF correctedDest = dest;
		
		if (shortest && this.dot(dest) < 0)
			correctedDest = new QuatF(-dest.x, -dest.y, -dest.z, -dest.w);
		
		return correctedDest.sub(this).mul(lerpFactor).add(this).normalized();
	}
	
	public QuatF sLerp(QuatF dest, float lerpFactor, boolean shortest)
	{
		final float EPSILON = 1e3f;
		
		float cos = this.dot(dest);
		QuatF correctedDest = dest;
		
		if (shortest && cos < 0)
		{
			cos = -cos;
			correctedDest = new QuatF(-dest.x, -dest.y, -dest.z, -dest.w);
		}
		
		if (Math.abs(cos) >= 1 - EPSILON)
			return nLerp(correctedDest, lerpFactor, false);
		
		float sin = (float)Math.sqrt(1.0f - cos * cos);
		float angle = (float)Math.atan2(sin, cos);
		float invSin = 1.0f / sin;
		
		float srcFactor = (float)Math.sin((1.0f - lerpFactor) * angle) * invSin;
		float destFactor = (float)Math.sin((lerpFactor) * angle) * invSin;
		
		return this.mul(srcFactor).add(correctedDest.mul(destFactor));
	}
	
	public Point3F getForward()
	{
		return new Point3F(0, 0, 1).rotate(this);
	}
	
	public Point3F getBack()
	{
		return new Point3F(0, 0, -1).rotate(this);
	}
	
	public Point3F getUp()
	{
		return new Point3F(0, 1, 0).rotate(this);
	}
	
	public Point3F getDown()
	{
		return new Point3F(0, -1, 0).rotate(this);
	}
	
	public Point3F getRight()
	{
		return new Point3F(1, 0, 0).rotate(this);
	}
	
	public Point3F getLeft()
	{
		return new Point3F(-1, 0, 0).rotate(this);
	}
	
	public QuatF set(float x, float y, float z, float w)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
		
		return this;
	}
	
	public QuatF set(QuatF r)
	{
		return this.set(r.x, r.y, r.z, r.w);
	}
	
	public MatrixF toMatrix()
	{
		
		if (x * x + y * y + z * z < 10E-20f)
		{
			MatrixF mat = new MatrixF();
			mat.initIdentity();
			return mat;
		} else {
			float[][] m = new float[4][4];
			float xs = x * 2.0f;
			float ys = y * 2.0f;
			float zs = z * 2.0f;
			float wx = w * xs;
			float wy = w * ys;
			float wz = w * zs;
			float xx = x * xs;
			float xy = x * ys;
			float xz = x * zs;
			float yy = y * ys;
			float yz = y * zs;
			float zz = z * zs;
			m[0][0] = 1.0f - (yy + zz);
			m[1][0] = xy - wz;
			m[2][0] = xz + wy;
			m[3][0] = 0.0f;
			m[0][1] = xy + wz;
			m[1][1] = 1.0f - (x + zz);
			m[2][1] = yz - wx;
			m[3][1] = 0.0f;
			m[0][2] = xz - wy;
			m[1][2] = yz + wx;
			m[2][2] = 1.0f - (xx + yy);
			m[3][2] = 0.0f;
			
			m[0][3] = 0.0f;
			m[1][3] = 0.0f;
			m[2][3] = 0.0f;
			m[3][3] = 1.0f;
			
			return new MatrixF(m);
		}
	}
	
	public Point3F toEuler()
	{
		MatrixF mat = this.toRotationMatrix();
		return mat.toEuler();
	}
	
	public float getX()
	{
		return this.x;
	}
	
	public void setX(float x)
	{
		this.x = x;
	}
	
	public float getY()
	{
		return this.y;
	}
	
	public void setY(float y)
	{
		this.y = y;
	}
	
	public float getZ()
	{
		return this.z;
	}
	
	public void setZ(float z)
	{
		this.z = z;
	}
	
	public float getW()
	{
		return this.w;
	}
	
	public void setW(float w)
	{
		this.w = w;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof QuatF))
			return false;
		
		QuatF other = (QuatF)o;
		
		return this.x == other.x && this.y == other.y && this.z == other.z && this.w == other.w;
	}
}
