package com.matt.difinspector.math;

public class MatrixF
{
	private float[][] m;
	
	public MatrixF()
	{
		this.m = new float[4][4];
	}
	
	public MatrixF(float[][] m)
	{
		this.m = m;
	}
	
	public MatrixF initIdentity()
	{
		m[0][0] = 1;	m[0][1] = 0;	m[0][2] = 0;	m[0][3] = 0;
		m[1][0] = 0;	m[1][1] = 1;	m[1][2] = 0;	m[1][3] = 0;
		m[2][0] = 0;	m[2][1] = 0;	m[2][2] = 1;	m[2][3] = 0;
		m[3][0] = 0;	m[3][1] = 0;	m[3][2] = 0;	m[3][3] = 1;
		
		return this;
	}
	
	public MatrixF initTranslation(float x, float y, float z)
	{
		m[0][0] = 1;	m[0][1] = 0;	m[0][2] = 0;	m[0][3] = x;
		m[1][0] = 0;	m[1][1] = 1;	m[1][2] = 0;	m[1][3] = y;
		m[2][0] = 0;	m[2][1] = 0;	m[2][2] = 1;	m[2][3] = z;
		m[3][0] = 0;	m[3][1] = 0;	m[3][2] = 0;	m[3][3] = 1;
		
		return this;
	}
	
	public MatrixF initRotation(float x, float y, float z)
	{
		MatrixF rx = new MatrixF();
		MatrixF ry = new MatrixF();
		MatrixF rz = new MatrixF();
		
		x = (float)Math.toRadians(x);
		y = (float)Math.toRadians(y);
		z = (float)Math.toRadians(z);
		
		rz.m[0][0] = (float)Math.cos(z);	rz.m[0][1] = -(float)Math.sin(z);	rz.m[0][2] = 0;	rz.m[0][3] = 0;
		rz.m[1][0] = (float)Math.sin(z);	rz.m[1][1] = (float)Math.cos(z); 	rz.m[1][2] = 0;	rz.m[1][3] = 0;
		rz.m[2][0] = 0;						rz.m[2][1] = 0;						rz.m[2][2] = 1;	rz.m[2][3] = 0;
		rz.m[3][0] = 0;						rz.m[3][1] = 0;						rz.m[3][2] = 0;	rz.m[3][3] = 1;
		
		rx.m[0][0] = 1;						rx.m[0][1] = 0;						rx.m[0][2] = 0;	rx.m[0][3] = 0;
		rx.m[1][0] = 0;						rx.m[1][1] = (float)Math.cos(x);	rx.m[1][2] = -(float)Math.sin(x);	rx.m[1][3] = 0;
		rx.m[2][0] = 0;						rx.m[2][1] = (float)Math.sin(x);	rx.m[2][2] = (float)Math.cos(x);	rx.m[2][3] = 0;
		rx.m[3][0] = 0;						rx.m[3][1] = 0;						rx.m[3][2] = 0;	rx.m[3][3] = 1;
		
		ry.m[0][0] = (float)Math.cos(y);	ry.m[0][1] = 0;						ry.m[0][2] = -(float)Math.sin(y);	ry.m[0][3] = 0;
		ry.m[1][0] = 0;						ry.m[1][1] = 1;						ry.m[1][2] = 0;	ry.m[1][3] = 0;
		ry.m[2][0] = (float)Math.sin(y);	ry.m[2][1] = 0;						ry.m[2][2] = (float)Math.cos(y);	ry.m[2][3] = 0;
		ry.m[3][0] = 0;						ry.m[3][1] = 0;						ry.m[3][2] = 0;	ry.m[3][3] = 1;
		
		m = rz.mul(ry.mul(rx)).getM();
		
		return this;
	}
	
	public MatrixF initScale(float x, float y, float z)
	{
		m[0][0] = x;	m[0][1] = 0;	m[0][2] = 0;	m[0][3] = 0;
		m[1][0] = 0;	m[1][1] = y;	m[1][2] = 0;	m[1][3] = 0;
		m[2][0] = 0;	m[2][1] = 0;	m[2][2] = z;	m[2][3] = 0;
		m[3][0] = 0;	m[3][1] = 0;	m[3][2] = 0;	m[3][3] = 1;
		
		return this;
	}
	
	public MatrixF initPerspective(float fov, float aspectRatio, float zNear, float zFar)
	{
		float tanHalfFOV = (float)Math.tan(fov / 2);
		float zRange = zNear - zFar;
		
		m[0][0] = 1.0f / (tanHalfFOV * aspectRatio);	m[0][1] = 0;	m[0][2] = 0;	m[0][3] = 0;
		m[1][0] = 0;									m[1][1] = 1.0f / tanHalfFOV;	m[1][2] = 0;	m[1][3] = 0;
		m[2][0] = 0;									m[2][1] = 0;	m[2][2] = (-zNear - zFar) / zRange;	m[2][3] = 2 * zFar * zNear / zRange;
		m[3][0] = 0;									m[3][1] = 0;	m[3][2] = 1;	m[3][3] = 0;
		
		return this;
	}
	
	public MatrixF initOrthographic(float left, float right, float bottom, float top, float near, float far)
	{
		float width = right - left;
		float height = top - bottom;
		float depth = far - near;
		
		m[0][0] = 2 / width;	m[0][1] = 0;	m[0][2] = 0;	m[0][3] = -(right + left) / width;
		m[1][0] = 0;			m[1][1] = 2 / height;	m[1][2] = 0;	m[1][3] = -(top + bottom) / height;
		m[2][0] = 0;			m[2][1] = 0;			m[2][2] = -2 / depth; m[2][3] = -(far + near) / depth;
		m[3][0] = 0;			m[3][1] = 0;			m[3][2] = 0;	m[3][3] = 1;
		
		return this;
	}
	
	public MatrixF initRotation(Point3F forward, Point3F up)
	{
		Point3F f = forward.normalized();
		Point3F r = up.normalized();
		r = r.cross(f);
		
		Point3F u = f.cross(r);
		
		return initRotation(f, u, r);
	}
	
	public MatrixF initRotation(Point3F forward, Point3F up, Point3F right)
	{
		Point3F f = forward;
		Point3F r = right;
		Point3F u = up;
		
		m[0][0] = r.x;	m[0][1] = r.y;	m[0][2] = r.z;	m[0][3] = 0;
		m[1][0] = u.x;	m[1][1] = u.y;	m[1][2] = u.z;	m[1][3] = 0;
		m[2][0] = f.x;	m[2][1] = f.y;	m[2][2] = f.z;	m[2][3] = 0;
		m[3][0] = 0;	m[3][1] = 0;	m[3][2] = 0;	m[3][3] = 1;
		
		return this;
	}
	
	public Point3F transform(Point3F r)
	{
		return new Point3F(m[0][0]*r.x + m[0][1]*r.y + m[0][2]*r.z + m[0][3],
						   m[1][0]*r.x + m[1][1]*r.y + m[1][2]*r.z + m[1][3],
						   m[2][0]*r.x + m[2][1]*r.y + m[2][2]*r.z + m[2][3]);
	}
	
	public MatrixF mul(MatrixF r)
	{
		MatrixF res = new MatrixF();
		
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				res.set(i, j, m[i][0] * r.get(0, j) +
							  m[i][1] * r.get(1, j) +
							  m[i][2] * r.get(2, j) +
							  m[i][3] * r.get(3, j));
			}
		}
		
		return res;
	}
	
	public Point3F toEuler()
	{
		Point3F r = new Point3F();
		r.x = (float)Math.asin(m[2][1]);
		
		if (Math.cos(r.x) != 0.0f)
		{
			r.y = (float)Math.atan2(-m[2][0], m[2][2]);
			r.z = (float)Math.atan2(-m[0][1], m[1][1]);
		} else {
			r.y = 0.0f;
			r.z = (float)Math.atan2(m[1][0], m[0][0]);
		}
		
		return r;
	}
	
	public float[][] getM()
	{
		return this.m;
	}
	
	public float get(int x, int y)
	{
		return this.m[x][y];
	}
	
	public void setM(float[][] m)
	{
		this.m = m;
	}
	
	public void set(int x, int y, float value)
	{
		this.m[x][y] = value;
	}
	
	public void swapRows(int row1, int row2)
	{
		float p1x = this.m[row1][0];
		float p1y = this.m[row1][1];
		float p1z = this.m[row1][2];
		float p1w = this.m[row1][3];
		
		float p2x = this.m[row2][0];
		float p2y = this.m[row2][1];
		float p2z = this.m[row2][2];
		float p2w = this.m[row2][3];
		
		this.m[row1][0] = p2x;
		this.m[row1][1] = p2y;
		this.m[row1][2] = p2z;
		this.m[row1][3] = p2w;
		
		this.m[row2][0] = p1x;
		this.m[row2][1] = p1y;
		this.m[row2][2] = p1z;
		this.m[row2][3] = p1w;
	}
	
	public void addRow(int destRow, int srcRow, float factor)
	{
		this.m[destRow][0] += this.m[srcRow][0] * factor;
		this.m[destRow][1] += this.m[srcRow][1] * factor;
		this.m[destRow][2] += this.m[srcRow][2] * factor;
		this.m[destRow][3] += this.m[srcRow][3] * factor;
	}
	
	public void scaleRow(int row, float scale)
	{
		this.m[row][0] *= scale;
		this.m[row][1] *= scale;
		this.m[row][2] *= scale;
		this.m[row][3] *= scale;
	}
	
	public MatrixF copy()
	{
		return new MatrixF(m.clone());
	}
	
	/*@Override
	public String toString()
	{
		return "(MatrixF){m: " + Util.getArrayString(m) + "}";
	}*/
}
