package com.matt.difinspector.math;

public class Transform
{
	protected Transform parent;
	protected MatrixF parentMatrix;
	
	protected Point3F position;
	protected QuatF rotation;
	protected Point3F scale;
	
	protected Point3F oldPosition;
	protected QuatF oldRotation;
	protected Point3F oldScale;
	
	public Transform(Point3F position, QuatF rotation, Point3F scale)
	{
		this.position = position;
		this.rotation = rotation;
		this.scale = scale;
		
		this.parentMatrix = new MatrixF().initIdentity();
	}
	
	public Transform(Point3F position, QuatF rotation)
	{
		this(position, rotation, new Point3F(1));
	}
	
	public Transform(Point3F position)
	{
		this(position, new QuatF(0, 0, 0, 1));
	}
	
	public Transform()
	{
		this(new Point3F(0));
	}
	
	public void update()
	{
		if (this.oldPosition != null)
		{
			this.oldPosition.set(this.position);
			this.oldRotation.set(this.rotation);
			this.oldScale.set(this.scale);
		} else {
			this.oldPosition = new Point3F(0, 0, 0).set(this.position).add(1.0f);
			this.oldRotation = new QuatF(0, 0, 0, 0).set(this.rotation).mul(0.5f);
			this.oldScale = new Point3F(0, 0, 0).set(this.scale).add(1.0f);
		}
	}
	
	public void rotate(Point3F axis, float angle)
	{
		this.rotation = new QuatF(axis, angle).mul(this.rotation).normalized();
		//System.out.println(this.rotation.getForward());
	}
	
	public void lookAt(Point3F point, Point3F up)
	{
		this.rotation = getLookAtRotation(point, up);
	}
	
	public QuatF getLookAtRotation(Point3F point, Point3F up)
	{
		return new QuatF(new MatrixF().initRotation(point.sub(this.position).normalized(), up));
	}
	
	public boolean hasChanged()
	{
		if (this.parent != null && this.parent.hasChanged())
			return true;
		if (!this.position.equals(this.oldPosition))
			return true;
		if (!this.rotation.equals(this.oldRotation))
			return true;
		if (!this.scale.equals(this.oldScale))
			return true;
		
		return false;
	}
	
	public MatrixF getTransformation()
	{
		MatrixF translationMatrix = new MatrixF().initTranslation(this.position.x, this.position.y, this.position.z);
		MatrixF rotationMatrix = this.rotation.toRotationMatrix();
		MatrixF scaleMatrix = new MatrixF().initScale(this.scale.x, this.scale.y, this.scale.z);
		
		return getParentMatrix().mul(translationMatrix.mul(rotationMatrix.mul(scaleMatrix)));
	}
	
	protected MatrixF getParentMatrix()
	{
		if (this.parent != null && this.parent.hasChanged())
			this.parentMatrix = this.parent.getTransformation();
		
		return this.parentMatrix;
	}
	
	public void setParent(Transform parent)
	{
		this.parent = parent;
	}
	
	public Point3F getTransformedPosition()
	{
		return this.getParentMatrix().transform(this.position);
	}
	
	public QuatF getTransformedRotation()
	{
		QuatF parentRotation = new QuatF(0, 0, 0, 1);
		
		if (this.parent != null)
			parentRotation = this.parent.getTransformedRotation();
		
		return parentRotation.mul(this.rotation);
	}

	public Point3F getPosition()
	{
		return position;
	}

	public void setPosition(Point3F position)
	{
		this.position = position;
	}

	public QuatF getRotation()
	{
		return rotation;
	}

	public void setRotation(QuatF rotation)
	{
		this.rotation = rotation;
	}

	public Point3F getScale()
	{
		return scale;
	}

	public void setScale(Point3F scale)
	{
		this.scale = scale;
	}
}
