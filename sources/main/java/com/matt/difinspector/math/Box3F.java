package com.matt.difinspector.math;

public class Box3F
{
	private Point3F min, max;
	
	public Box3F(Point3F min, Point3F max)
	{
		this.min = min;
		this.max = max;
	}
	
	public Point3F getMin()
	{
		return this.min;
	}
	
	public Point3F getMax()
	{
		return this.max;
	}
	
	@Override
	public String toString()
	{
		return "(Box3F){min: " + this.min + ", max: " + this.max + "}";
	}
}
