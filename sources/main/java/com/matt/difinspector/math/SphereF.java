package com.matt.difinspector.math;

public class SphereF
{
	private Point3F center;
	private float radius;
	
	public SphereF(Point3F center, float radius)
	{
		this.center = center;
		this.radius = radius;
	}
	
	public void setCenter(Point3F center)
	{
		this.center = center;
	}
	
	public Point3F getCenter()
	{
		return this.center;
	}
	
	public void setRadius(float radius)
	{
		this.radius = radius;
	}
	
	public float getRadius()
	{
		return this.radius;
	}
	
	@Override
	public String toString()
	{
		return "(SphereF){center: " + this.center + ", radius: " + this.radius + "}";
	}
}
