package com.matt.difinspector.materials;

import java.util.ArrayList;
import java.util.List;

import com.matt.difinspector.structures.TexGenPlanes;

public class TexGenList
{
	private List<TexGenPlanes> texGenPlanes;
	
	public TexGenList()
	{
		this.texGenPlanes = new ArrayList<TexGenPlanes>();
	}
	
	public void add(TexGenPlanes planes)
	{
		this.texGenPlanes.add(planes);
	}
	
	public TexGenPlanes get(int index)
	{
		return this.texGenPlanes.get(index);
	}
	
	public int indexOf(TexGenPlanes planes)
	{
		if (planes == null)
			return -1;
		
		for (int i = 0; i < this.texGenPlanes.size(); i++)
		{
			if (planes.equals(this.texGenPlanes.get(i)))
					return i;
		}
		
		return -1;
	}
	
	public boolean contains(TexGenPlanes planes)
	{
		return this.indexOf(planes) >= 0;
	}
	
	public int size()
	{
		return this.texGenPlanes.size();
	}
	
	public void clear()
	{
		this.texGenPlanes.clear();
	}
}
