package com.matt.difinspector.materials;

public class TorqueMaterial
{
	private String name;
	
	public TorqueMaterial(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
}
