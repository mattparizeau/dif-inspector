package com.matt.difinspector.materials;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.matt.difinspector.io.ReverseDataInputStream;
import com.matt.difinspector.io.ReverseDataOutputStream;

public class MaterialList
{
	protected List<TorqueMaterial> materials;
	
	public MaterialList()
	{
		this.materials = new ArrayList<TorqueMaterial>();
	}
	
	public boolean read(ReverseDataInputStream dis) throws IOException
	{
		byte version = dis.readByte();
		
		if (version != 1)
		{
			System.err.println("Incompatible material version");
			return false;
		}
		
		int materialCount = dis.readInt();
		for (int i = 0; i < materialCount; i++)
		{
			String name = dis.readString();
			
			TorqueMaterial m = new TorqueMaterial(name);
			this.materials.add(m);
		}
		
		return true;
	}
	
	public boolean write(ReverseDataOutputStream dos) throws IOException
	{
		dos.writeByte(1);
		dos.writeInt(this.materials.size());
		
		for (int i = 0; i < this.materials.size(); i++)
		{
			dos.writeString(this.materials.get(i).getName());// + "_mbm");
		}
		
		return true;
	}
	
	public List<TorqueMaterial> getMaterials()
	{
		return this.materials;
	}

	public void dumpInfo(BufferedWriter bw) throws IOException
	{
		final String TAB = "    ";
		
		bw.write(TAB + TAB + "numMaterials = " + this.materials.size() + "\n");
		for (int i = 0; i < this.materials.size(); i++)
		{
			TorqueMaterial material = this.materials.get(i);
			bw.write(TAB + TAB + "material[" + i + "] = new Material(){name: " + material.getName() + "}\n");
		}
	}
	
	public void addMaterial(String texture)
	{
		this.materials.add(new TorqueMaterial(texture));
	}
	
	public short indexOf(String material)
	{
		for (short i = 0; i < this.materials.size(); i++)
		{
			String name = this.materials.get(i).getName();
			if (material.equalsIgnoreCase(name))
				return i;
		}
		return -1;
	}
	
	public boolean contains(String material)
	{
		return this.indexOf(material) >= 0;
	}
	
}
