package com.matt.difinspector.mesh;

import com.matt.difinspector.math.Point2F;
import com.matt.difinspector.math.Point3F;

public class Mesh
{
	protected Point3F[] vertices;
	protected int[] indices;
	
	protected Point2F[] textureCoords;
	protected int[] texCoordIndices;
	
	protected String[] textures;
	protected int[] textureIndices;
	
	public Mesh()
	{
		this.vertices = new Point3F[0];
		this.indices = new int[0];
		
		this.textureCoords = new Point2F[0];
		this.texCoordIndices = new int[0];
		
		this.textures = new String[0];
		this.textureIndices = new int[0];
	}

	public Point3F[] getVertices()
	{
		return this.vertices;
	}

	public int[] getIndices()
	{
		return this.indices;
	}

	public void setVertices(Point3F[] vertices, int[] indices)
	{
		this.vertices = vertices;
		this.indices = indices;
	}

	public Point2F[] getTextureCoords()
	{
		return this.textureCoords;
	}

	public int[] getTexCoordIndices()
	{
		return this.texCoordIndices;
	}

	public void setTextureCoords(Point2F[] textureCoords, int[] texCoordIndices)
	{
		this.textureCoords = textureCoords;
		this.texCoordIndices = texCoordIndices;
	}

	public String[] getTextures()
	{
		return this.textures;
	}

	public int[] getTextureIndices()
	{
		return this.textureIndices;
	}

	public void setTextures(String[] textures, int[] textureIndices)
	{
		this.textures = textures;
		this.textureIndices = textureIndices;
	}
}
