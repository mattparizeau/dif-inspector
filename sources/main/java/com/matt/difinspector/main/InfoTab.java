package com.matt.difinspector.main;

import java.awt.BorderLayout;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.matt.difinspector.interior.InteriorResource;

public class InfoTab extends JPanel
{
	private static final long serialVersionUID = -2966079731355627417L;
	
	private DefaultListModel<String> listModel;
	private JList<String> list;
	
	public InfoTab()
	{
		this.setLayout(new BorderLayout());
		this.listModel = new DefaultListModel<String>();
		this.list = new JList<String>(listModel);
		JScrollPane scroll = new JScrollPane(this.list);
		this.add(scroll, BorderLayout.CENTER);
	}
	
	public void update()
	{
		this.listModel.removeAllElements();
		InteriorResource interior = DifInspector.getInstance().getInterior();
		
		if (interior.getDetailLevels().size() > 0)
			this.listModel.addElement("File Version: " + interior.getDetailLevels().get(0).getFileVersion());
		else
			this.listModel.addElement("File Version: ???");
		this.listModel.addElement("Num Detail Levels: " + interior.getDetailLevels().size());
		this.listModel.addElement("Num Sub Objects: " + interior.getSubObjects().size());
		this.listModel.addElement("Num Triggers: " + interior.getTriggers().size());
		this.listModel.addElement("Num Interior Path Followers: " + interior.getInteriorPathFollowers().size());
		this.listModel.addElement("Num Force Fields: " + interior.getForceFields().size());
		this.listModel.addElement("Num AI Special Nodes: " + interior.getAISpecialNodes().size());
		this.listModel.addElement("Vehicle Collisions: " + (interior.getHasVehicleCollisions() ? "Yes" : "No"));
		this.listModel.addElement("Num Game Entities: " + interior.getGameEntities().size());
	}
}
