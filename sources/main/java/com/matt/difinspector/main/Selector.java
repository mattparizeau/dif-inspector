package com.matt.difinspector.main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import com.matt.difinspector.interior.Interior;
import com.matt.difinspector.interior.InteriorResource;
import com.matt.difinspector.render.EnumSelectionMode;
import com.matt.difinspector.render.GLRender;

public class Selector extends JFrame
{
	private static final long serialVersionUID = -4992134287486338958L;
	
	private JPanel panel;
	private GLRender render;
	
	private IntField indexField;
	private JButton selectButton;
	private EnumSelectionMode mode;
	
	private JRadioButton surfacesRadio;
	private JRadioButton pointsRadio;
	private JRadioButton hullsRadio;
	private JRadioButton planesRadio;
	
	public Selector(GLRender render)
	{
		this.mode = EnumSelectionMode.SURFACES;
		this.render = render;
		this.setTitle("Select Surface");
		this.setSize(320, 120);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.panel = new JPanel();
		
		this.surfacesRadio = new JRadioButton("Surfaces", true);
		this.surfacesRadio.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e)
			{
				if (e.getStateChange() == ItemEvent.SELECTED)
				{
					Selector.this.mode = EnumSelectionMode.SURFACES;
					Selector.this.refresh();
				}
			}
		});
		this.pointsRadio = new JRadioButton("Points");
		this.pointsRadio.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e)
			{
				if (e.getStateChange() == ItemEvent.SELECTED)
				{
					Selector.this.mode = EnumSelectionMode.POINTS;
					Selector.this.refresh();
				}
			}
		});
		this.hullsRadio = new JRadioButton("Hulls");
		this.hullsRadio.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e)
			{
				if (e.getStateChange() == ItemEvent.SELECTED)
				{
					Selector.this.mode = EnumSelectionMode.HULLS;
					Selector.this.refresh();
				}
			}
		});
		this.planesRadio = new JRadioButton("Planes");
		this.planesRadio.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e)
			{
				if (e.getStateChange() == ItemEvent.SELECTED)
				{
					Selector.this.mode = EnumSelectionMode.PLANES;
					Selector.this.refresh();
				}
			}
		});
		ButtonGroup group = new ButtonGroup();
		group.add(this.surfacesRadio);
		group.add(this.pointsRadio);
		group.add(this.hullsRadio);
		group.add(this.planesRadio);
		
		this.indexField = new IntField(0);
		this.indexField.addValueChangedListener(new ValueChangedListener() {
			
			@Override
			public void valueChanged(ValueChangedEvent event)
			{
				Selector.this.updateHighlighting();
			}
		});
		
		this.selectButton = new JButton("Select");
		this.selectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				Selector.this.select();
			}
		});
		
		this.panel.add(this.surfacesRadio);
		this.panel.add(this.pointsRadio);
		this.panel.add(this.hullsRadio);
		this.panel.add(this.planesRadio);
		this.panel.add(this.indexField);
		this.panel.add(this.selectButton);
		
		this.add(this.panel);
		this.setLocationRelativeTo(null);
	}
	
	@Override
	public void setVisible(boolean b)
	{
		if (b)
		{
			this.render.setSelected(mode, this.indexField.getValue());
		} else {
			this.render.setSelected(EnumSelectionMode.NONE, 0);
		}
		super.setVisible(b);
	}
	
	private void updateHighlighting()
	{
		int highlightIndex = this.indexField.getValue();
		//this.render.setHighlight(true, false, true, highlightIndex);
		this.render.setSelected(mode, highlightIndex);
	}
	
	public void updateRange()
	{
		InteriorResource res = DifInspector.getInstance().getInterior();
		Interior interior = res.getDetailLevels().get(0);
		
		if (this.mode == EnumSelectionMode.SURFACES)
		{
			if (interior.getSurfaces() == null)
				return;
			this.indexField.setMax(interior.getSurfaces().length - 1);
		}
		else if (this.mode == EnumSelectionMode.POINTS)
		{
			if (interior.getPoints() == null)
				return;
			this.indexField.setMax(interior.getPoints().length - 1);
		}
		else if (this.mode == EnumSelectionMode.HULLS)
		{
			if (interior.getConvexHulls() == null)
				return;
			this.indexField.setMax(interior.getConvexHulls().length - 1);
		}
		else if (this.mode == EnumSelectionMode.PLANES)
		{
			if (interior.getPlanes() == null)
				return;
			this.indexField.setMax(interior.getPlanes().length - 1);
		}
	}
	
	public void refresh()
	{
		this.updateRange();
		this.updateHighlighting();
	}
	
	public void select()
	{
		this.setVisible(false);
		int selectedIndex = this.indexField.getValue();

		if (this.mode == EnumSelectionMode.SURFACES)
			DifInspector.getInstance().editSurface(selectedIndex);
		else if (this.mode == EnumSelectionMode.POINTS)
			DifInspector.getInstance().editPoint(selectedIndex);
		else if (this.mode == EnumSelectionMode.PLANES)
			DifInspector.getInstance().editPlane(selectedIndex);
	}
}
