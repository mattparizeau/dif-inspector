package com.matt.difinspector.main;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import com.matt.difinspector.interior.Interior;
import com.matt.difinspector.interior.InteriorResource;
import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.math.QuatF;
import com.matt.difinspector.render.GLRender;

public class RenderSettings extends JFrame
{
	private static final long serialVersionUID = -4992134287486338958L;
	
	private JPanel panel;
	private JPanel highlightPanel;
	private JPanel highlightIndexPanel;
	private JPanel transformOptionsPanel;
	private JPanel transformPanel;
	private GLRender render;
	
	private JRadioButton hulls;
	private JRadioButton surfaces;
	private JCheckBox highlight;
	//private JTextField indexField;
	private IntField indexField;
	//private JButton incIndex;
	//private JButton decIndex;
	
	private JCheckBox rotate;
	//private Point3F position;
	//private Point3F rotation;
	
	private Point3Field position;
	private Point3Field rotation;
	
	public RenderSettings(GLRender render)
	{
		this.render = render;
		this.setTitle("Render Settings");
		this.setSize(320, 240);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.panel = new JPanel();
		
		highlight = new JCheckBox("Highlight");
		hulls = new JRadioButton("Highlight Hulls", true);
		surfaces = new JRadioButton("Highlight Surfaces");
		ButtonGroup group = new ButtonGroup();
		
		group.add(hulls);
		group.add(surfaces);
		
		this.indexField = new IntField(0);
		/*this.indexField = new JTextField("0", 20);
		this.incIndex = new JButton(">");
		this.incIndex.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				RenderSettings.this.increaseHighlightIndex();
			}
		});
		this.decIndex = new JButton("<");
		this.decIndex.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				RenderSettings.this.decreaseHighlightIndex();
			}
		});*/
		
		this.highlight.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				RenderSettings.this.updateHighlighting();
			}
		});
		
		this.hulls.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				RenderSettings.this.updateRange();
				RenderSettings.this.updateHighlighting();
			}
		});
		
		this.surfaces.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				RenderSettings.this.updateRange();
				RenderSettings.this.updateHighlighting();
			}
		});
		
		/*this.indexField.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e)
			{
				RenderSettings.this.updateHighlighting();
			}
			
			@Override
			public void keyReleased(KeyEvent e){}
			
			@Override
			public void keyPressed(KeyEvent e){}
		});*/
		
		this.indexField.addValueChangedListener(new ValueChangedListener() {
			
			@Override
			public void valueChanged(ValueChangedEvent event)
			{
				RenderSettings.this.updateHighlighting();
			}
		});
		
		this.highlightPanel = new JPanel();
		this.highlightPanel.add(this.highlight);
		this.highlightPanel.add(this.hulls);
		this.highlightPanel.add(this.surfaces);
		this.highlightIndexPanel = new JPanel();
		//this.highlightIndexPanel.add(this.decIndex);
		this.highlightIndexPanel.add(this.indexField);
		//this.highlightIndexPanel.add(this.incIndex);
		this.highlightPanel.setPreferredSize(new Dimension(290, 75));
		this.highlightPanel.add(this.highlightIndexPanel);
		this.highlightPanel.setBorder(BorderFactory.createEtchedBorder());
		//this.panel.add(this.highlightPanel);
		
		this.transformPanel = new JPanel();
		this.transformPanel.setPreferredSize(new Dimension(290, 120));
		this.transformPanel.setBorder(BorderFactory.createEtchedBorder());
		this.transformOptionsPanel = new JPanel();
		this.transformOptionsPanel.setPreferredSize(new Dimension(280, 25));
		
		this.rotate = new JCheckBox("Rotate", false);
		this.rotate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				RenderSettings.this.updateSettings();
			}
		});
		
		this.position = new Point3Field("Position: ", new Point3F(0, 0, 40));
		this.position.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				RenderSettings.this.updateSettings();
			}
		});
		this.rotation = new Point3Field("Rotation: ", new Point3F(-60, 0, 0));
		this.rotation.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				RenderSettings.this.updateSettings();
			}
		});
		this.transformOptionsPanel.add(this.rotate);
		this.transformPanel.add(this.transformOptionsPanel);
		this.transformPanel.add(this.position);
		this.transformPanel.add(this.rotation);
		
		this.panel.add(this.transformPanel);
		
		
		this.add(this.panel);
		this.pack();
		
		this.setLocationRelativeTo(null);
	}
	
	/*private void increaseHighlightIndex()
	{
		String indexText = this.indexField.getText();
		
		int highlightIndex;
		
		try {
			highlightIndex = Integer.parseInt(indexText) + 1;
		} catch (NumberFormatException e)
		{
			highlightIndex = 0;
		}
		
		this.indexField.setText("" + highlightIndex);
		
		this.updateHighlighting();
	}
	
	private void decreaseHighlightIndex()
	{
		String indexText = this.indexField.getText();
		
		int highlightIndex;
		
		try {
			highlightIndex = Integer.parseInt(indexText) - 1;
		} catch (NumberFormatException e)
		{
			highlightIndex = 0;
		}
		
		if (highlightIndex < 0)
			highlightIndex = 0;
		
		this.indexField.setText("" + highlightIndex);
		
		this.updateHighlighting();
	}*/
	
	private void updateSettings()
	{
		this.render.setAutoRotate(this.rotate.isSelected());
		
		this.render.getCamera().getTransform().setPosition(this.position.getPoint());
		this.render.getCamera().getTransform().setRotation(new QuatF(this.rotation.getPoint()));
	}
	
	private void updateHighlighting()
	{
		/*String indexText = this.indexField.getText();
		
		int highlightIndex;
		
		try {
			highlightIndex = Integer.parseInt(indexText);
		} catch (NumberFormatException e)
		{
			highlightIndex = 0;
		}*/
		
		//int highlightIndex = this.indexField.getValue();
		
		//if (highlightIndex < 0)
			//this.indexField.setValue(0);
		
		//this.render.setHighlight(this.highlight.isSelected(), this.hulls.isSelected(), this.surfaces.isSelected(), highlightIndex);
	}
	
	public void updateRange()
	{
		InteriorResource res = DifInspector.getInstance().getInterior();
		Interior interior = res.getDetailLevels().get(0);
		
		
		if (this.hulls.isSelected())
		{
			if (interior.getConvexHulls() == null)
				return;
			this.indexField.setMax(interior.getConvexHulls().length - 1);
		} else if (this.surfaces.isSelected())
		{
			if (interior.getSurfaces() == null)
				return;
			this.indexField.setMax(interior.getSurfaces().length - 1);
		}
	}
	
	public void refresh()
	{
		this.updateRange();
		this.updateHighlighting();
	}
}
