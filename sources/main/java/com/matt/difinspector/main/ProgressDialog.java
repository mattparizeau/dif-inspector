package com.matt.difinspector.main;

import java.awt.Dimension;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

public class ProgressDialog extends JDialog
{
	private static final long serialVersionUID = 3325346180146941878L;
	
	private JPanel panel;
	private JLabel status;
	private JProgressBar bar;
	
	public ProgressDialog(JFrame parent)
	{
		super(parent, false);
		this.setTitle("Loading...");
		
		this.panel = new JPanel();
		this.panel.setPreferredSize(new Dimension(300, 50));
		this.status = new JLabel("Loading...");
		this.status.setPreferredSize(new Dimension(290, 25));
		this.bar = new JProgressBar(0, 100);
		this.bar.setPreferredSize(new Dimension(290, 25));
		
		this.panel.add(this.status);
		this.panel.add(this.bar);
		this.add(this.panel);
		
		this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		
		this.setSize(320, 120);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
	}
	
	public void showDialog()
	{
		this.getParent().setEnabled(false);
		if (!this.isVisible())
			this.setVisible(true);
	}
	
	public void hideDialog()
	{
		this.getParent().setEnabled(true);
		this.setVisible(false);
		this.setStatus("");
		this.setProgress(0);
	}
	
	public void setStatus(String status)
	{
		if (!this.status.getText().equals(status))
			this.status.setText(status);
	}
	
	public void setProgress(int value)
	{	
		if (value >= 100)
		{
			this.hideDialog();
			return;
		}
		
		if (this.bar.getValue() != value)
			this.bar.setValue(value);
	}
}
