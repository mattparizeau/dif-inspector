package com.matt.difinspector.main;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.matt.difinspector.math.Point3F;

public class Point3Field extends JComponent
{
	private static final long serialVersionUID = -7166619358118798606L;
	
	private JLabel label;
	private JTextField xField;
	private JTextField yField;
	private JTextField zField;
	
	private class PointActionListener implements KeyListener
	{
		@Override
		public void keyTyped(KeyEvent e)
		{
			ActionEvent event = new ActionEvent(Point3Field.this, ActionEvent.ACTION_PERFORMED, "Changed");
			//PointField.this.dispatchEvent();
			ActionListener[] listeners = Point3Field.this.listenerList.getListeners(ActionListener.class);
			for (int i = 0; i < listeners.length; i++)
			{
				listeners[i].actionPerformed(event);
			}
		}

		@Override
		public void keyPressed(KeyEvent e){}

		@Override
		public void keyReleased(KeyEvent e){}
	}
	
	public Point3Field(String label, Point3F point)
	{
		this.setLayout(new FlowLayout());
		this.setPreferredSize(new Dimension(175, 25));
		this.label = new JLabel(label);
		this.xField = new JTextField("" + point.getX(), 3);
		this.xField.addKeyListener(new PointActionListener());
		this.yField = new JTextField("" + point.getY(), 3);
		this.yField.addKeyListener(new PointActionListener());
		this.zField = new JTextField("" + point.getZ(), 3);
		this.zField.addKeyListener(new PointActionListener());
		
		this.add(this.label);
		this.add(this.xField);
		this.add(this.yField);
		this.add(this.zField);
	}
	
	public Point3Field(Point3F point)
	{
		this("", point);
	}
	
	public Point3Field(String label)
	{
		this(label, new Point3F());
	}
	
	public Point3Field()
	{
		this("");
	}
	
	public void setPoint(Point3F point)
	{
		this.xField.setText("" + point.getX());
		this.yField.setText("" + point.getY());
		this.zField.setText("" + point.getZ());
	}
	
	public Point3F getPoint()
	{
		float x;
		float y;
		float z;
		
		try {
			x = Float.parseFloat(this.xField.getText());
		} catch (NumberFormatException e)
		{
			x = 0;
		}
		
		try {
			y = Float.parseFloat(this.yField.getText());
		} catch (NumberFormatException e)
		{
			y = 0;
		}
		
		try {
			z = Float.parseFloat(this.zField.getText());
		} catch (NumberFormatException e)
		{
			z = 0;
		}
		
		return new Point3F(x, y, z);
	}
	
	public void addActionListener(ActionListener l) {
        this.listenerList.add(ActionListener.class, l);
    }
	
}
