package com.matt.difinspector.main;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.matt.difinspector.math.Point2F;

public class Point2Field extends JComponent
{
	private static final long serialVersionUID = 4432082004476176516L;
	
	private JLabel label;
	private JTextField xField;
	private JTextField yField;
	
	private class PointActionListener implements KeyListener
	{
		@Override
		public void keyTyped(KeyEvent e)
		{
			ActionEvent event = new ActionEvent(Point2Field.this, ActionEvent.ACTION_PERFORMED, "Changed");
			//PointField.this.dispatchEvent();
			ActionListener[] listeners = Point2Field.this.listenerList.getListeners(ActionListener.class);
			for (int i = 0; i < listeners.length; i++)
			{
				listeners[i].actionPerformed(event);
			}
		}

		@Override
		public void keyPressed(KeyEvent e){}

		@Override
		public void keyReleased(KeyEvent e){}
	}
	
	public Point2Field(String label, Point2F point)
	{
		this.setLayout(new FlowLayout());
		this.setPreferredSize(new Dimension(175, 25));
		this.label = new JLabel(label);
		this.xField = new JTextField("" + point.getX(), 3);
		this.xField.addKeyListener(new PointActionListener());
		this.yField = new JTextField("" + point.getY(), 3);
		this.yField.addKeyListener(new PointActionListener());
		
		this.add(this.label);
		this.add(this.xField);
		this.add(this.yField);
	}
	
	public Point2Field(Point2F point)
	{
		this("", point);
	}
	
	public Point2Field(String label)
	{
		this(label, new Point2F());
	}
	
	public Point2Field()
	{
		this("");
	}
	
	public void setPoint(Point2F point)
	{
		this.xField.setText("" + point.getX());
		this.yField.setText("" + point.getY());
	}
	
	public Point2F getPoint()
	{
		float x;
		float y;
		
		try {
			x = Float.parseFloat(this.xField.getText());
		} catch (NumberFormatException e)
		{
			x = 0;
		}
		
		try {
			y = Float.parseFloat(this.yField.getText());
		} catch (NumberFormatException e)
		{
			y = 0;
		}
		
		return new Point2F(x, y);
	}
	
	public void addActionListener(ActionListener l) {
        this.listenerList.add(ActionListener.class, l);
    }
	
}
