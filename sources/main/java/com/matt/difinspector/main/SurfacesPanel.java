package com.matt.difinspector.main;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.matt.difinspector.interior.Interior;
import com.matt.difinspector.structures.Surface;
import com.matt.difinspector.util.Util;

public class SurfacesPanel extends SubPanel
{
	private static final long serialVersionUID = -6923922097457175257L;
	
	private DefaultTableModel model;
	private JTable table;
	
	public SurfacesPanel()
	{
		this.setLayout(new BorderLayout());

		this.model = new DefaultTableModel();
		
		
		this.table = new JTable(model);
		this.table.setFillsViewportHeight(true);
		
		JScrollPane scroll = new JScrollPane(table);
		scroll.setColumnHeader(null);
		this.add(scroll, BorderLayout.CENTER);
	}
	
	public void update(Interior interior)
	{
		Surface[] surfaces = interior.getSurfaces();
		
		Vector<String> tableHeader = new Vector<String>();
		tableHeader.addElement("Index");
		tableHeader.addElement("Winding Start");
		tableHeader.addElement("Plane Index");
		tableHeader.addElement("Texture Index");
		tableHeader.addElement("TexGen Index");
		tableHeader.addElement("Light Count");
		tableHeader.addElement("Surface Flags");
		tableHeader.addElement("Winding Count");
		tableHeader.addElement("Fan Mask");
		tableHeader.addElement("Light State Info Start");
		tableHeader.addElement("Map Offset X");
		tableHeader.addElement("Map Offset Y");
		tableHeader.addElement("Map Size X");
		tableHeader.addElement("Map Size Y");
		tableHeader.addElement("T");
		tableHeader.addElement("B");
		tableHeader.addElement("N");
		tableHeader.addElement("Normal");
		tableHeader.addElement("Unused");
		tableHeader.addElement("BrushId");
		
		List<List<String>> data = new ArrayList<List<String>>();
		
		if (surfaces != null)
		{
			for (Surface surface : surfaces)
			{
				List<String> l = new ArrayList<String>();
				l.add("" + surface.getWindingStart());
				l.add("" + surface.getPlaneIndex());
				l.add("" + surface.getTextureIndex() + " (" + surface.getTexture() + ")");
				l.add("" + surface.getTexGenIndex());
				l.add("" + surface.getLightCount());
				l.add("" + surface.getSurfaceFlags());
				l.add("" + surface.getWindingCount());
				l.add("" + surface.getFanMask());
				l.add("" + surface.getLightStateInfoStart());
				l.add("" + surface.getMapOffsetX());
				l.add("" + surface.getMapOffsetY());
				l.add("" + surface.getMapSizeX());
				l.add("" + surface.getMapSizeY());
				l.add("" + "<Point3F>");
				l.add("" + "<Point3F>");
				l.add("" + "<Point3F>");
				l.add("" + "<Point3F>");
				l.add("" + (surface.getUnused() ? "true" : "false"));
				l.add("" + surface.getBrushId());
				data.add(l);
			}
		}
		
		model.setDataVector(Util.stringListToStringVector(data), tableHeader);
	}
}
