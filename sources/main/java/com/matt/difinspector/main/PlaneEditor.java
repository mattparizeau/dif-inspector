package com.matt.difinspector.main;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.matt.difinspector.interior.InteriorResource;
import com.matt.difinspector.math.PlaneF;
import com.matt.difinspector.render.EnumSelectionMode;

public class PlaneEditor extends JFrame
{
	private static final long serialVersionUID = -2746362797227228710L;
	
	private InteriorResource	interior;
	private JPanel				panel;
	private JPanel				dataPanel;
	
	private PlaneField			planeField;
	
	private JButton				applyButton;
								
	private int					planeId;
								
	public PlaneEditor()
	{
		this.setTitle("Plane Editor");
		this.setSize(200, 200);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.panel = new JPanel();
		this.dataPanel = new JPanel();
		
		this.planeField = new PlaneField("Plane: ");
		
		this.applyButton = new JButton("Apply");
		this.applyButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				PlaneEditor.this.apply();
			}
		});
		
		this.dataPanel.setPreferredSize(new Dimension(200, 45));
		this.dataPanel.add(this.planeField);
		
		this.setLayout(new FlowLayout());
		this.add(this.panel);
		this.add(this.dataPanel);
		
		this.add(this.applyButton);
		this.setLocationRelativeTo(null);
		
		this.planeId = 0;
	}
	
	@Override
	public void setVisible(boolean b)
	{
		if (b)
		{
			DifInspector.getInstance().getRender().setSelected(EnumSelectionMode.PLANES, this.planeId);
		} else {
			DifInspector.getInstance().getRender().setSelected(EnumSelectionMode.NONE, 0);
		}
		super.setVisible(b);
	}
	
	public void setInterior(InteriorResource interior)
	{
		this.interior = interior;
		//Interior detail = interior.getDetailLevels().get(0);
		
		/*
		 * this.surfaceSelection.removeAllItems(); for (int i = 0; i < detail.getSurfaces().length; i++) {
		 * this.surfaceSelection.addItem("Surface: " + i); }
		 * 
		 * if (this.surfaceSelection.getItemCount() > 0) this.surfaceSelection.setSelectedIndex(0);
		 */
		
		updateDisplay();
	}
	
	public void updateDisplay()
	{
		if (this.interior.getDetailLevels().get(0).getPlanes() == null)
			return;
		
		int planeIndex = this.planeId;
		PlaneF plane = this.interior.getDetailLevels().get(0).getPlanes()[planeIndex];
		
		this.planeField.setPlane(plane);
	}
	
	private void apply()
	{
		int planeIndex = this.planeId;
		this.interior.getDetailLevels().get(0).getPlanes()[planeIndex] = this.planeField.getPlane();
		
		this.updateDisplay();
	}
	
	public void setPlane(int planeId)
	{
		this.planeId = planeId;
		this.updateDisplay();
	}
}
