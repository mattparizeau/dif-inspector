package com.matt.difinspector.main;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.matt.difinspector.math.PlaneF;

public class PlaneField extends JComponent
{
	private static final long serialVersionUID = -7166619358118798606L;
	
	private JLabel label;
	private JTextField xField;
	private JTextField yField;
	private JTextField zField;
	private JTextField dField;
	
	private class PlaneActionListener implements KeyListener
	{
		@Override
		public void keyTyped(KeyEvent e)
		{
			ActionEvent event = new ActionEvent(PlaneField.this, ActionEvent.ACTION_PERFORMED, "Changed");
			//PointField.this.dispatchEvent();
			ActionListener[] listeners = PlaneField.this.listenerList.getListeners(ActionListener.class);
			for (int i = 0; i < listeners.length; i++)
			{
				listeners[i].actionPerformed(event);
			}
		}

		@Override
		public void keyPressed(KeyEvent e){}

		@Override
		public void keyReleased(KeyEvent e){}
	}
	
	public PlaneField(String label, PlaneF plane)
	{
		this.setLayout(new FlowLayout());
		this.setPreferredSize(new Dimension(200, 25));
		this.label = new JLabel(label);
		this.xField = new JTextField("" + plane.getX(), 3);
		this.xField.addKeyListener(new PlaneActionListener());
		this.yField = new JTextField("" + plane.getY(), 3);
		this.yField.addKeyListener(new PlaneActionListener());
		this.zField = new JTextField("" + plane.getZ(), 3);
		this.zField.addKeyListener(new PlaneActionListener());
		this.dField = new JTextField("" + plane.getD(), 3);
		this.dField.addKeyListener(new PlaneActionListener());
		
		this.add(this.label);
		this.add(this.xField);
		this.add(this.yField);
		this.add(this.zField);
		this.add(this.dField);
	}
	
	public PlaneField(PlaneF point)
	{
		this("", point);
	}
	
	public PlaneField(String label)
	{
		this(label, new PlaneF());
	}
	
	public PlaneField()
	{
		this("");
	}
	
	public void setPlane(PlaneF plane)
	{
		this.xField.setText("" + plane.getX());
		this.yField.setText("" + plane.getY());
		this.zField.setText("" + plane.getZ());
		this.dField.setText("" + plane.getD());
	}
	
	public PlaneF getPlane()
	{
		float x;
		float y;
		float z;
		float d;
		
		try {
			x = Float.parseFloat(this.xField.getText());
		} catch (NumberFormatException e)
		{
			x = 0;
		}
		
		try {
			y = Float.parseFloat(this.yField.getText());
		} catch (NumberFormatException e)
		{
			y = 0;
		}
		
		try {
			z = Float.parseFloat(this.zField.getText());
		} catch (NumberFormatException e)
		{
			z = 0;
		}
		
		try {
			d = Float.parseFloat(this.dField.getText());
		} catch (NumberFormatException e)
		{
			d = 0;
		}
		
		return new PlaneF(x, y, z, d);
	}
	
	public void addActionListener(ActionListener l) {
        this.listenerList.add(ActionListener.class, l);
    }
	
}
