package com.matt.difinspector.main;

import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JComboBox;
import javax.swing.JPanel;

import com.matt.difinspector.interior.InteriorResource;

public class DetailLevelsTab extends JPanel
{
	private static final long serialVersionUID = -5578301654094737597L;
	
	private Map<String, SubPanel> panels;
	
	private JComboBox<String> levels;
	private JComboBox<String> menu;
	private SubPanel displayedPanel;
	
	public DetailLevelsTab()
	{
		this.panels = new HashMap<String, SubPanel>();
		this.addPanels();
		
		this.setLayout(new BorderLayout());
		
		this.levels = new JComboBox<String>();
		
		this.levels.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e)
			{
				if (e.getStateChange() == ItemEvent.SELECTED)
				{
					DetailLevelsTab.this.setDisplay(menu.getSelectedItem().toString());
				}
			}
		});
		
		this.menu = new JComboBox<String>();
		for (String s : this.panels.keySet())
		{
			this.menu.addItem(s);
		}
		
		this.menu.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e)
			{
				if (e.getStateChange() == ItemEvent.SELECTED)
				{
					DetailLevelsTab.this.setDisplay(e.getItem().toString());
				}
			}
		});
		
		JPanel menus = new JPanel();
		
		menus.add(this.levels);
		menus.add(this.menu);
		
		this.add(menus, BorderLayout.NORTH);
		
		this.update();
	}
	
	public void addPanels()
	{
		this.panels.put("Materials", new MaterialsPanel());
		this.panels.put("Edges", new EdgesPanel());
		this.panels.put("Surfaces", new SurfacesPanel());
		this.panels.put("Points", new PointsPanel());
		this.panels.put("General", new GeneralPanel());
		this.panels.put("Planes", new PlanesPanel());
	}
	
	public void setDisplay(String option)
	{
		if (this.displayedPanel != null)
		{
			this.remove(this.displayedPanel);
		}
		
		this.displayedPanel = this.panels.get(option);
		
		if (this.displayedPanel != null)
		{
			this.add(this.displayedPanel);
			if (DifInspector.getInstance() != null && DifInspector.getInstance().getInterior() != null)
				this.displayedPanel.update(DifInspector.getInstance().getInterior().getDetailLevels().get(this.levels.getSelectedIndex()));
			this.displayedPanel.revalidate();
		} else {
			System.err.println("Could not find panel: " + option);
		}
	}
	
	public void update()
	{
		this.levels.removeAllItems();
		if (DifInspector.getInstance() != null && DifInspector.getInstance().getInterior() != null)
		{
			InteriorResource interior = DifInspector.getInstance().getInterior();
			for (int i = 0; i < interior.getDetailLevels().size(); i++)
			{
				this.levels.addItem("Detail Level: " + i);
			}
		}
		
		if (this.levels.getItemCount() > 0)
			this.levels.setSelectedIndex(0);
		this.menu.setSelectedIndex(0);
		this.setDisplay(this.menu.getSelectedItem().toString());
	}
}
