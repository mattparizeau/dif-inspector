package com.matt.difinspector.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JTextField;

public class FloatField extends JComponent
{
	private static final long serialVersionUID = 7230243367647067418L;
	
	private JTextField numberTextField;
	private JButton incButton;
	private JButton decButton;
	private String lastValue;
	private float min;
	private float max;
	
	public FloatField(float value)
	{
		this.setLayout(new BorderLayout());
		this.numberTextField = new JTextField("" + value);
		this.setPreferredSize(new Dimension(175, 25));
		this.numberTextField.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e)
			{
				KeyListener[] listeners = FloatField.this.getKeyListeners();//.listenerList.getListeners(KeyListener.class);
				for (KeyListener l : listeners)
				{
					KeyEvent ev = new KeyEvent(FloatField.this, e.getID(), e.getWhen(), e.getModifiers(), e.getKeyCode(), e.getKeyChar(), e.getKeyLocation());
					l.keyTyped(ev);
				}
			}
			
			@Override
			public void keyReleased(KeyEvent e)
			{
				KeyListener[] listeners = FloatField.this.getKeyListeners();//.listenerList.getListeners(KeyListener.class);
				for (KeyListener l : listeners)
				{
					KeyEvent ev = new KeyEvent(FloatField.this, e.getID(), e.getWhen(), e.getModifiers(), e.getKeyCode(), e.getKeyChar(), e.getKeyLocation());
					l.keyReleased(ev);
				}
			}
			
			@Override
			public void keyPressed(KeyEvent e)
			{
				KeyListener[] listeners = FloatField.this.getKeyListeners();//.listenerList.getListeners(KeyListener.class);
				for (KeyListener l : listeners)
				{
					KeyEvent ev = new KeyEvent(FloatField.this, e.getID(), e.getWhen(), e.getModifiers(), e.getKeyCode(), e.getKeyChar(), e.getKeyLocation());
					l.keyPressed(ev);
				}
				
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
				{
					if (FloatField.this.hasValueChanged())
					{
						FloatField.this.fireValueChangedEvent();
					}
					FloatField.this.updateValueChanged();
				}
			}
		});
		this.incButton = new JButton(">");
		this.incButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				FloatField.this.increase();
			}
		});
		this.decButton = new JButton("<");
		this.decButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				FloatField.this.decrease();
			}
		});
		
		this.add(this.incButton, BorderLayout.EAST);
		this.add(this.numberTextField, BorderLayout.CENTER);
		this.add(this.decButton, BorderLayout.WEST);
		
		this.min = 0;
		this.max = 100;
		
		this.updateValueChanged();
	}
	
	protected void increase()
	{
		this.setValue(this.getValue() + 1);
		this.fireValueChangedEvent();
	}
	
	protected void decrease()
	{
		this.setValue(this.getValue() - 1);
		this.fireValueChangedEvent();
	}
	
	protected void updateValueChanged()
	{
		float value = this.getValue();
		if (value < this.min)
			this.setValue(this.min);
		if (value > this.max)
			this.setValue(this.max);
		this.lastValue = this.numberTextField.getText();
	}
	
	protected boolean hasValueChanged()
	{
		float value = this.getValue();
		if (value < this.min || value > this.max)
			return false;
		return !this.lastValue.equals(this.numberTextField.getText());
	}
	
	protected void fireValueChangedEvent()
	{
		ValueChangedEvent event = new ValueChangedEvent(this, ValueChangedEvent.VALUE_CHANGED_DEFAULT);
		ValueChangedListener[] listeners = this.listenerList.getListeners(ValueChangedListener.class);
		for (ValueChangedListener listener : listeners)
		{
			listener.valueChanged(event);
		}
	}
	
	public void addValueChangedListener(ValueChangedListener listener)
	{
		this.listenerList.add(ValueChangedListener.class, listener);
	}
	
	public void removeValueChangedListener(ValueChangedListener listener)
	{
		this.listenerList.remove(ValueChangedListener.class, listener);
	}
	
	public float getValue()
	{
		float result;
		
		try {
			result = Float.parseFloat(this.numberTextField.getText());
		} catch (NumberFormatException e)
		{
			result = 0;
		}
		
		return result;
	}
	
	public void setValue(float value)
	{
		if (value < this.min)
			value = this.min;
		if (value > this.max)
			value = this.max;
		this.numberTextField.setText("" + value);
		this.updateValueChanged();
	}

	public float getMin()
	{
		return min;
	}

	public void setMin(float min)
	{
		this.min = min;
		float value = this.getValue();
		if (value < this.min)
			this.setValue(this.min);
		if (value > this.max)
			this.setValue(this.max);
	}

	public float getMax()
	{
		return max;
	}

	public void setMax(float max)
	{
		this.max = max;
		float value = this.getValue();
		if (value < this.min)
			this.setValue(this.min);
		if (value > this.max)
			this.setValue(this.max);
	}
}
