package com.matt.difinspector.main;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.matt.difinspector.interior.InteriorResource;
import com.matt.difinspector.materials.TexData;
import com.matt.difinspector.math.PlaneF;
import com.matt.difinspector.math.Point2F;
import com.matt.difinspector.math.Point2I;
import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.structures.Surface;
import com.matt.difinspector.structures.TexGenPlanes;
import com.matt.difinspector.util.ImageData;
import com.matt.difinspector.util.Util;

public class SurfaceEditor extends JFrame
{
	private static final long	serialVersionUID	= -4992134287486338958L;
													
	private InteriorResource	interior;
	private JPanel				panel;
	private JPanel				texturePanel;
								
	// private JComboBox<String> surfaceSelection;
	
	private JLabel				textureIndexLabel;
	private JTextField			indexField;
	// private JButton incIndex;
	// private JButton decIndex;
	
	private JButton				setCoordsButton;
	private SetCoordsWindow		setCoordsWindow;
	
	private JButton				applyButton;
								
	private PlaneField			planeXField;
	private PlaneField			planeYField;
								
	private int					surfaceId;
	
	private JCheckBox			surfaceFlipped;
	
	private Point2IField mapOffset;
	private Point2IField mapSize;
	
	private JPanel mapPanel;
								
	public SurfaceEditor()
	{
		this.setTitle("Surface Editor");
		this.setSize(200, 320);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.panel = new JPanel();
		this.texturePanel = new JPanel();
		
		this.mapOffset = new Point2IField("Map Offset");
		this.mapSize = new Point2IField("Map Size");
		
		this.mapPanel = new JPanel();
		this.mapPanel.setPreferredSize(new Dimension(200, 75));
		this.mapPanel.add(this.mapOffset);
		this.mapPanel.add(this.mapSize);
		
		// this.surfaceSelection = new JComboBox<String>();
		// this.surfaceSelection.setPreferredSize(new Dimension(290, 25));
		
		this.textureIndexLabel = new JLabel("Texture: ");
		
		this.indexField = new JTextField("0", 20);
		/*
		 * this.incIndex = new JButton(">"); this.incIndex.addActionListener(new ActionListener() {
		 * 
		 * @Override public void actionPerformed(ActionEvent e) { SurfaceEditor.this.increaseHighlightIndex(); } });
		 * this.decIndex = new JButton("<"); this.decIndex.addActionListener(new ActionListener() {
		 * 
		 * @Override public void actionPerformed(ActionEvent e) { SurfaceEditor.this.decreaseHighlightIndex(); } });
		 */
		
		this.planeXField = new PlaneField("Plane X: ");
		this.planeYField = new PlaneField("Plane Y: ");
		
		this.setCoordsButton = new JButton("...");
		this.setCoordsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				SurfaceEditor.this.setCoords();
			}
		});
		
		this.applyButton = new JButton("Apply");
		this.applyButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				SurfaceEditor.this.apply();
			}
		});
		
		/*
		 * this.surfaceSelection.addItemListener(new ItemListener() {
		 * 
		 * @Override public void itemStateChanged(ItemEvent e) { if (e.getStateChange() == ItemEvent.SELECTED) {
		 * SurfaceEditor.this.updateDisplay(); } } });
		 * 
		 * this.add(this.surfaceSelection);
		 */
		
		this.surfaceFlipped = new JCheckBox("No Collision");
		
		this.panel.add(this.textureIndexLabel);
		this.texturePanel.setPreferredSize(new Dimension(200, 125));
		// this.texturePanel.add(this.decIndex);
		this.texturePanel.add(this.indexField);
		// this.texturePanel.add(this.incIndex);
		
		this.texturePanel.add(this.planeXField);
		this.texturePanel.add(this.planeYField);
		this.texturePanel.add(this.setCoordsButton);
		
		this.setLayout(new FlowLayout());
		this.add(this.panel);
		this.add(this.texturePanel);
		this.add(this.mapPanel);
		this.add(this.surfaceFlipped);
		
		this.add(this.applyButton);
		this.setLocationRelativeTo(null);
		
		this.surfaceId = 0;
		
		this.setCoordsWindow = new SetCoordsWindow(this);
	}
	
	public void setInterior(InteriorResource interior)
	{
		this.interior = interior;
		//Interior detail = interior.getDetailLevels().get(0);
		
		/*
		 * this.surfaceSelection.removeAllItems(); for (int i = 0; i < detail.getSurfaces().length; i++) {
		 * this.surfaceSelection.addItem("Surface: " + i); }
		 * 
		 * if (this.surfaceSelection.getItemCount() > 0) this.surfaceSelection.setSelectedIndex(0);
		 */
		
		updateDisplay();
	}
	
	public void updateDisplay()
	{
		if (this.interior.getDetailLevels().get(0).getSurfaces() == null)
			return;
		/*
		 * String text = this.surfaceSelection.getSelectedItem().toString();
		 * 
		 * if (text.startsWith("Surface: ")) { text = text.substring(9);
		 * 
		 * try { int surfaceIndex = Integer.parseInt(text);
		 */
		
		int surfaceIndex = this.surfaceId;
		Surface surface = this.interior.getDetailLevels().get(0).getSurfaces()[surfaceIndex];
		
		this.indexField.setText(surface.getTexture().toLowerCase());
		
		TexGenPlanes planes = surface.getTexGen();//this.interior.getDetailLevels().get(0).getTexGenEQs().get(surface.getTexGenIndex());
		
		this.planeXField.setPlane(planes.getPlaneX());
		this.planeYField.setPlane(planes.getPlaneY());
		
		this.surfaceFlipped.setSelected(surface.isPlaneFlipped());
		
		// DifInspector.getInstance().getRender().setHighlight(true, false, true, surfaceIndex);
		/*
		 * } catch (NumberFormatException e) { e.printStackTrace(); } }
		 */
		
		this.mapOffset.setPoint(new Point2I(surface.getMapOffsetX(), surface.getMapOffsetY()));
		this.mapSize.setPoint(new Point2I(surface.getMapSizeX(), surface.getMapSizeY()));
	}
	
	/*
	 * private void increaseHighlightIndex() { String indexText = this.indexField.getText();
	 * 
	 * int textureIndex;
	 * 
	 * try { textureIndex = Integer.parseInt(indexText) + 1; } catch (NumberFormatException e) { textureIndex = 0; }
	 * 
	 * this.indexField.setText("" + textureIndex); }
	 * 
	 * private void decreaseHighlightIndex() { String indexText = this.indexField.getText();
	 * 
	 * int textureIndex;
	 * 
	 * try { textureIndex = Integer.parseInt(indexText) - 1; } catch (NumberFormatException e) { textureIndex = 0; }
	 * 
	 * if (textureIndex < 0) textureIndex = 0;
	 * 
	 * this.indexField.setText("" + textureIndex); }
	 */
	
	private void apply()
	{
		/*
		 * String indexText = this.indexField.getText();
		 * 
		 * int textureIndex;
		 * 
		 * try { textureIndex = Integer.parseInt(indexText); } catch (NumberFormatException e) { textureIndex = 0; }
		 * 
		 * if (textureIndex < 0) textureIndex = 0;
		 */
		
		PlaneF planeX = this.planeXField.getPlane();
		PlaneF planeY = this.planeYField.getPlane();
		
		/*
		 * String text = this.surfaceSelection.getSelectedItem().toString();
		 * 
		 * if (text.startsWith("Surface: ")) { text = text.substring(9);
		 * 
		 * try { int surfaceIndex = Integer.parseInt(text);
		 */
		int surfaceIndex = this.surfaceId;
		Surface surface = this.interior.getDetailLevels().get(0).getSurfaces()[surfaceIndex];
		
		// surface.setTextureIndex((short)textureIndex);
		surface.setTexture(this.indexField.getText().toLowerCase());
		
		TexGenPlanes planes = surface.getTexGen();//this.interior.getDetailLevels().get(0).getTexGenEQs().get(surface.getTexGenIndex());
		
		planes.set(planeX, planeY);
		
		if (this.surfaceFlipped.isSelected() != surface.isPlaneFlipped())
		{
			surface.flip();
		}
		
		Point2I offset = this.mapOffset.getPoint();
		surface.setMapOffsetX(offset.getX());
		surface.setMapOffsetY(offset.getY());
		
		Point2I size = this.mapSize.getPoint();
		surface.setMapSizeX(size.getX());
		surface.setMapSizeY(size.getY());
		
		/*
		 * } catch (NumberFormatException e) { e.printStackTrace(); } }
		 */
		
		this.updateDisplay();
	}
	
	public void setCoords()
	{
		//this.setCoordsWindow.clearData();
		
		int surfaceIndex = this.surfaceId;
		Surface surface = this.interior.getDetailLevels().get(0).getSurfaces()[surfaceIndex];
		
		ImageData texture = ImageData.loadImage("textures/" + this.indexField.getText());
		if (texture == null)
		{
			JOptionPane.showMessageDialog(this, "Texture must exist in textures folder to set texture coords this way!");
			return;
		}
		
		PlaneF plane = this.interior.getDetailLevels().get(0).getPlanes()[surface.getPlaneIndex()];
		Point3F normal = new Point3F(plane.getX(), plane.getY(), plane.getZ());
		normal.normalize();
		
		TexGenPlanes planes = new TexGenPlanes(this.planeXField.getPlane(), this.planeYField.getPlane());
		
		TexData texData = Util.getTexData(normal, planes, texture);
		
		this.setCoordsWindow.setData(texData);
		
		this.setCoordsWindow.setVisible(true);
	}
	
	public void setTexGen(Point2F offset, Point2F scale, float rotation)
	{
		int surfaceIndex = this.surfaceId;
		Surface surface = this.interior.getDetailLevels().get(0).getSurfaces()[surfaceIndex];
		
		ImageData texture = ImageData.loadImage("textures/" + this.indexField.getText());
		if (texture == null)
		{
			JOptionPane.showMessageDialog(this, "Texture must exist in textures folder to set texture coords this way!");
			return;
		}
		
		PlaneF plane = this.interior.getDetailLevels().get(0).getPlanes()[surface.getPlaneIndex()];
		Point3F normal = new Point3F(plane.getX(), plane.getY(), plane.getZ());
		normal.normalize();
		TexData texData = new TexData(normal, offset, rotation, scale);
		TexGenPlanes planes = Util.getTexGen(texData, texture);
		
		/*float xx = 0.0f / scale.getX() * (float)texture.getWidth();
		float xy = -1.0f / scale.getX() * (float)texture.getWidth();
		float xz = 0.0f / scale.getX() * (float)texture.getWidth();
		float xd = 16.0f / (float)texture.getWidth();
		PlaneF planeX = new PlaneF(xx, xy, xz, xd);
		float yx = 0.0f / scale.getY() * (float)texture.getHeight();
		float yy = 0.0f / scale.getY() * (float)texture.getHeight();
		float yz = -1.0f / scale.getY() * (float)texture.getHeight();
		float yd = -16.0f / (float)texture.getHeight();
		PlaneF planeY = new PlaneF(yx, yy, yz, yd);*/
		
		this.planeXField.setPlane(planes.getPlaneX());
		this.planeYField.setPlane(planes.getPlaneY());
	}
	
	public void setSurface(int surfaceId)
	{
		this.surfaceId = surfaceId;
		this.updateDisplay();
	}
}
