package com.matt.difinspector.main;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.matt.difinspector.interior.Interior;
import com.matt.difinspector.util.Util;

public class GeneralPanel extends SubPanel
{
	private static final long serialVersionUID = -6923922097457175257L;
	
	private DefaultTableModel model;
	private JTable table;
	
	public GeneralPanel()
	{
		this.setLayout(new BorderLayout());

		this.model = new DefaultTableModel();
		
		
		this.table = new JTable(model);
		this.table.setFillsViewportHeight(true);
		
		JScrollPane scroll = new JScrollPane(table);
		scroll.setColumnHeader(null);
		this.add(scroll, BorderLayout.CENTER);
	}
	
	public void update(Interior interior)
	{
		Vector<String> tableHeader = new Vector<String>();
		tableHeader.addElement("Index");
		tableHeader.addElement("Detail Level");
		tableHeader.addElement("Min Pixels");
		tableHeader.addElement("Bounding Box");
		tableHeader.addElement("Bounding Sphere");
		tableHeader.addElement("Has Alarm State");
		tableHeader.addElement("Light State Entries");
		tableHeader.addElement("Triggerable Lights");
		tableHeader.addElement("Light State Flags");
		tableHeader.addElement("Coord Bin Mode");
		tableHeader.addElement("Light Map Border Size");
		tableHeader.addElement("Base Ambient");
		tableHeader.addElement("Alarm Ambient");
		
		List<List<String>> data = new ArrayList<List<String>>();
		
		if (interior != null)
		{
			List<String> l = new ArrayList<String>();
			l.add("" + interior.getDetailLevel());
			l.add("" + interior.getMinPixels());
			l.add("" + interior.getBoundingBox());
			l.add("" + interior.getBoundingSphere());
			l.add("" + interior.isHasAlarmState());
			l.add("" + interior.getNumLightStateEntries());
			l.add("" + interior.getNumTriggerableLights());
			l.add("" + interior.getFlags());
			l.add("" + interior.getCoordBinMode());
			l.add("" + interior.getLightMapBorderSize());
			l.add("" + interior.getBaseAmbient());
			l.add("" + interior.getAlarmAmbient());
			data.add(l);
		}
		
		model.setDataVector(Util.stringListToStringVector(data), tableHeader);
	}
}
