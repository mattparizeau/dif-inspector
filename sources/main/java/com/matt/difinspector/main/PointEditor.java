package com.matt.difinspector.main;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.matt.difinspector.interior.InteriorResource;
import com.matt.difinspector.render.EnumSelectionMode;
import com.matt.difinspector.structures.ItrPaddedPoint;

public class PointEditor extends JFrame
{
	private static final long serialVersionUID = -1593084289155588387L;
	
	private InteriorResource	interior;
	private JPanel				panel;
	private JPanel				dataPanel;
	
	private Point3Field			pointField;
	
	private JButton				applyButton;
								
	private int					pointId;
								
	public PointEditor()
	{
		this.setTitle("Point Editor");
		this.setSize(200, 200);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.panel = new JPanel();
		this.dataPanel = new JPanel();
		
		this.pointField = new Point3Field("Position: ");
		
		this.applyButton = new JButton("Apply");
		this.applyButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				PointEditor.this.apply();
			}
		});
		
		this.dataPanel.setPreferredSize(new Dimension(200, 45));
		this.dataPanel.add(this.pointField);
		
		this.setLayout(new FlowLayout());
		this.add(this.panel);
		this.add(this.dataPanel);
		
		this.add(this.applyButton);
		this.setLocationRelativeTo(null);
		
		this.pointId = 0;
	}
	
	@Override
	public void setVisible(boolean b)
	{
		if (b)
		{
			DifInspector.getInstance().getRender().setSelected(EnumSelectionMode.POINTS, this.pointId);
		} else {
			DifInspector.getInstance().getRender().setSelected(EnumSelectionMode.NONE, 0);
		}
		super.setVisible(b);
	}
	
	public void setInterior(InteriorResource interior)
	{
		this.interior = interior;
		//Interior detail = interior.getDetailLevels().get(0);
		
		/*
		 * this.surfaceSelection.removeAllItems(); for (int i = 0; i < detail.getSurfaces().length; i++) {
		 * this.surfaceSelection.addItem("Surface: " + i); }
		 * 
		 * if (this.surfaceSelection.getItemCount() > 0) this.surfaceSelection.setSelectedIndex(0);
		 */
		
		updateDisplay();
	}
	
	public void updateDisplay()
	{
		int pointIndex = this.pointId;
		ItrPaddedPoint point = this.interior.getDetailLevels().get(0).getPoints()[pointIndex];
		
		this.pointField.setPoint(point.getPoint());
	}
	
	private void apply()
	{
		int pointIndex = this.pointId;
		ItrPaddedPoint point = this.interior.getDetailLevels().get(0).getPoints()[pointIndex];
		
		point.setPoint(this.pointField.getPoint());
		
		this.updateDisplay();
	}
	
	public void setPoint(int pointId)
	{
		this.pointId = pointId;
		this.updateDisplay();
	}
}
