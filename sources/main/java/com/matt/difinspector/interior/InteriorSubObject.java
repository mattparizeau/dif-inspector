package com.matt.difinspector.interior;

import java.io.IOException;

import com.matt.difinspector.io.ReverseDataInputStream;
import com.matt.difinspector.io.ReverseDataOutputStream;

public abstract class InteriorSubObject
{
	protected int soKey;
	
	public InteriorSubObject(int soKey)
	{
		this.soKey = soKey;
	}
	
	public static InteriorSubObject read(ReverseDataInputStream dis) throws IOException
	{
		int soKey = dis.readInt();
		if (soKey >= EnumSubObjectType.values().length)
			return null;
		EnumSubObjectType type = EnumSubObjectType.values()[soKey];
		
		InteriorSubObject obj;
		switch (type)
		{
			case MirrorSubObjectKey:
				obj = new MirrorSubObject(soKey);
				break;
			default:
				System.err.println("Bad key in subObject stream!");
				return null;
		}
		
		if (!obj.doRead(dis))
		{
			return null;
		}
		
		return obj;
	}
	
	public boolean write(ReverseDataOutputStream dos) throws IOException
	{
		dos.writeInt(this.soKey);
		
		if (!this.doWrite(dos))
		{
			return false;
		}
		
		return true;
	}
	
	public abstract boolean doRead(ReverseDataInputStream dis) throws IOException;
	public abstract boolean doWrite(ReverseDataOutputStream dos) throws IOException;
	
	@Override
	public String toString()
	{
		return "(InteriorSubObject){soKey: " + this.soKey + "}";
	}
}
