package com.matt.difinspector.interior;

import java.io.BufferedWriter;
import java.io.IOException;

import com.matt.difinspector.io.ReverseDataInputStream;
import com.matt.difinspector.io.ReverseDataOutputStream;
import com.matt.difinspector.materials.MaterialList;
import com.matt.difinspector.math.Box3F;
import com.matt.difinspector.math.MatrixF;
import com.matt.difinspector.math.PlaneF;
import com.matt.difinspector.math.Point2F;
import com.matt.difinspector.math.Point2I;
import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.structures.Primitive;
import com.matt.difinspector.util.Util;

public class InteriorSimpleMesh
{
	private Primitive[] primitives;
	private short[] indices;
	private Point3F[] vertices;
	private Point3F[] normals;
	private Point2F[] diffuseUVs;
	private Point2F[] lightMapUVs;
	private MaterialList materialList;
	
	private boolean hasSolid;
	private boolean hasTranslucency;
	private Box3F bounds;
	private MatrixF transform;
	private Point3F scale;
	
	private InteriorSimpleMesh()
	{
		
	}
	
	public static InteriorSimpleMesh read(ReverseDataInputStream dis) throws IOException
	{
		InteriorSimpleMesh ism = new InteriorSimpleMesh();
		
		// Primitives
		int numPrimitives = dis.readInt();
		ism.primitives = new Primitive[numPrimitives];
		for (int i = 0; i < numPrimitives; i++)
		{
			boolean alpha = dis.readBoolean();
			int texS = dis.readInt();
			int texT = dis.readInt();
			int diffuseIndex = dis.readInt();
			int lightMapIndex = dis.readInt();
			int start = dis.readInt();
			int count = dis.readInt();
			
			PlaneF lightMapEquationX = dis.readPlaneF();
			PlaneF lightMapEquationY = dis.readPlaneF();
			Point2I lightMapOffset = dis.readPoint2I();
			Point2I lightMapSize = dis.readPoint2I();
			ism.primitives[i] = new Primitive(alpha, texS, texT, diffuseIndex, lightMapIndex, start, count, lightMapEquationX, lightMapEquationY, lightMapOffset, lightMapSize);
		}
		
		// Indices
		int numIndices = dis.readInt();
		ism.indices = new short[numIndices];
		for (int i = 0; i < numIndices; i++)
		{
			ism.indices[i] = dis.readShort();
		}
		
		// Vertices
		int numVertices = dis.readInt();
		ism.vertices = new Point3F[numVertices];
		for (int i = 0; i < numVertices; i++)
		{
			ism.vertices[i] = dis.readPoint3F();
		}
		
		// Normals
		int numNormals = dis.readInt();
		ism.normals = new Point3F[numNormals];
		for (int i = 0; i < numNormals; i++)
		{
			ism.normals[i] = dis.readPoint3F();
		}
		
		// Diffuse UVs
		int numDiffuseUVs = dis.readInt();
		ism.diffuseUVs = new Point2F[numDiffuseUVs];
		for (int i = 0; i < numDiffuseUVs; i++)
		{
			ism.diffuseUVs[i] = dis.readPoint2F();
		}
		
		// Lightmap UVs
		int numLightMapUVs = dis.readInt();
		ism.lightMapUVs = new Point2F[numLightMapUVs];
		for (int i = 0; i < numLightMapUVs; i++)
		{
			ism.lightMapUVs[i] = dis.readPoint2F();
		}
		
		// Material List
		boolean hasMaterialList = dis.readBoolean();
		if (hasMaterialList)
		{
			ism.materialList = new MaterialList();
			ism.materialList.read(dis);
		}
		
		// Diffuse bitmaps
		int numDiffuseBitmaps = dis.readInt();
		for (int i = 0; i < numDiffuseBitmaps; i++)
		{
			boolean hasBitmap = dis.readBoolean();
			if (hasBitmap)
			{
				// Unused
				dis.readImage();
			}
		}
		
		ism.hasSolid = dis.readBoolean();
		ism.hasTranslucency = dis.readBoolean();
		ism.bounds = dis.readBox3F();
		ism.transform = dis.readMatrixF();
		ism.scale = dis.readPoint3F();
		
		return ism;
	}
	
	public boolean write(ReverseDataOutputStream dos) throws IOException
	{
		// Primitives
		dos.writeInt(this.primitives.length);
		for (int i = 0; i < this.primitives.length; i++)
		{
			dos.writeBoolean(this.primitives[i].getAlpha());
			dos.writeInt(this.primitives[i].getTexS());
			dos.writeInt(this.primitives[i].getTexT());
			dos.writeInt(this.primitives[i].getDiffuseIndex());
			dos.writeInt(this.primitives[i].getLightMapIndex());
			dos.writeInt(this.primitives[i].getStart());
			dos.writeInt(this.primitives[i].getCount());
			
			dos.writePlaneF(this.primitives[i].getLightMapEquationX());
			dos.writePlaneF(this.primitives[i].getLightMapEquationY());
			dos.writePoint2I(this.primitives[i].getLightMapOffset());
			dos.writePoint2I(this.primitives[i].getLightMapSize());
		}
		
		// Indices
		dos.writeInt(this.indices.length);
		for (int i = 0; i < this.indices.length; i++)
		{
			dos.writeShort(this.indices[i]);
		}
		
		// Vertices
		dos.writeInt(this.vertices.length);
		for (int i = 0; i < this.vertices.length; i++)
		{
			dos.writePoint3F(this.vertices[i]);
		}
		
		// Normals
		dos.writeInt(this.normals.length);
		for (int i = 0; i < this.normals.length; i++)
		{
			dos.writePoint3F(this.normals[i]);
		}
		
		// Diffuse UVs
		dos.writeInt(this.diffuseUVs.length);
		for (int i = 0; i < this.diffuseUVs.length; i++)
		{
			dos.writePoint2F(this.diffuseUVs[i]);
		}
		
		// Lightmap UVs
		dos.writeInt(this.lightMapUVs.length);
		for (int i = 0; i < this.lightMapUVs.length; i++)
		{
			dos.writePoint2F(this.lightMapUVs[i]);
		}
		
		if (this.materialList != null)
		{
			dos.writeBoolean(true);
			this.materialList.write(dos);
		} else {
			dos.writeBoolean(false);
		}
		
		if (this.materialList == null)
		{
			dos.writeInt(0);
		} else {
			dos.writeInt(this.materialList.getMaterials().size());
			
			for (int i = 0; i < this.materialList.getMaterials().size(); i++)
			{
				dos.writeBoolean(false);
			}
		}
		
		dos.writeBoolean(this.hasSolid);
		dos.writeBoolean(this.hasTranslucency);
		dos.writeBox3F(this.bounds);
		dos.writeMatrixF(this.transform);
		dos.writePoint3F(this.scale);
		
		return true;
	}
	
	public void dumpInfo(BufferedWriter bw) throws IOException
	{
		final String TAB = "    ";
		
		// Primitives
		bw.write(TAB + TAB + TAB + "numPrimitives = " + this.primitives.length);
		bw.write(TAB + TAB + TAB + "{\n");
		for (int i = 0; i < this.primitives.length; i++)
		{
			bw.write(TAB + TAB + TAB + TAB + this.primitives[i] + "\n");
		}
		bw.write(TAB + TAB + TAB + "}\n\n");
		
		// Indices
		bw.write(TAB + TAB + TAB + "indices = " + Util.getArrayString(this.indices) + "\n\n");
		
		// Vertices
		bw.write(TAB + TAB + TAB + "numVertices = " + this.vertices.length);
		bw.write(TAB + TAB + TAB + "{\n");
		for (int i = 0; i < this.vertices.length; i++)
		{
			bw.write(TAB + TAB + TAB + TAB + this.vertices[i] + "\n");
		}
		bw.write(TAB + TAB + TAB + "}\n\n");
		
		// Normals
		bw.write(TAB + TAB + TAB + "numNormals = " + this.normals.length);
		bw.write(TAB + TAB + TAB + "{\n");
		for (int i = 0; i < this.normals.length; i++)
		{
			bw.write(TAB + TAB + TAB + TAB + this.normals[i] + "\n");
		}
		bw.write(TAB + TAB + TAB + "}\n\n");
		
		// DiffuseUVs
		bw.write(TAB + TAB + TAB + "numDiffuseUVs = " + this.diffuseUVs.length);
		bw.write(TAB + TAB + TAB + "{\n");
		for (int i = 0; i < this.diffuseUVs.length; i++)
		{
			bw.write(TAB + TAB + TAB + TAB + this.diffuseUVs[i] + "\n");
		}
		bw.write(TAB + TAB + TAB + "}\n\n");
		
		// LightMapUVs
		bw.write(TAB + TAB + TAB + "numLightMapUVs = " + this.lightMapUVs.length);
		bw.write(TAB + TAB + TAB + "{\n");
		for (int i = 0; i < this.lightMapUVs.length; i++)
		{
			bw.write(TAB + TAB + TAB + TAB + this.lightMapUVs[i] + "\n");
		}
		bw.write(TAB + TAB + TAB + "}\n\n");
		
		// MaterialList
		bw.write(TAB + TAB + TAB + "new MaterialList() {\n");
		this.materialList.dumpInfo(bw);
		bw.write(TAB + TAB + TAB + "}\n\n");
		
		// HasSolid
		bw.write(TAB + TAB + TAB + "hasSolid = " + (this.hasSolid ? "true" : "false") + "\n");
		
		// HasTranslucency
		bw.write(TAB + TAB + TAB + "hasTranslucency = " + (this.hasTranslucency ? "true" : "false") + "\n");
		
		// Bounds
		bw.write(TAB + TAB + TAB + "bounds = " + this.bounds + "\n");
		
		// Transform
		bw.write(TAB + TAB + TAB + "transform = " + this.transform + "\n");
		
		// Scale
		bw.write(TAB + TAB + TAB + "scale = " + this.scale + "\n");
	}
}
