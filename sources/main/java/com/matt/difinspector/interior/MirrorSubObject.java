package com.matt.difinspector.interior;

import java.io.IOException;

import com.matt.difinspector.io.ReverseDataInputStream;
import com.matt.difinspector.io.ReverseDataOutputStream;
import com.matt.difinspector.math.Point3F;

public class MirrorSubObject extends InteriorSubObject
{
	private int detailLevel;
	private int zone;
	private float alphaLevel;
	private Point3F centroid;
	private int surfaceCount;
	private int surfaceStart;
	
	public MirrorSubObject(int soKey)
	{
		super(soKey);
	}
	
	@Override
	public boolean doRead(ReverseDataInputStream dis) throws IOException
	{
		this.detailLevel = dis.readInt();
		this.zone = dis.readInt();
		this.alphaLevel = dis.readFloat();
		this.surfaceCount = dis.readInt();
		this.surfaceStart = dis.readInt();
		
		float x = dis.readFloat();
		float y = dis.readFloat();
		float z = dis.readFloat();
		
		this.centroid = new Point3F(x, y, z);
		
		return true;
	}

	@Override
	public boolean doWrite(ReverseDataOutputStream dos) throws IOException
	{
		dos.writeInt(this.detailLevel);
		dos.writeInt(this.zone);
		dos.writeFloat(this.alphaLevel);
		dos.writeInt(this.surfaceCount);
		dos.writeInt(this.surfaceStart);
		
		dos.writeFloat(this.centroid.getX());
		dos.writeFloat(this.centroid.getY());
		dos.writeFloat(this.centroid.getZ());
		
		return true;
	}
	
	public int getDetailLevel()
	{
		return this.detailLevel;
	}
	
	public int getZone()
	{
		return this.zone;
	}
	
	public float getAlphaLevel()
	{
		return this.alphaLevel;
	}
	
	public Point3F getCentroid()
	{
		return this.centroid;
	}
	
	public int getSurfaceCount()
	{
		return this.surfaceCount;
	}
	
	public int getSurfaceStart()
	{
		return this.surfaceStart;
	}
	@Override
	public String toString()
	{
		return "(MirrorSubObject){soKey: " + this.soKey + ", detailLevel: " + this.detailLevel + ", zone: " + this.zone + ", alphaLevel: " + this.alphaLevel + ", centroid: " + this.centroid + ", surfaceCount: " + this.surfaceCount + ", surfaceStart: " + this.surfaceStart + "}";
	}

}
