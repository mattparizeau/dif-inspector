package com.matt.difinspector.interior;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.matt.difinspector.io.ReverseDataInputStream;
import com.matt.difinspector.io.ReverseDataOutputStream;

public class InteriorDictionary
{
	private List<InteriorDictEntry> entries;
	
	public InteriorDictionary()
	{
		this.entries = new ArrayList<InteriorDictEntry>();
	}
	
	public boolean read(ReverseDataInputStream dis) throws IOException
	{
		int size = dis.readInt();
		for (int i = 0; i < size; i++)
		{
			String name = dis.readString();
			String value = dis.readString();
			this.entries.add(new InteriorDictEntry(name, value));
		}
		
		return true;
	}
	
	public boolean write(ReverseDataOutputStream dos) throws IOException
	{
		dos.writeInt(this.entries.size());
		for (int i = 0; i < this.entries.size(); i++)
		{
			dos.writeString(this.entries.get(i).getName());
			dos.writeString(this.entries.get(i).getValue());
		}
		
		return true;
	}
	
	public void dumpInfo(BufferedWriter bw) throws IOException
	{
		final String TAB = "    ";
		
		bw.write(TAB + TAB + "numEntries = " + this.entries.size() + "\n");
		for (int i = 0; i < this.entries.size(); i++)
		{
			InteriorDictEntry entry = this.entries.get(i);
			bw.write(TAB + TAB + "entry[" + i + "] = new Entry(){name: " + entry.getName() + ", value: " + entry.getValue() + "}\n");
		}
	}
}
