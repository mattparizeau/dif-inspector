package com.matt.difinspector.interior;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.matt.difinspector.io.ReverseDataInputStream;
import com.matt.difinspector.io.ReverseDataOutputStream;
import com.matt.difinspector.map.Brush;
import com.matt.difinspector.map.BrushPlane;
import com.matt.difinspector.materials.MaterialList;
import com.matt.difinspector.materials.TexGenList;
import com.matt.difinspector.math.Box3F;
import com.matt.difinspector.math.PlaneF;
import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.math.SphereF;
import com.matt.difinspector.math.TriangleF;
import com.matt.difinspector.models.OBJModel;
import com.matt.difinspector.models.OBJModel.OBJIndex;
import com.matt.difinspector.structures.AnimatedLight;
import com.matt.difinspector.structures.BSPNode;
import com.matt.difinspector.structures.BSPSolidLeaf;
import com.matt.difinspector.structures.ColorI;
import com.matt.difinspector.structures.ConvexHull;
import com.matt.difinspector.structures.CoordBin;
import com.matt.difinspector.structures.Edge;
import com.matt.difinspector.structures.ItrPaddedPoint;
import com.matt.difinspector.structures.LightState;
import com.matt.difinspector.structures.LightStateData;
import com.matt.difinspector.structures.NullSurface;
import com.matt.difinspector.structures.Portal;
import com.matt.difinspector.structures.Surface;
import com.matt.difinspector.structures.TexGenPlanes;
import com.matt.difinspector.structures.TexMatrix;
import com.matt.difinspector.structures.TriFan;
import com.matt.difinspector.structures.Zone;
import com.matt.difinspector.util.Util;
import com.matt.jmatt.lists.ErrorList;

public class Interior
{
	private int						detailLevel;
	private int						minPixels;
	private Box3F					boundingBox;
	private SphereF					boundingSphere;
	private boolean					hasAlarmState;
	private int						numLightStateEntries;
	private PlaneF[]				planes;
	private ItrPaddedPoint[]		points;
	private byte[]					pointVisibility;
	private TexGenList				texGenEQs;
	private BSPNode[]				bspNodes;
	private BSPSolidLeaf[]			bspSolidLeaves;
	private MaterialList			materialList;
	private int[]					windings;
	private TriFan[]				windingIndices;
	private Edge[]					edges;
	private Zone[]					zones;
	private short[]					zoneSurfaces;
	private int[]					zoneStaticMeshes;
	private short[]					zonePortalList;
	private Portal[]				portals;
	private Surface[]				surfaces;
	private TexGenPlanes[]			lmTexGenEQs;
	private int[]					normalLMapIndices;
	private int[]					alarmLMapIndices;
	private NullSurface[]			nullSurfaces;
	private BufferedImage[]			lightMaps;
	private BufferedImage[]			lightDirMaps;
	private boolean[]				lightMapKeep;
	private int[]					solidLeafSurfaces;
	private int						numTriggerableLights;
	private AnimatedLight[]			animatedLights;
	private LightState[]			lightStates;
	private LightStateData[]		lightStateData;
	private byte[]					stateDataBuffer;
	private int						flags;
	private char[]					nameBuffer;
	private InteriorSubObject[]		subObjects;
	private ConvexHull[]			convexHulls;
	private byte[]					convexHullEmitStrings;
	private int[]					hullIndices;
	private short[]					hullPlaneIndices;
	private int[]					hullEmitStringIndices;
	private int[]					hullSurfaceIndices;
	private short[]					polyListPlanes;
	private int[]					polyListPoints;
	private byte[]					polyListStrings;
	private CoordBin[]				coordBins;
	private short[]					coordBinIndices;
	private int						coordBinMode;
	private ColorI					baseAmbient;
	private ColorI					alarmAmbient;
	private InteriorSimpleMesh[]	staticMeshes;
	private Point3F[]				normals;
	private TexMatrix[]				texMatrices;
	private int[]					texMatIndices;
	private int						lightMapBorderSize;
	private ConvexHull[]			vehicleConvexHulls;
	private byte[]					vehicleConvexHullEmitStrings;
	private int[]					vehicleHullIndices;
	private short[]					vehicleHullPlaneIndices;
	private int[]					vehicleHullEmitStringIndices;
	private int[]					vehicleHullSurfaceIndices;
	private short[]					vehiclePolyListPlanes;
	private int[]					vehiclePolyListPoints;
	private byte[]					vehiclePolyListStrings;
	private NullSurface[]			vehicleNullSurfaces;
	private ItrPaddedPoint[]		vehiclePoints;
	private PlaneF[]				vehiclePlanes;
	private int[]					vehicleWindings;
	private TriFan[]				vehicleWindingIndices;
	private int						fileVersion;
	private List<Short>				normalIndices;
	private Point3F[]				planeNormals;
	private Point3F[]				normalsLegacy;
	private ErrorList				errors;
	// private PlaneIndex[] planeIndices;
	
	public Interior()
	{
		this.errors = new ErrorList();
	}
	
	public boolean read(ReverseDataInputStream dis) throws IOException
	{
		int fileVersion = dis.readInt();
		this.fileVersion = fileVersion;
		
		if (fileVersion > 14)
		{
			System.err.println("Incompatible detail version found.");
			return false;
		}
		
		// Geometry Factors
		this.detailLevel = dis.readInt();
		
		this.minPixels = dis.readInt();
		this.boundingBox = dis.readBox3F();
		this.boundingSphere = dis.readSphereF();
		this.hasAlarmState = dis.readBoolean();
		this.numLightStateEntries = dis.readInt();
		
		// Planes
		this.readPlaneVector(dis);
		
		// Points
		int numPoints = dis.readInt();
		this.points = new ItrPaddedPoint[numPoints];
		for (int i = 0; i < numPoints; i++)
		{
			Point3F point = dis.readPoint3F();
			this.points[i] = new ItrPaddedPoint(point);
		}
		
		// Point Visibility
		if (fileVersion == 4)
		{
			this.pointVisibility = new byte[0];
		} else
		{
			int numPointVisibilities = dis.readInt();
			this.pointVisibility = new byte[numPointVisibilities];
			for (int i = 0; i < numPointVisibilities; i++)
			{
				this.pointVisibility[i] = dis.readByte();
			}
		}
		
		// TexGenEQs
		int numTexGenEQs = dis.readInt();
		this.texGenEQs = new TexGenList();// new TexGenPlanes[numTexGenEQs];
		for (int i = 0; i < numTexGenEQs; i++)
		{
			PlaneF planeX = dis.readPlaneF();
			PlaneF planeY = dis.readPlaneF();
			// this.texGenEQs[i] = new TexGenPlanes(planeX, planeY);
			this.texGenEQs.add(new TexGenPlanes(planeX, planeY));
		}
		
		// BSPNodes
		int numBSPNodes = dis.readInt();
		this.bspNodes = new BSPNode[numBSPNodes];
		for (int i = 0; i < numBSPNodes; i++)
		{
			short planeIndex = dis.readShort();
			int frontIndex;
			int backIndex;
			
			if (fileVersion >= 14)
			{
				frontIndex = dis.readInt();
				backIndex = dis.readInt();
			} else
			{
				frontIndex = dis.readShort();
				backIndex = dis.readShort();
			}
			this.bspNodes[i] = new BSPNode(planeIndex, frontIndex, backIndex);
		}
		
		// BSPSolidLeaves
		int numBSPSolidLeaves = dis.readInt();
		this.bspSolidLeaves = new BSPSolidLeaf[numBSPSolidLeaves];
		for (int i = 0; i < numBSPSolidLeaves; i++)
		{
			int surfaceIndex = dis.readInt();
			short surfaceCount = dis.readShort();
			this.bspSolidLeaves[i] = new BSPSolidLeaf(surfaceIndex, surfaceCount);
		}
		
		// MaterialList
		this.materialList = new MaterialList();
		this.materialList.read(dis);
		
		// Windings
		boolean readWindingsAlt = false;
		// byte numWindingsAlt = 0;
		int numWindings = dis.readInt();
		if ((numWindings & 0x80000000) != 0)
		{
			numWindings ^= 0x80000000;
			readWindingsAlt = true;
			/* numWindingsAlt = */ dis.readByte();
		}
		this.windings = new int[numWindings];
		for (int i = 0; i < numWindings; i++)
		{
			if (readWindingsAlt)
			{
				this.windings[i] = dis.readShort();
			} else
			{
				this.windings[i] = dis.readInt();
			}
		}
		
		// Winding Indices
		int numWindingIndices = dis.readInt();
		this.windingIndices = new TriFan[numWindingIndices];
		for (int i = 0; i < numWindingIndices; i++)
		{
			int windingStart = dis.readInt();
			int windingCount = dis.readInt();
			
			this.windingIndices[i] = new TriFan(windingStart, windingCount);
		}
		
		// 0x1E91A
		
		// Edges
		if (fileVersion >= 12)
		{
			int numEdges = dis.readInt();
			this.edges = new Edge[numEdges];
			for (int i = 0; i < numEdges; i++)
			{
				int vertex1 = dis.readInt();
				int vertex2 = dis.readInt();
				int face1 = dis.readInt();
				int face2 = dis.readInt();
				
				this.edges[i] = new Edge(vertex1, vertex2, face1, face2);
			}
		}
		
		// Zones
		int numZones = dis.readInt();
		this.zones = new Zone[numZones];
		for (int i = 0; i < numZones; i++)
		{
			short portalStart = dis.readShort();
			short portalCount = dis.readShort();
			
			int surfaceStart = dis.readInt();
			short surfaceCount = dis.readShort();
			
			int planeStart = 0;
			short planeCount = 0;
			
			int staticMeshStart = 0;
			int staticMeshCount = 0;
			
			if (fileVersion >= 12)
			{
				staticMeshStart = dis.readInt();
				staticMeshCount = dis.readInt();
			}
			
			short flags = dis.readShort();
			short zoneId = 0;
			
			this.zones[i] = new Zone(portalStart, portalCount, surfaceStart, planeStart, surfaceCount, planeCount,
					staticMeshStart, staticMeshCount, flags, zoneId);
		}
		
		// 0x1E92A
		
		// Zone surfaces
		// boolean readZoneSurfacesAlt = false;
		// byte numZoneSurfacesAlt = 0;
		int numZoneSurfaces = dis.readInt();
		if ((numZoneSurfaces & 0x80000000) != 0)
		{
			numZoneSurfaces ^= 0x80000000;
			// readZoneSurfacesAlt = true;
			/* numZoneSurfacesAlt = */ dis.readByte();
		}
		this.zoneSurfaces = new short[numZoneSurfaces];
		for (int i = 0; i < numZoneSurfaces; i++)
		{
			this.zoneSurfaces[i] = dis.readShort();
		}
		
		// Zone static meshes
		if (fileVersion >= 12)
		{
			int numZoneStaticMeshes = dis.readInt();
			this.zoneStaticMeshes = new int[numZoneStaticMeshes];
			for (int i = 0; i < numZoneStaticMeshes; i++)
			{
				this.zoneStaticMeshes[i] = dis.readInt();
			}
		}
		
		// ZonePortalList
		// boolean readZonePortalListAlt = false;
		// byte numZonePortalListAlt = 0;
		int numZonePortalList = dis.readInt();
		if ((numZonePortalList & 0x80000000) != 0)
		{
			numZonePortalList ^= 0x80000000;
			// readZonePortalListAlt = true;
			/* numZonePortalListAlt = */ dis.readByte();
		}
		this.zonePortalList = new short[numZonePortalList];
		for (int i = 0; i < numZonePortalList; i++)
		{
			this.zonePortalList[i] = dis.readShort();
		}
		
		// Portals
		int numPortals = dis.readInt();
		this.portals = new Portal[numPortals];
		for (int i = 0; i < numPortals; i++)
		{
			short planeIndex = dis.readShort();
			short triFanCount = dis.readShort();
			int triFanStart = dis.readInt();
			short zoneFront = dis.readShort();
			short zoneBack = dis.readShort();
			
			this.portals[i] = new Portal(planeIndex, triFanCount, triFanStart, zoneFront, zoneBack);
		}
		
		boolean tgeInterior = false;
		
		dis.mark(32768);
		
		// 0x1F440
		// s = 0x2B
		
		// Surfaces
		int numSurfaces = dis.readInt();
		this.surfaces = new Surface[numSurfaces];
		this.lmTexGenEQs = new TexGenPlanes[numSurfaces];
		for (int i = 0; i < numSurfaces; i++)
		{
			this.lmTexGenEQs[i] = new TexGenPlanes();
			this.surfaces[i] = this.readSurface(dis, this.lmTexGenEQs[i], false);
			if (this.surfaces[i] == null)
			{
				tgeInterior = true;
				break;
			}
		}
		
		// 0x2E197
		
		if (fileVersion == 0 && tgeInterior)
		{
			dis.reset();
			// Surfaces
			numSurfaces = dis.readInt();
			this.surfaces = new Surface[numSurfaces];
			this.lmTexGenEQs = new TexGenPlanes[numSurfaces];
			for (int i = 0; i < numSurfaces; i++)
			{
				this.lmTexGenEQs[i] = new TexGenPlanes();
				this.surfaces[i] = this.readSurface(dis, this.lmTexGenEQs[i], true);
				if (this.surfaces[i] == null)
				{
					return false;
				}
			}
		} else if (fileVersion != 0 && tgeInterior)
		{
			return false;
		}
		
		// Edges
		// Looking at MBU in IDA reveals that these don't exist in version 0 OR 1.
		if (fileVersion > 1 && fileVersion <= 5)
		{
			int numEdges = dis.readInt();
			this.edges = new Edge[numEdges];
			for (int i = 0; i < numEdges; i++)
			{
				int vertex1 = dis.readInt();
				int vertex2 = dis.readInt();
				int normal1 = dis.readInt();
				int normal2 = dis.readInt();
				int face1 = 0;
				int face2 = 0;
				
				if (fileVersion > 2) // version 3 is where surface id's get added
				{
					face1 = dis.readInt();
					face2 = dis.readInt();
				}
				this.edges[i] = new Edge(vertex1, vertex2, face1, face2, normal1, normal2);
			}
		}
		
		// Normals
		if (fileVersion == 4 || fileVersion == 5)
		{
			// boolean normalsAlt = false;
			// byte numNormalsAlt = 0;
			int numNormals = dis.readInt();
			// System.out.println("0x" + Integer.toHexString(numNormals));
			if ((numNormals & 0x80000000) != 0)
			{
				numNormals ^= 0x80000000;
				// normalsAlt = true;
				/* numNormalsAlt = */ dis.readByte();
			}
			this.normalsLegacy = new Point3F[numNormals];
			for (int i = 0; i < numNormals; i++)
			{
				this.normalsLegacy[i] = dis.readPoint3F();
			}
			
			// NormalIndices
			boolean normalIndicesAlt = false;
			byte normalIndicesParam = 0;
			int numNormalIndices = dis.readInt();
			if ((numNormalIndices & 0x80000000) != 0)
			{
				numNormalIndices ^= 0x80000000;
				normalIndicesAlt = true;
				normalIndicesParam = dis.readByte();
			}
			normalIndices = new ArrayList<Short>();
			for (int i = 0; i < numNormalIndices; i++)
			{
				if (normalIndicesAlt && normalIndicesParam == 0)
				{
					normalIndices.add((short) dis.readByte());
				} else
				{
					normalIndices.add(dis.readShort());
				}
			}
		}
		
		// NormalLMapIndices
		// boolean readNormalLMapIndicesAlt = false;
		// byte numNormalLMapIndicesAlt = 0;
		int numNormalLMapIndices = dis.readInt();
		if ((numNormalLMapIndices & 0x80000000) != 0)
		{
			numNormalLMapIndices ^= 0x80000000;
			// readNormalLMapIndicesAlt = true;
			/* numNormalLMapIndicesAlt = */ dis.readByte();
		}
		this.normalLMapIndices = new int[numNormalLMapIndices];
		for (int i = 0; i < numNormalLMapIndices; i++)
		{
			if (fileVersion >= 13)
			{
				this.normalLMapIndices[i] = dis.readInt();
			} else
			{
				this.normalLMapIndices[i] = dis.readByte();
			}
		}
		
		// AlarmLMapIndices
		if (fileVersion == 4)
		{
			this.alarmLMapIndices = new int[0];
		} else
		{
			int numAlarmLMapIndices = dis.readInt();
			this.alarmLMapIndices = new int[numAlarmLMapIndices];
			for (int i = 0; i < numAlarmLMapIndices; i++)
			{
				if (fileVersion >= 13)
				{
					this.alarmLMapIndices[i] = dis.readInt();
				} else
				{
					this.alarmLMapIndices[i] = dis.readByte();
				}
			}
		}
		
		// NullSurfaces
		int numNullSurfaces = dis.readInt();
		this.nullSurfaces = new NullSurface[numNullSurfaces];
		for (int i = 0; i < numNullSurfaces; i++)
		{
			int windingStart = dis.readInt();
			short planeIndex = dis.readShort();
			byte surfaceFlags = dis.readByte();
			int windingCount;
			
			if (fileVersion >= 13)
			{
				windingCount = dis.readInt();
			} else
			{
				windingCount = dis.readByte();
			}
			
			this.nullSurfaces[i] = new NullSurface(windingStart, planeIndex, surfaceFlags, windingCount);
		}
		
		// Lightmaps
		if (fileVersion == 4)
		{
			this.lightMaps = new BufferedImage[0];
			this.lightDirMaps = new BufferedImage[0];
			this.lightMapKeep = new boolean[0];
		} else
		{
			int numLightMaps = dis.readInt();
			this.lightMaps = new BufferedImage[numLightMaps];
			this.lightDirMaps = new BufferedImage[numLightMaps];
			this.lightMapKeep = new boolean[numLightMaps];
			for (int i = 0; i < numLightMaps; i++)
			{
				this.lightMaps[i] = dis.readImage();
				
				if (!tgeInterior && (fileVersion >= 0 || fileVersion <= 5 || fileVersion >= 12))
				{
					this.lightDirMaps[i] = dis.readImage();
				}
				
				this.lightMapKeep[i] = dis.readBoolean();
			}
		}
		
		// SolidLeafSurfaces
		boolean readSolidLeafSurfaces2 = false;
		// byte readSolidLeafSurfacesParam = 0;
		int numSolidLeafSurfaces = dis.readInt();
		if ((numSolidLeafSurfaces & 0x80000000) != 0)
		{
			numSolidLeafSurfaces ^= 0x80000000;
			readSolidLeafSurfaces2 = true;
			/* readSolidLeafSurfacesParam = */ dis.readByte();
		}
		this.solidLeafSurfaces = new int[numSolidLeafSurfaces];
		for (int i = 0; i < numSolidLeafSurfaces; i++)
		{
			if (readSolidLeafSurfaces2)
			{
				this.solidLeafSurfaces[i] = dis.readShort();
			} else
			{
				this.solidLeafSurfaces[i] = dis.readInt();
			}
		}
		
		// Animated Lights
		this.numTriggerableLights = 0;
		int numAnimatedLights = dis.readInt();
		this.animatedLights = new AnimatedLight[numAnimatedLights];
		for (int i = 0; i < numAnimatedLights; i++)
		{
			int nameIndex = dis.readInt();
			int stateIndex = dis.readInt();
			short stateCount = dis.readShort();
			short flags = dis.readShort();
			int duration = dis.readInt();
			
			if ((flags & EnumLightFlags.AnimationAmbient.getValue()) == 0)
				this.numTriggerableLights++;
				
			this.animatedLights[i] = new AnimatedLight(nameIndex, stateIndex, stateCount, flags, duration);
		}
		
		// Light States
		int numLightStates = dis.readInt();
		this.lightStates = new LightState[numLightStates];
		for (int i = 0; i < numLightStates; i++)
		{
			byte red = dis.readByte();
			byte green = dis.readByte();
			byte blue = dis.readByte();
			int activeTime = dis.readInt();
			int dataIndex = dis.readInt();
			short dataCount = dis.readShort();
			
			this.lightStates[i] = new LightState(red, green, blue, activeTime, dataIndex, dataCount);
		}
		
		if (fileVersion == 4)
		{
			this.lightStateData = new LightStateData[0];
			this.stateDataBuffer = new byte[0];
			this.nameBuffer = new char[0];
			this.subObjects = new InteriorSubObject[0];
		} else
		{
			// StateData
			int numLightStateData = dis.readInt();
			this.lightStateData = new LightStateData[numLightStateData];
			for (int i = 0; i < numLightStateData; i++)
			{
				int surfaceIndex = dis.readInt();
				int mapIndex = dis.readInt();
				short lightStateIndex = dis.readShort();
				
				this.lightStateData[i] = new LightStateData(surfaceIndex, mapIndex, lightStateIndex);
			}
			
			// StateDataBuffer
			int numStateDataBuffer = dis.readInt();
			this.stateDataBuffer = new byte[numStateDataBuffer];
			this.flags = dis.readInt();
			for (int i = 0; i < numStateDataBuffer; i++)
			{
				this.stateDataBuffer[i] = dis.readByte();
			}
			
			// NameBuffer
			int numNameBuffer = dis.readInt();
			this.nameBuffer = new char[numNameBuffer];
			for (int i = 0; i < numNameBuffer; i++)
			{
				this.nameBuffer[i] = dis.readChar();
			}
			
			// SubObjects
			int numSubObjects = dis.readInt();
			this.subObjects = new InteriorSubObject[numSubObjects];
			for (int i = 0; i < numSubObjects; i++)
			{
				InteriorSubObject subObject = InteriorSubObject.read(dis);
				if (subObject == null)
				{
					throw new IOException("Error, bad sub object in stream!");
				}
				this.subObjects[i] = subObject;
			}
		}
		
		// Convex Hulls
		int numConvexHulls = dis.readInt();
		this.convexHulls = new ConvexHull[numConvexHulls];
		for (int i = 0; i < numConvexHulls; i++)
		{
			int hullStart = dis.readInt();
			short hullCount = dis.readShort();
			float minX = dis.readFloat();
			float maxX = dis.readFloat();
			float minY = dis.readFloat();
			float maxY = dis.readFloat();
			float minZ = dis.readFloat();
			float maxZ = dis.readFloat();
			int surfaceStart = dis.readInt();
			short surfaceCount = dis.readShort();
			int planeStart = dis.readInt();
			int polyListPlaneStart = dis.readInt();
			int polyListPointStart = dis.readInt();
			int polyListStringStart = dis.readInt();
			boolean staticMesh;
			
			if (fileVersion >= 12)
			{
				staticMesh = dis.readBoolean();
			} else
			{
				staticMesh = false;
			}
			
			this.convexHulls[i] = new ConvexHull(minX, maxX, minY, maxY, minZ, maxZ, hullStart, surfaceStart,
					planeStart, hullCount, surfaceCount, polyListPlaneStart, polyListPointStart, polyListStringStart,
					(short) 0, staticMesh);
		}
		
		// Convex Hull Emit Strings
		int numConvexHullEmitStrings = dis.readInt();
		this.convexHullEmitStrings = new byte[numConvexHullEmitStrings];
		for (int i = 0; i < numConvexHullEmitStrings; i++)
		{
			this.convexHullEmitStrings[i] = dis.readByte();
		}
		
		// Hull indices
		boolean readHullIndicesAlt = false;
		// byte numHullIndicesAlt = 0;
		int numHullIndices = dis.readInt();
		if ((numHullIndices & 0x80000000) != 0)
		{
			numHullIndices ^= 0x80000000;
			readHullIndicesAlt = true;
			/* numHullIndicesAlt = */ dis.readByte();
		}
		this.hullIndices = new int[numHullIndices];
		for (int i = 0; i < numHullIndices; i++)
		{
			if (readHullIndicesAlt)
			{
				this.hullIndices[i] = dis.readShort();
			} else
			{
				this.hullIndices[i] = dis.readInt();
			}
		}
		
		// Hull plane indices
		// boolean readHullPlaneIndicesAlt = false;
		// byte numHullPlaneIndicesAlt = 0;
		int numHullPlaneIndices = dis.readInt();
		if ((numHullPlaneIndices & 0x80000000) != 0)
		{
			numHullPlaneIndices ^= 0x80000000;
			// readHullPlaneIndicesAlt = true;
			/* numHullPlaneIndicesAlt = */ dis.readByte();
		}
		this.hullPlaneIndices = new short[numHullPlaneIndices];
		for (int i = 0; i < numHullPlaneIndices; i++)
		{
			this.hullPlaneIndices[i] = dis.readShort();
		}
		
		// Hull emit string indices
		boolean readHullEmitStringIndicesAlt = false;
		// byte numHullEmitStringIndicesAlt = 0;
		int numHullEmitStringIndices = dis.readInt();
		if ((numHullEmitStringIndices & 0x80000000) != 0)
		{
			numHullEmitStringIndices ^= 0x80000000;
			readHullEmitStringIndicesAlt = true;
			/* numHullEmitStringIndicesAlt = */ dis.readByte();
		}
		this.hullEmitStringIndices = new int[numHullEmitStringIndices];
		for (int i = 0; i < numHullEmitStringIndices; i++)
		{
			if (readHullEmitStringIndicesAlt)
			{
				this.hullEmitStringIndices[i] = dis.readShort();
			} else
			{
				this.hullEmitStringIndices[i] = dis.readInt();
			}
		}
		
		// Hull surface indices
		boolean readHullSurfaceIndicesAlt = false;
		// byte numHullSurfaceIndicesAlt = 0;
		int numHullSurfaceIndices = dis.readInt();
		if ((numHullSurfaceIndices & 0x80000000) != 0)
		{
			numHullSurfaceIndices ^= 0x80000000;
			readHullSurfaceIndicesAlt = true;
			/* numHullSurfaceIndicesAlt = */ dis.readByte();
		}
		this.hullSurfaceIndices = new int[numHullSurfaceIndices];
		for (int i = 0; i < numHullSurfaceIndices; i++)
		{
			if (readHullSurfaceIndicesAlt)
			{
				this.hullSurfaceIndices[i] = dis.readShort();
				if ((this.hullSurfaceIndices[i] & 0x8000) != 0)
					this.hullSurfaceIndices[i] ^= 0x8000;
			} else
			{
				this.hullSurfaceIndices[i] = dis.readInt();
				if ((this.hullSurfaceIndices[i] & 0x80000000) != 0)
					this.hullSurfaceIndices[i] ^= 0x80000000;
			}
		}
		
		// PolyList planes
		// boolean readPolyListPlanesAlt = false;
		// byte numPolyListPlanesAlt = 0;
		int numPolyListPlanes = dis.readInt();
		if ((numPolyListPlanes & 0x80000000) != 0)
		{
			numPolyListPlanes ^= 0x80000000;
			// readPolyListPlanesAlt = true;
			/* numPolyListPlanesAlt = */ dis.readByte();
		}
		this.polyListPlanes = new short[numPolyListPlanes];
		for (int i = 0; i < numPolyListPlanes; i++)
		{
			this.polyListPlanes[i] = dis.readShort();
		}
		
		// PolyList points
		boolean readPolyListPointsAlt = false;
		// byte numPolyListPointsAlt = 0;
		int numPolyListPoints = dis.readInt();
		if ((numPolyListPoints & 0x80000000) != 0)
		{
			numPolyListPoints ^= 0x80000000;
			readPolyListPointsAlt = true;
			/* numPolyListPointsAlt = */ dis.readByte();
		}
		this.polyListPoints = new int[numPolyListPoints];
		for (int i = 0; i < numPolyListPoints; i++)
		{
			if (readPolyListPointsAlt)
			{
				this.polyListPoints[i] = dis.readShort();
			} else
			{
				this.polyListPoints[i] = dis.readInt();
			}
		}
		
		// PolyList strings
		int numPolyListStrings = dis.readInt();
		this.polyListStrings = new byte[numPolyListStrings];
		for (int i = 0; i < numPolyListStrings; i++)
		{
			this.polyListStrings[i] = dis.readByte();
		}
		
		final int numCoordBins = 16;
		
		// Coord Bins
		this.coordBins = new CoordBin[numCoordBins * numCoordBins];
		for (int i = 0; i < numCoordBins * numCoordBins; i++)
		{
			int binStart = dis.readInt();
			int binCount = dis.readInt();
			
			this.coordBins[i] = new CoordBin(binStart, binCount);
		}
		
		// Coord Bin Indices
		// boolean readCoordBinIndicesAlt = false;
		// byte numCoordBinIndicesAlt = 0;
		int numCoordBinIndices = dis.readInt();
		if ((numCoordBinIndices & 0x80000000) != 0)
		{
			numCoordBinIndices ^= 0x80000000;
			// readCoordBinIndicesAlt = true;
			/* numCoordBinIndicesAlt = */ dis.readByte();
		}
		this.coordBinIndices = new short[numCoordBinIndices];
		for (int i = 0; i < numCoordBinIndices; i++)
		{
			this.coordBinIndices[i] = dis.readShort();
		}
		
		// Coord Bin Mode
		this.coordBinMode = dis.readInt();
		
		if (fileVersion == 4)
		{
			this.baseAmbient = new ColorI((byte) 0, (byte) 0, (byte) 0, (byte) 255);
			this.alarmAmbient = new ColorI((byte) 0, (byte) 0, (byte) 0, (byte) 255);
			this.staticMeshes = new InteriorSimpleMesh[0];
			this.texMatrices = new TexMatrix[0];
			this.texMatIndices = new int[0];
		} else
		{
			// Ambient Colors
			this.baseAmbient = dis.readColorI();
			this.alarmAmbient = dis.readColorI();
			
			if (fileVersion >= 10)
			{
				// Static Meshes
				int numStaticMeshes = dis.readInt();
				// System.out.println("numStaticMeshes: " + numStaticMeshes);
				this.staticMeshes = new InteriorSimpleMesh[numStaticMeshes];
				for (int i = 0; i < numStaticMeshes; i++)
				{
					this.staticMeshes[i] = InteriorSimpleMesh.read(dis);
				}
			}
			
			if (fileVersion >= 11)
			{
				// Normals
				int numNormals = dis.readInt();
				// System.out.println("numNormals: " + numNormals);
				this.normals = new Point3F[numNormals];
				for (int i = 0; i < numNormals; i++)
				{
					this.normals[i] = dis.readPoint3F();
				}
				
				// TexMatrices
				int numTexMatrices = dis.readInt();
				// System.out.println("numTexMatrices: " + numTexMatrices);
				this.texMatrices = new TexMatrix[numTexMatrices];
				for (int i = 0; i < numTexMatrices; i++)
				{
					int T = dis.readInt();
					int N = dis.readInt();
					int B = dis.readInt();
					this.texMatrices[i] = new TexMatrix(T, N, B);
				}
				
				// TexMatIndices
				int numTexMatIndices = dis.readInt();
				// System.out.println("numTexMatIndices: " + numTexMatIndices);
				this.texMatIndices = new int[numTexMatIndices];
				for (int i = 0; i < numTexMatIndices; i++)
				{
					this.texMatIndices[i] = dis.readInt();
				}
			}
			
			// For future expandability
			int dummy;
			if (fileVersion < 10)
			{
				dummy = dis.readInt();
				if (dummy != 0)
				{
					return false;
				}
			}
			
			if (fileVersion < 11)
			{
				dummy = dis.readInt();
				if (dummy != 0)
				{
					return false;
				}
				
				dummy = dis.readInt();
				if (dummy != 0)
				{
					return false;
				}
			}
			
			//
			// Support for interior light map border sizes.
			//
			
			int extendedLightMapData = dis.readInt();
			if (extendedLightMapData == 1)
			{
				this.lightMapBorderSize = dis.readInt();
				
				// future expansion under current block (avoid using too
				// many of the above expansion slots by allowing nested
				// blocks)...
				dummy = dis.readInt();
				if (dummy != 0)
				{
					return false;
				}
			}
		}
		
		processData();
		
		return true;
	}
	
	public void processData()
	{
		for (Surface surface : this.surfaces)
		{
			String texture = this.materialList.getMaterials().get(surface.getTextureIndex()).getName();
			TexGenPlanes texGenOriginal = this.texGenEQs.get(surface.getTexGenIndex());
			PlaneF planeXOriginal = texGenOriginal.getPlaneX();
			PlaneF planeYOriginal = texGenOriginal.getPlaneY();
			
			PlaneF planeX = new PlaneF(planeXOriginal.getX(), planeXOriginal.getY(), planeXOriginal.getZ(),
					planeXOriginal.getD());
			PlaneF planeY = new PlaneF(planeYOriginal.getX(), planeYOriginal.getY(), planeYOriginal.getZ(),
					planeYOriginal.getD());
			TexGenPlanes texGen = new TexGenPlanes(planeX, planeY);
			
			surface.setTexture(texture);
			surface.setTexGen(texGen);
		}
	}
	
	public void prepareData()
	{
		if (this.boundingBox == null)
			this.boundingBox = new Box3F(new Point3F(), new Point3F());
		if (this.boundingSphere == null)
			this.boundingSphere = new SphereF(new Point3F(), 0);
		if (this.texGenEQs == null)
			this.texGenEQs = new TexGenList();
		this.texGenEQs.clear();
		
		if (this.materialList == null)
			this.materialList = new MaterialList();
		this.materialList.getMaterials().clear();
		this.materialList.addMaterial("NULL");
		this.materialList.addMaterial("ORIGIN");
		this.materialList.addMaterial("TRIGGER");
		this.materialList.addMaterial("EMITTER");
		
		if (this.surfaces == null)
			this.surfaces = new Surface[0];
		for (int i = 0; i < this.surfaces.length; i++)
		{
			String texture = this.surfaces[i].getTexture().toLowerCase();
			if (!this.materialList.contains(texture))
				this.materialList.addMaterial(texture);
				
			this.surfaces[i].setTextureIndex(this.materialList.indexOf(texture));
			
			TexGenPlanes texGen = this.surfaces[i].getTexGen();
			if (!this.texGenEQs.contains(texGen))
				this.texGenEQs.add(texGen);
				
			this.surfaces[i].setTexGenIndex(this.texGenEQs.indexOf(texGen));
		}
		
		this.recalculateBounds();
	}
	
	public void recalculateBounds()
	{
		Point3F min = new Point3F((float)1e10, (float)1e10, (float)1e10);
		Point3F max = new Point3F((float)-1e10, (float)-1e10, (float)-1e10);
		
		this.boundingBox = new Box3F(min, max);
		
		if (this.convexHulls == null)
			this.convexHulls = new ConvexHull[0];
		for (int i = 0; i < this.convexHulls.length; i++)
		{
			ConvexHull hull = this.convexHulls[i];
			
			Point3F minHull = new Point3F((float)1e10, (float)1e10, (float)1e10);
			Point3F maxHull = new Point3F((float)-1e10, (float)-1e10, (float)-1e10);
			
			int surfaceStart = hull.getSurfaceStart();
			int surfaceCount = hull.getSurfaceCount();
			
			//System.out.println(surfaceStart + ": " + (surfaceStart + surfaceCount));
			
			for (int j = surfaceStart; j < surfaceStart + surfaceCount; j++)
			{
				int surfaceWinding = this.hullSurfaceIndices[j];
				
				Surface surface = this.surfaces[surfaceWinding];
				
				//short planeIndex = surface.getPlaneIndex();
				//planeIndex &= ~0x8000;
				
				//PlaneF plane = this.planes[planeIndex];
				
				/*if (i == 272)
				{
					System.out.println("MinX: " + hull.getMinX() + ", MaxY: " + hull.getMaxY() + ", PX: " + plane.getX() + ", PY: " + plane.getY() + ", PD: " + plane.getD());
				}*/
				
				int windingStart = surface.getWindingStart();
				int windingCount = surface.getWindingCount();
				
				for (int k = windingStart; k < windingStart + windingCount; k++)
				{
					Point3F point = this.points[this.windings[k]].getPoint();
					
					//System.out.println("Point: " + point.toString());
					
					minHull.setMin(point);
					maxHull.setMax(point);
				}
				
				/*if (plane.getX() > 0)
				{
					maxHull.setMaxX(plane.getD());
				} else if (plane.getX() < 0)
				{
					minHull.setMinX(plane.getD());
				}
				
				if (plane.getY() > 0)
				{
					maxHull.setMaxY(plane.getD());
				} else if (plane.getY() < 0)
				{
					minHull.setMinY(plane.getD());
				}
				
				if (plane.getZ() > 0)
				{
					maxHull.setMaxZ(plane.getD());
				} else if (plane.getZ() < 0)
				{
					minHull.setMinZ(plane.getD());
				}*/
				
				//minHull = minHull.sub(4);
				//maxHull = maxHull.add(4);
				
				//minHull = new Point3F(-1000, -1000, -1000);
				//maxHull = new Point3F(1000, 1000, 1000);
				
				//minHull = new Point3F(0, 0, 0);
				//maxHull = new Point3F(0, 0, 0);
				
				//minHull = new Point3F(-10, -10, -10);
				//maxHull = new Point3F(10, 10, 10);
				
				hull.setMinX(minHull.getX());
				hull.setMinY(minHull.getY());
				hull.setMinZ(minHull.getZ());
				hull.setMaxX(maxHull.getX());
				hull.setMaxY(maxHull.getY());
				hull.setMaxZ(maxHull.getZ());
				
				/*if (plane.getX() > 0 && plane.getD() * -1 > hull.getMaxX())
				{
					hull.setMaxX(plane.getD() * -1);
				} else if (plane.getX() < 0 && plane.getD() < hull.getMinX())
				{
					hull.setMinX(plane.getD());
				}
				
				if (plane.getY() > 0 && plane.getY() * -1 > hull.getMaxY())
				{
					hull.setMaxY(plane.getD());
				} else if (plane.getY() < 0 && plane.getD() < hull.getMinY())
				{
					hull.setMinY(plane.getD());
				}
				
				if (plane.getZ() > 0 && plane.getD() * -1 > hull.getMaxZ())
				{
					hull.setMaxZ(plane.getD() * -1);
				} else if (plane.getZ() < 0 && plane.getD() < hull.getMinZ())
				{
					hull.setMinZ(plane.getD());
				}*/
			}
			
			/*if (hull.getMaxX() > this.boundingBox.getMax().getX())
			{
				this.boundingBox.getMax().setX(hull.getMaxX());
			} else if (hull.getMinX() < this.boundingBox.getMin().getX())
			{
				this.boundingBox.getMin().setX(hull.getMinX());
			}
			
			if (hull.getMaxY() > this.boundingBox.getMax().getY())
			{
				this.boundingBox.getMax().setY(hull.getMaxY());
			} else if (hull.getMinY() < this.boundingBox.getMin().getY())
			{
				this.boundingBox.getMin().setY(hull.getMinY());
			}
			
			if (hull.getMaxZ() > this.boundingBox.getMax().getZ())
			{
				this.boundingBox.getMax().setZ(hull.getMaxZ());
			} else if (hull.getMinZ() < this.boundingBox.getMin().getZ())
			{
				this.boundingBox.getMin().setZ(hull.getMinZ());
			}*/
			
			this.boundingBox.getMin().setMin(new Point3F(hull.getMinX(), hull.getMinY(), hull.getMinZ()));
			this.boundingBox.getMax().setMax(new Point3F(hull.getMaxX(), hull.getMaxY(), hull.getMaxZ()));
			
			if (this.boundingSphere == null)
				this.boundingSphere = new SphereF(new Point3F(), 0);
			this.boundingSphere.setCenter(this.boundingBox.getMax().abs().sub(this.boundingBox.getMin().abs()));
			
			float max1 = this.boundingBox.getMax().absMax();
			float min1 = this.boundingBox.getMin().absMax();
			
			float diamater = Math.max(max1, min1);
			
			this.boundingSphere.setRadius(diamater);
		}
		//this.boundingBox = new Box3F(new Point3F(-200.00002f, -152.00002f, -33.00009f), new Point3F(232.0f, 168.0f, 14.0f));
		//this.boundingSphere.setCenter(new Point3F(15.999992f, 7.9999924f, -9.500046f));
		//this.boundingSphere.setRadius(269.83005f);
	}
	
	public void readPlaneVector(ReverseDataInputStream dis) throws IOException
	{
		int numNormals = dis.readInt();
		this.planeNormals = new Point3F[numNormals];
		for (int i = 0; i < numNormals; i++)
		{
			this.planeNormals[i] = dis.readPoint3F();
		}
		
		int numPlanes = dis.readInt();
		this.planes = new PlaneF[numPlanes];
		for (int i = 0; i < numPlanes; i++)
		{
			short index = dis.readShort();
			float d = dis.readFloat();
			
			this.planes[i] = new PlaneF(this.planeNormals[index].getX(), this.planeNormals[index].getY(),
					this.planeNormals[index].getZ(), d);
			this.planes[i].setNormalIndex(index);
		}
		
		/*
		 * int numNormals = dis.readInt(); this.planeNormals = new Point3F[numNormals]; for (int i = 0; i < numNormals;
		 * i++) { this.planeNormals[i] = dis.readPoint3F(); }
		 * 
		 * int numPlanes = dis.readInt(); this.planeIndices = new PlaneIndex[numPlanes]; this.planes = new
		 * PlaneF[numPlanes]; for (int i = 0; i < numPlanes; i++) { short index = dis.readShort(); float d =
		 * dis.readFloat();
		 * 
		 * this.planeIndices[i] = new PlaneIndex(index, d); this.planes[i] = new PlaneF(this.planeNormals[index].getX(),
		 * this.planeNormals[index].getY(), this.planeNormals[index].getZ(), d); }
		 */
		
	}
	
	public boolean readLMapTexGen(ReverseDataInputStream dis, TexGenPlanes texGenPlanes) throws IOException
	{
		float[] genX = new float[4];
		float[] genY = new float[4];
		
		for (int i = 0; i < 4; i++)
		{
			genX[i] = 0.0f;
			genY[i] = 0.0f;
		}
		
		short finalWord = dis.readShort();
		genX[3] = dis.readFloat();
		genY[3] = dis.readFloat();
		
		// Unpack the final word
		int logScaleY = (finalWord >> 0) & ((1 << 6) - 1);
		int logScaleX = (finalWord >> 6) & ((1 << 6) - 1);
		short stEnc = (short) ((finalWord >> 13) & 7);
		
		int sc, tc;
		switch (stEnc)
		{
			case 0:
				sc = 0;
				tc = 1;
				break;
			case 1:
				sc = 0;
				tc = 2;
				break;
			case 2:
				sc = 1;
				tc = 0;
				break;
			case 3:
				sc = 1;
				tc = 2;
				break;
			case 4:
				sc = 2;
				tc = 0;
				break;
			case 5:
				sc = 2;
				tc = 1;
				break;
			default:
				sc = tc = -1;
				throw new IOException("Invalid st coord encoding in Interior::readLMapTG");
		}
		
		int invScaleX = 1 << logScaleX;
		int invScaleY = 1 << logScaleY;
		
		genX[sc] = (float) (1.0 / (double) invScaleX);
		genY[tc] = (float) (1.0 / (double) invScaleY);
		
		PlaneF planeX = new PlaneF(genX[0], genX[1], genX[2], genX[3]);
		PlaneF planeY = new PlaneF(genY[0], genY[1], genY[2], genY[3]);
		
		// new TexGenPlanes(planeX, planeY)
		
		texGenPlanes.set(planeX, planeY);
		
		return true;
	}
	
	public boolean writeLMapTexGen(ReverseDataOutputStream dos, PlaneF planeX, PlaneF planeY) throws IOException
	{
		float[] genX = new float[4];
		float[] genY = new float[4];
		
		genX[0] = planeX.getX();
		genX[1] = planeX.getY();
		genX[2] = planeX.getZ();
		genX[3] = planeX.getD();
		genY[0] = planeY.getX();
		genY[1] = planeY.getY();
		genY[2] = planeY.getZ();
		genY[3] = planeY.getD();
		
		int sc = -1;
		int tc = -1;
		if (genX[0] != 0.0f)
			sc = 0;
		else if (genX[1] != 0.0f)
			sc = 1;
		else if (genX[2] != 0.0f)
			sc = 2;
			
		if (genY[0] != 0.0f)
			tc = 0;
		else if (genY[1] != 0.0f)
			tc = 1;
		else if (genY[2] != 0.0f)
			tc = 2;
			
		boolean success = sc != -1 && tc != -1 && sc != tc;
		if (!success)
		{
			throw new IOException("Hm, something wrong here.");
		}
		
		int invScaleX = (int) ((1.0f / genX[sc]) + 0.5f);
		int invScaleY = (int) ((1.0f / genY[tc]) + 0.5f);
		
		// System.out.println(invScaleX + ", " + invScaleY);
		
		success = invScaleX != 0 && Util.isPow2(invScaleX) && invScaleY != 0 && Util.isPow2(invScaleY);
		if (!success)
		{
			throw new IOException("Not a power of 2?  Something wrong");
		}
		
		int logScaleX = Util.log(invScaleX, 2);// Util.getBinLog2(invScaleX);
		int logScaleY = Util.log(invScaleY, 2);// Util.getBinLog2(invScaleY);
		
		success = logScaleX < 63 && logScaleY < 63;
		if (!success)
		{
			throw new IOException("Error, you've set the lightmap scale WAAYYY to high!");
		}
		
		short stEnc = -1;
		if (sc == 0 && tc == 1)
			stEnc = 0;
		else if (sc == 0 && tc == 2)
			stEnc = 1;
		else if (sc == 1 && tc == 0)
			stEnc = 2;
		else if (sc == 1 && tc == 2)
			stEnc = 3;
		else if (sc == 2 && tc == 0)
			stEnc = 4;
		else if (sc == 2 && tc == 1)
			stEnc = 5;
			
		if (stEnc == -1)
		{
			throw new IOException("Hm.  This should never happen. (" + sc + ", " + tc + ")");
		}
		
		short finalWord = (short) (stEnc << 13);
		finalWord |= logScaleX << 6;
		finalWord |= logScaleY << 0;
		
		dos.writeShort(finalWord);
		dos.writeFloat(genX[3]);
		dos.writeFloat(genY[3]);
		
		return true;
	}
	
	public Surface readSurface(ReverseDataInputStream dis, TexGenPlanes texGenPlanes, boolean tgeInterior)
			throws IOException
	{
		int windingStart = dis.readInt();
		
		if (windingStart > this.windings.length)
			return null;
			
		int windingCount;
		
		if (fileVersion >= 13)
		{
			windingCount = dis.readInt();
		} else
		{
			windingCount = dis.readByte();
		}
		
		if (windingStart + windingCount > this.windings.length)
			return null;
			
		short planeIndex = dis.readShort();
		//boolean flipped = (planeIndex >> 15 != 0);
		//planeIndex &= ~0x8000;
		
		if ((int) (planeIndex & ~0x8000) >= this.planes.length) // this.planeIndices.length)
			return null;
			
		short textureIndex = dis.readShort();
		
		if (textureIndex >= this.materialList.getMaterials().size())
			return null;
			
		int texGenIndex = dis.readInt();
		
		if (texGenIndex >= this.texGenEQs.size())
			return null;
			
		byte surfaceFlags = dis.readByte();
		int fanMask = dis.readInt();
		
		// try {
		// this.lmTexGenEQs[i] =
		if (!this.readLMapTexGen(dis, texGenPlanes))
		{
			return null;
		}
		/*
		 * } catch (IOException e) { tgeInterior = true; break; }
		 */
		
		short lightCount = dis.readShort();
		int lightStateInfoStart = dis.readInt();
		int mapOffsetX;
		int mapOffsetY;
		int mapSizeX;
		int mapSizeY;
		
		if (fileVersion >= 13)
		{
			mapOffsetX = dis.readInt();
			mapOffsetY = dis.readInt();
			mapSizeX = dis.readInt();
			mapSizeY = dis.readInt();
		} else
		{
			mapOffsetX = dis.readByte();
			mapOffsetY = dis.readByte();
			mapSizeX = dis.readByte();
			mapSizeY = dis.readByte();
		}
		
		boolean unused = false;
		
		if (!tgeInterior && ((fileVersion >= 0 && fileVersion <= 5) || fileVersion >= 12))
		{
			unused = dis.readBoolean();
		}
		
		int brushId = 0;
		
		// Looking at MBU in IDA reveals that these don't exist in version 0 OR 1.
		if (fileVersion > 1 && fileVersion <= 5)
		{
			brushId = dis.readInt();
		}
		
		return new Surface(windingStart, planeIndex, textureIndex, texGenIndex, lightCount, surfaceFlags, windingCount,
				fanMask, lightStateInfoStart, mapOffsetX, mapOffsetY, mapSizeX, mapSizeY, unused, brushId);//, flipped);
	}
	
	public boolean readVehicleCollision(ReverseDataInputStream dis) throws IOException
	{
		// Version this stream. We only load stream of the current version
		int fileVersion = dis.readInt();
		// System.out.println("v: " + fileVersion);
		if (fileVersion > 14)
		{
			System.err.println("incompatible vehicle collision version found.");
			return false;
		}
		
		// Vehicle Convex Hulls
		int numVehicleConvexHulls = dis.readInt();
		// System.out.println("numVehicleConvexHulls: " + numVehicleConvexHulls);
		this.vehicleConvexHulls = new ConvexHull[numVehicleConvexHulls];
		for (int i = 0; i < numVehicleConvexHulls; i++)
		{
			int hullStart = dis.readInt();
			short hullCount = dis.readShort();
			float minX = dis.readFloat();
			float maxX = dis.readFloat();
			float minY = dis.readFloat();
			float maxY = dis.readFloat();
			float minZ = dis.readFloat();
			float maxZ = dis.readFloat();
			int surfaceStart = dis.readInt();
			short surfaceCount = dis.readShort();
			int planeStart = dis.readInt();
			int polyListPlaneStart = dis.readInt();
			int polyListPointStart = dis.readInt();
			int polyListStringStart = dis.readInt();
			
			this.vehicleConvexHulls[i] = new ConvexHull(minX, maxX, minY, maxY, minZ, maxZ, hullStart, surfaceStart,
					planeStart, hullCount, surfaceCount, polyListPlaneStart, polyListPointStart, polyListStringStart,
					(short) 0, false);
		}
		
		int numVehicleHullEmitStrings = dis.readInt();
		// System.out.println("numVehicleHullEmitStrings: " + numVehicleHullEmitStrings);
		this.vehicleConvexHullEmitStrings = new byte[numVehicleHullEmitStrings];
		for (int i = 0; i < numVehicleHullEmitStrings; i++)
		{
			this.vehicleConvexHullEmitStrings[i] = dis.readByte();
		}
		
		int numVehicleHullIndices = dis.readInt();
		// System.out.println("numVehicleHullIndices: " + numVehicleHullIndices);
		this.vehicleHullIndices = new int[numVehicleHullIndices];
		for (int i = 0; i < numVehicleHullIndices; i++)
		{
			this.vehicleHullIndices[i] = dis.readInt();
		}
		
		int numVehicleHullPlaneIndices = dis.readInt();
		// System.out.println("numVehicleHullPlaneIndices: " + numVehicleHullPlaneIndices);
		this.vehicleHullPlaneIndices = new short[numVehicleHullPlaneIndices];
		for (int i = 0; i < numVehicleHullPlaneIndices; i++)
		{
			this.vehicleHullPlaneIndices[i] = dis.readShort();
		}
		
		int numVehicleHullEmitStringIndices = dis.readInt();
		// System.out.println("numVehicleHullEmitStringIndices: " + numVehicleHullEmitStringIndices);
		this.vehicleHullEmitStringIndices = new int[numVehicleHullEmitStringIndices];
		for (int i = 0; i < numVehicleHullEmitStringIndices; i++)
		{
			this.vehicleHullEmitStringIndices[i] = dis.readInt();
		}
		
		int numVehicleHullSurfaceIndices = dis.readInt();
		// System.out.println("numVehicleHullSurfaceIndices: " + numVehicleHullSurfaceIndices);
		this.vehicleHullSurfaceIndices = new int[numVehicleHullSurfaceIndices];
		for (int i = 0; i < numVehicleHullSurfaceIndices; i++)
		{
			this.vehicleHullSurfaceIndices[i] = dis.readInt();
		}
		
		int numVehiclePolyListPlanes = dis.readInt();
		// System.out.println("numVehiclePolyListPlanes: " + numVehiclePolyListPlanes);
		this.vehiclePolyListPlanes = new short[numVehiclePolyListPlanes];
		for (int i = 0; i < numVehiclePolyListPlanes; i++)
		{
			this.vehiclePolyListPlanes[i] = dis.readShort();
		}
		
		int numVehiclePolyListPoints = dis.readInt();
		// System.out.println("numVehiclePolyListPoints: " + numVehiclePolyListPoints);
		this.vehiclePolyListPoints = new int[numVehiclePolyListPoints];
		for (int i = 0; i < numVehiclePolyListPoints; i++)
		{
			this.vehiclePolyListPoints[i] = dis.readInt();
		}
		
		int numVehiclePolyListStrings = dis.readInt();
		// System.out.println("numVehiclePolyListStrings: " + numVehiclePolyListStrings);
		this.vehiclePolyListStrings = new byte[numVehiclePolyListStrings];
		for (int i = 0; i < numVehiclePolyListStrings; i++)
		{
			this.vehiclePolyListStrings[i] = dis.readByte();
		}
		
		int numVehicleNullSurfaces = dis.readInt();
		// System.out.println("numVehicleNullSurfaces: " + numVehicleNullSurfaces);
		this.vehicleNullSurfaces = new NullSurface[numVehicleNullSurfaces];
		for (int i = 0; i < numVehicleNullSurfaces; i++)
		{
			int windingStart = dis.readInt();
			short planeIndex = dis.readShort();
			byte surfaceFlags = dis.readByte();
			int windingCount = dis.readInt();
			
			this.vehicleNullSurfaces[i] = new NullSurface(windingStart, planeIndex, surfaceFlags, windingCount);
		}
		
		int numVehiclePoints = dis.readInt();
		// System.out.println("numVehiclePoints: " + numVehiclePoints);
		this.vehiclePoints = new ItrPaddedPoint[numVehiclePoints];
		for (int i = 0; i < numVehiclePoints; i++)
		{
			Point3F point = dis.readPoint3F();
			this.vehiclePoints[i] = new ItrPaddedPoint(point);
		}
		
		int numVehiclePlanes = dis.readInt();
		// System.out.println("numVehiclePlanes: " + numVehiclePlanes);
		this.vehiclePlanes = new PlaneF[numVehiclePlanes];
		for (int i = 0; i < numVehiclePlanes; i++)
		{
			this.vehiclePlanes[i] = dis.readPlaneF();
		}
		
		int numVehicleWindings = dis.readInt();
		// System.out.println("numVehicleWindings: " + numVehicleWindings);
		this.vehicleWindings = new int[numVehicleWindings];
		for (int i = 0; i < numVehicleWindings; i++)
		{
			this.vehicleWindings[i] = dis.readInt();
		}
		
		int numVehicleWindingIndices = dis.readInt();
		// System.out.println("numVehicleWindingIndices: " + numVehicleWindingIndices);
		this.vehicleWindingIndices = new TriFan[numVehicleWindingIndices];
		for (int i = 0; i < numVehicleWindingIndices; i++)
		{
			int windingStart = dis.readInt();
			int windingCount = dis.readInt();
			
			this.vehicleWindingIndices[i] = new TriFan(windingStart, windingCount);
		}
		
		return true;
	}
	
	public boolean write(ReverseDataOutputStream dos, int targetVersion) throws IOException
	{
		prepareData();
		
		boolean tgeInterior = false;
		if (targetVersion == -1)
		{
			targetVersion = 0;
			tgeInterior = true;
		}

		if (!this.verify(targetVersion))
		{
			return false;
		}
		
		dos.writeInt(targetVersion);
		
		dos.writeInt(this.detailLevel);
		dos.writeInt(this.minPixels);
		dos.writeBox3F(this.boundingBox);
		dos.writeSphereF(this.boundingSphere);
		dos.writeBoolean(hasAlarmState);
		dos.writeInt(this.numLightStateEntries);
		
		// Planes
		this.writePlaneVector(dos, targetVersion);
		
		// Points
		dos.writeInt(this.points.length);
		for (int i = 0; i < this.points.length; i++)
		{
			dos.writePoint3F(this.points[i].getPoint());
		}
		
		// Point Visibility
		if (targetVersion != 4)
		{
			if (this.pointVisibility == null)
				this.pointVisibility = new byte[0];
			dos.writeInt(this.pointVisibility.length);
			for (int i = 0; i < this.pointVisibility.length; i++)
			{
				dos.writeByte(this.pointVisibility[i]);
			}
		}
		
		// TexGenEQs
		dos.writeInt(this.texGenEQs.size());
		for (int i = 0; i < this.texGenEQs.size(); i++)
		{
			dos.writePlaneF(this.texGenEQs.get(i).getPlaneX());
			dos.writePlaneF(this.texGenEQs.get(i).getPlaneY());
		}
		
		// BSP Nodes
		if (this.bspNodes == null)
			this.bspNodes = new BSPNode[0];
		dos.writeInt(this.bspNodes.length);
		for (int i = 0; i < this.bspNodes.length; i++)
		{
			dos.writeShort(this.bspNodes[i].getPlaneIndex());
			
			if (targetVersion < 14)
			{
				short frontIndex;
				short backIndex;
				if (this.fileVersion >= 14)
				{
					frontIndex = Util.convertInt(this.bspNodes[i].getFrontIndex());
					backIndex = Util.convertInt(this.bspNodes[i].getBackIndex());
				} else
				{
					frontIndex = (short) this.bspNodes[i].getFrontIndex();
					backIndex = (short) this.bspNodes[i].getBackIndex();
				}
				
				dos.writeShort(frontIndex);
				dos.writeShort(backIndex);
			} else
			{
				dos.writeInt(this.bspNodes[i].getFrontIndex());
				dos.writeInt(this.bspNodes[i].getBackIndex());
			}
		}
		
		// BSP Solid Leaves
		if (this.bspSolidLeaves == null)
			this.bspSolidLeaves = new BSPSolidLeaf[0];
		dos.writeInt(this.bspSolidLeaves.length);
		for (int i = 0; i < this.bspSolidLeaves.length; i++)
		{
			dos.writeInt(this.bspSolidLeaves[i].getSurfaceIndex());
			dos.writeShort(this.bspSolidLeaves[i].getSurfaceCount());
		}
		
		// Material List
		this.materialList.write(dos);
		
		// Windings
		dos.writeInt(this.windings.length);
		for (int i = 0; i < this.windings.length; i++)
		{
			dos.writeInt(this.windings[i]);
		}
		
		// Winding Indices
		if (this.windingIndices == null)
			this.windingIndices = new TriFan[0];
		dos.writeInt(this.windingIndices.length);
		for (int i = 0; i < this.windingIndices.length; i++)
		{
			dos.writeInt(this.windingIndices[i].getWindingStart());
			dos.writeInt(this.windingIndices[i].getWindingCount());
		}
		
		// Edges
		if (targetVersion >= 12)
		{
			dos.writeInt(this.edges.length);
			for (int i = 0; i < this.edges.length; i++)
			{
				dos.writeInt(this.edges[i].getVertex1());
				dos.writeInt(this.edges[i].getVertex2());
				dos.writeInt(this.edges[i].getFace1());
				dos.writeInt(this.edges[i].getFace2());
			}
		}
		
		// Zones
		if (this.zones == null)
			this.zones = new Zone[0];
		dos.writeInt(this.zones.length);
		for (int i = 0; i < this.zones.length; i++)
		{
			dos.writeShort(this.zones[i].getPortalStart());
			dos.writeShort(this.zones[i].getPortalCount());
			dos.writeInt(this.zones[i].getSurfaceStart());
			
			if (tgeInterior)
				dos.writeInt(this.zones[i].getSurfaceCount());
			else
				dos.writeShort(this.zones[i].getSurfaceCount());
				
			if (targetVersion >= 12)
			{
				dos.writeInt(this.zones[i].getStaticMeshStart());
				dos.writeInt(this.zones[i].getStaticMeshCount());
			}
			
			if (!tgeInterior)
				dos.writeShort(this.zones[i].getFlags());
		}
		
		// Zone Surfaces
		if (this.zoneSurfaces == null)
			this.zoneSurfaces = new short[0];
		dos.writeInt(this.zoneSurfaces.length);
		for (int i = 0; i < this.zoneSurfaces.length; i++)
		{
			dos.writeShort(this.zoneSurfaces[i]);
		}
		
		// Zone Static Meshes
		if (targetVersion >= 12)
		{
			dos.writeInt(this.zoneStaticMeshes.length);
			for (int i = 0; i < this.zoneStaticMeshes.length; i++)
			{
				dos.writeInt(this.zoneStaticMeshes[i]);
			}
		}
		
		// Zone Portal List
		if (this.zonePortalList == null)
			this.zonePortalList = new short[0];
		dos.writeInt(this.zonePortalList.length);
		for (int i = 0; i < this.zonePortalList.length; i++)
		{
			dos.writeShort(this.zonePortalList[i]);
		}
		
		// Portals
		if (this.portals == null)
			this.portals = new Portal[0];
		dos.writeInt(this.portals.length);
		for (int i = 0; i < this.portals.length; i++)
		{
			dos.writeShort(this.portals[i].getPlaneIndex());
			dos.writeShort(this.portals[i].getTriFanCount());
			dos.writeInt(this.portals[i].getTriFanStart());
			dos.writeShort(this.portals[i].getZoneFront());
			dos.writeShort(this.portals[i].getZoneBack());
		}
		
		// Surfaces
		dos.writeInt(this.surfaces.length);
		if (this.lmTexGenEQs == null)
			this.lmTexGenEQs = new TexGenPlanes[this.surfaces.length];
		for (int i = 0; i < this.surfaces.length; i++)
		{
			dos.writeInt(this.surfaces[i].getWindingStart());
			
			if (targetVersion >= 13)
			{
				dos.writeInt(this.surfaces[i].getWindingCount());
			} else
			{
				dos.writeByte((byte) this.surfaces[i].getWindingCount());
			}
			
			dos.writeShort(this.surfaces[i].getPlaneIndex());
			dos.writeShort(this.surfaces[i].getTextureIndex());
			// TODO: FIX
			dos.writeInt(this.surfaces[i].getTexGenIndex());
			dos.writeByte(this.surfaces[i].getSurfaceFlags());
			dos.writeInt(this.surfaces[i].getFanMask());
			if (this.lmTexGenEQs[i] == null)
				this.lmTexGenEQs[i] = new TexGenPlanes();
			this.writeLMapTexGen(dos, this.lmTexGenEQs[i].getPlaneX(), this.lmTexGenEQs[i].getPlaneY());
			
			dos.writeShort(this.surfaces[i].getLightCount());
			dos.writeInt(this.surfaces[i].getLightStateInfoStart());
			
			if (targetVersion >= 13)
			{
				dos.writeInt(this.surfaces[i].getMapOffsetX());
				dos.writeInt(this.surfaces[i].getMapOffsetY());
				dos.writeInt(this.surfaces[i].getMapSizeX());
				dos.writeInt(this.surfaces[i].getMapSizeY());
			} else
			{
				dos.writeByte((byte) this.surfaces[i].getMapOffsetX());
				dos.writeByte((byte) this.surfaces[i].getMapOffsetY());
				dos.writeByte((byte) this.surfaces[i].getMapSizeX());
				dos.writeByte((byte) this.surfaces[i].getMapSizeY());
			}
			
			if (!tgeInterior)
			{
				if ((targetVersion >= 0 && targetVersion <= 5) || targetVersion >= 12)
					dos.writeBoolean(this.surfaces[i].getUnused());
					
				// Looking at MBU in IDA reveals that these don't exist in version 0 OR 1.
				if (targetVersion > 1 && targetVersion <= 5)
				{
					dos.writeInt(this.surfaces[i].getBrushId());
				}
			}
		}
		
		// Edges
		// Looking at MBU in IDA reveals that these don't exist in version 0 OR 1.
		if (targetVersion > 1 && targetVersion <= 5)
		{
			if (this.edges == null)
				this.edges = new Edge[0];
			dos.writeInt(this.edges.length);
			for (int i = 0; i < this.edges.length; i++)
			{
				dos.writeInt(this.edges[i].getVertex1());
				dos.writeInt(this.edges[i].getVertex2());
				dos.writeInt(this.edges[i].getNormal1());
				dos.writeInt(this.edges[i].getNormal2());
				
				if (targetVersion > 2)
				{
					dos.writeInt(this.edges[i].getFace1());
					dos.writeInt(this.edges[i].getFace2());
				}
			}
		}
		
		// Normals
		if (targetVersion == 4 || targetVersion == 5)
		{
			if (this.normalsLegacy == null)
				this.normalsLegacy = new Point3F[0];
			dos.writeInt(this.normalsLegacy.length);
			for (int i = 0; i < this.normalsLegacy.length; i++)
			{
				dos.writePoint3F(this.normalsLegacy[i]);
			}
			
			// Quick Fix
			if (this.normalIndices == null)
				this.normalIndices = new ArrayList<Short>();
				
			dos.writeInt(this.normalIndices.size());
			for (int i = 0; i < this.normalIndices.size(); i++)
			{
				dos.writeShort(this.normalIndices.get(i));
			}
		}
		
		// NormalLMapIndices
		if (this.normalLMapIndices == null)
			this.normalLMapIndices = new int[0];
		dos.writeInt(this.normalLMapIndices.length);
		for (int i = 0; i < this.normalLMapIndices.length; i++)
		{
			if (targetVersion >= 13)
			{
				dos.writeInt(this.normalLMapIndices[i]);
			} else
			{
				dos.writeByte((byte) this.normalLMapIndices[i]);
			}
		}
		
		// AlarmLMapIndices
		if (targetVersion != 4)
		{
			if (this.alarmLMapIndices == null)
				this.alarmLMapIndices = new int[0];
			dos.writeInt(this.alarmLMapIndices.length);
			for (int i = 0; i < this.alarmLMapIndices.length; i++)
			{
				if (targetVersion >= 13)
				{
					dos.writeInt(this.alarmLMapIndices[i]);
				} else
				{
					dos.writeByte((byte) this.alarmLMapIndices[i]);
				}
			}
		}
		
		// Null Surfaces
		if (this.nullSurfaces == null)
			this.nullSurfaces = new NullSurface[0];
		dos.writeInt(this.nullSurfaces.length);
		for (int i = 0; i < this.nullSurfaces.length; i++)
		{
			dos.writeInt(this.nullSurfaces[i].getWindingStart());
			dos.writeShort(this.nullSurfaces[i].getPlaneIndex());
			dos.writeByte(this.nullSurfaces[i].getSurfaceFlags());
			
			if (targetVersion >= 13)
			{
				dos.writeInt(this.nullSurfaces[i].getWindingCount());
			} else
			{
				dos.writeByte((byte) this.nullSurfaces[i].getWindingCount());
			}
		}
		
		// Lightmaps
		if (targetVersion != 4)
		{
			// test
			/*
			 * if (targetVersion == 13 || targetVersion == 0 || targetVersion == 3) { this.lightMaps = new
			 * BufferedImage[0]; this.lightDirMaps = new BufferedImage[0]; this.lightMapKeep = new boolean[0]; }
			 */
			if (this.lightMaps == null)
				this.lightMaps = new BufferedImage[0];
			dos.writeInt(this.lightMaps.length);
			for (int i = 0; i < this.lightMaps.length; i++)
			{
				dos.writeImage(this.lightMaps[i]);
				
				if (!tgeInterior)
				{
					if (targetVersion >= 0 || targetVersion <= 5 || targetVersion >= 12)
					{
						dos.writeImage(this.lightDirMaps[i]);
					}
				}
				
				dos.writeBoolean(this.lightMapKeep[i]);
			}
		}
		
		// Solid Leaf Surfaces
		if (this.solidLeafSurfaces == null)
			this.solidLeafSurfaces = new int[0];
		dos.writeInt(this.solidLeafSurfaces.length);
		for (int i = 0; i < this.solidLeafSurfaces.length; i++)
		{
			dos.writeInt(this.solidLeafSurfaces[i]);
		}
		
		// Animated Lights
		if (this.animatedLights == null)
			this.animatedLights = new AnimatedLight[0];
		dos.writeInt(this.animatedLights.length);
		for (int i = 0; i < this.animatedLights.length; i++)
		{
			dos.writeInt(this.animatedLights[i].getNameIndex());
			dos.writeInt(this.animatedLights[i].getStateIndex());
			dos.writeShort(this.animatedLights[i].getStateCount());
			dos.writeShort(this.animatedLights[i].getFlags());
			dos.writeInt(this.animatedLights[i].getDuration());
		}
		
		// Light States
		if (this.lightStates == null)
			this.lightStates = new LightState[0];
		dos.writeInt(this.lightStates.length);
		for (int i = 0; i < this.lightStates.length; i++)
		{
			dos.writeByte(this.lightStates[i].getRed());
			dos.writeByte(this.lightStates[i].getGreen());
			dos.writeByte(this.lightStates[i].getBlue());
			dos.writeInt(this.lightStates[i].getActiveTime());
			dos.writeInt(this.lightStates[i].getDataIndex());
			dos.writeShort(this.lightStates[i].getDataCount());
		}
		
		if (targetVersion != 4)
		{
			// Light State Data
			if (this.lightStateData == null)
				this.lightStateData = new LightStateData[0];
			dos.writeInt(this.lightStateData.length);
			for (int i = 0; i < this.lightStateData.length; i++)
			{
				dos.writeInt(this.lightStateData[i].getSurfaceIndex());
				dos.writeInt(this.lightStateData[i].getMapIndex());
				dos.writeShort(this.lightStateData[i].getLightStateIndex());
			}
			
			// State Data Buffer
			if (this.stateDataBuffer == null)
				this.stateDataBuffer = new byte[0];
			dos.writeInt(this.stateDataBuffer.length);
			if (!tgeInterior)
				dos.writeInt(0);
			for (int i = 0; i < this.stateDataBuffer.length; i++)
			{
				dos.writeInt(this.stateDataBuffer[i]);
			}
			if (tgeInterior)
				dos.writeInt(0);
				
			// Name Buffer
			if (this.nameBuffer == null)
				this.nameBuffer = new char[0];
			dos.writeInt(this.nameBuffer.length);
			for (int i = 0; i < this.nameBuffer.length; i++)
			{
				dos.writeByte(this.nameBuffer[i]);
			}
			
			// Sub Objects
			if (this.subObjects == null)
				this.subObjects = new InteriorSubObject[0];
			dos.writeInt(this.subObjects.length);
			for (int i = 0; i < this.subObjects.length; i++)
			{
				if (!this.subObjects[i].write(dos))
				{
					throw new IOException("Error writing sub object to stream!");
				}
			}
		}
		
		// Convex Hulls
		if (this.convexHulls == null)
			this.convexHulls = new ConvexHull[0];
		dos.writeInt(this.convexHulls.length);
		for (int i = 0; i < this.convexHulls.length; i++)
		{
			dos.writeInt(this.convexHulls[i].getHullStart());
			dos.writeShort(this.convexHulls[i].getHullCount());
			dos.writeFloat(this.convexHulls[i].getMinX());
			dos.writeFloat(this.convexHulls[i].getMaxX());
			dos.writeFloat(this.convexHulls[i].getMinY());
			dos.writeFloat(this.convexHulls[i].getMaxY());
			dos.writeFloat(this.convexHulls[i].getMinZ());
			dos.writeFloat(this.convexHulls[i].getMaxZ());
			dos.writeInt(this.convexHulls[i].getSurfaceStart());
			dos.writeShort(this.convexHulls[i].getSurfaceCount());
			dos.writeInt(this.convexHulls[i].getPlaneStart());
			dos.writeInt(this.convexHulls[i].getPolyListPlaneStart());
			dos.writeInt(this.convexHulls[i].getPolyListPointStart());
			dos.writeInt(this.convexHulls[i].getPolyListStringStart());
			
			if (targetVersion >= 12)
			{
				dos.writeBoolean(this.convexHulls[i].getStaticMesh());
			}
		}
		
		if (this.convexHullEmitStrings == null)
			this.convexHullEmitStrings = new byte[0];
		dos.writeInt(this.convexHullEmitStrings.length);
		for (int i = 0; i < this.convexHullEmitStrings.length; i++)
		{
			dos.writeByte(this.convexHullEmitStrings[i]);
		}
		
		// Hull Indices
		if (this.hullIndices == null)
			this.hullIndices = new int[0];
		dos.writeInt(this.hullIndices.length);
		for (int i = 0; i < this.hullIndices.length; i++)
		{
			dos.writeInt(this.hullIndices[i]);
		}
		
		// Hull Plane Indices
		if (this.hullPlaneIndices == null)
			this.hullPlaneIndices = new short[0];
		dos.writeInt(this.hullPlaneIndices.length);
		for (int i = 0; i < this.hullPlaneIndices.length; i++)
		{
			dos.writeShort(this.hullPlaneIndices[i]);
		}
		
		// Hull Emit String Indices
		if (this.hullEmitStringIndices == null)
			this.hullEmitStringIndices = new int[0];
		dos.writeInt(this.hullEmitStringIndices.length);
		for (int i = 0; i < this.hullEmitStringIndices.length; i++)
		{
			dos.writeInt(this.hullEmitStringIndices[i]);
		}
		
		// Hull Surface Indices
		if (this.hullSurfaceIndices == null)
			this.hullSurfaceIndices = new int[0];
		dos.writeInt(this.hullSurfaceIndices.length);
		for (int i = 0; i < this.hullSurfaceIndices.length; i++)
		{
			dos.writeInt(this.hullSurfaceIndices[i]);
		}
		
		// Poly List Planes
		if (this.polyListPlanes == null)
			this.polyListPlanes = new short[0];
		dos.writeInt(this.polyListPlanes.length);
		for (int i = 0; i < this.polyListPlanes.length; i++)
		{
			dos.writeShort(this.polyListPlanes[i]);
		}
		
		// Poly List Points
		if (this.polyListPoints == null)
			this.polyListPoints = new int[0];
		dos.writeInt(this.polyListPoints.length);
		for (int i = 0; i < this.polyListPoints.length; i++)
		{
			dos.writeInt(this.polyListPoints[i]);
		}
		
		// Poly List Strings
		if (this.polyListStrings == null)
			this.polyListStrings = new byte[0];
		dos.writeInt(this.polyListStrings.length);
		for (int i = 0; i < this.polyListStrings.length; i++)
		{
			dos.writeByte(this.polyListStrings[i]);
		}
		
		final int numCoordBins = 16;
		
		// Coord Bins
		if (this.coordBins == null)
		{
			this.coordBins = new CoordBin[numCoordBins * numCoordBins];
			
			for (int i = 0; i < this.coordBins.length; i++)
			{
				this.coordBins[i] = new CoordBin(i, 1);
			}
		}
		for (int i = 0; i < numCoordBins * numCoordBins; i++)
		{
			dos.writeInt(this.coordBins[i].getBinStart());
			dos.writeInt(this.coordBins[i].getBinCount());
		}
		
		if (this.coordBinIndices == null)
		{
			this.coordBinIndices = new short[this.coordBins.length];
			
			for (int i = 0; i < this.coordBinIndices.length; i++)
			{
				this.coordBinIndices[i] = (short)i;
			}
		}
		dos.writeInt(this.coordBinIndices.length);
		for (int i = 0; i < this.coordBinIndices.length; i++)
		{
			dos.writeShort(this.coordBinIndices[i]);
		}
		
		dos.writeInt(this.coordBinMode);
		
		if (targetVersion != 4)
		{
			if (this.baseAmbient == null)
				this.baseAmbient = new ColorI((byte)0, (byte)0, (byte)0, (byte)1);
			dos.writeColorI(this.baseAmbient);
			if (this.alarmAmbient == null)
				this.alarmAmbient = new ColorI((byte)0, (byte)0, (byte)0, (byte)1);
			dos.writeColorI(this.alarmAmbient);
			
			if (targetVersion >= 10)
			{
				if (this.staticMeshes == null)
					this.staticMeshes = new InteriorSimpleMesh[0];
				dos.writeInt(this.staticMeshes.length);
				for (int i = 0; i < this.staticMeshes.length; i++)
				{
					this.staticMeshes[i].write(dos);
				}
			} else
			/* if (!tgeInterior) */ {
				dos.writeInt(0);
			}
			
			if (targetVersion >= 11)// || tgeInterior)
			{
				// test
				/*
				 * if (targetVersion == 13 || targetVersion == 0 || targetVersion == 3) { this.normals = new Point3F[0];
				 * this.texMatrices = new TexMatrix[0]; this.texMatIndices = new int[0]; }
				 */
				// Normals
				if (this.normals == null)
					this.normals = new Point3F[0];
				dos.writeInt(this.normals.length);
				for (int i = 0; i < this.normals.length; i++)
				{
					dos.writePoint3F(this.normals[i]);
				}
				
				// TexMatrices
				if (this.texMatrices == null)
					this.texMatrices = new TexMatrix[0];
				dos.writeInt(this.texMatrices.length);
				for (int i = 0; i < this.texMatrices.length; i++)
				{
					dos.writeInt(this.texMatrices[i].getT());
					dos.writeInt(this.texMatrices[i].getN());
					dos.writeInt(this.texMatrices[i].getB());
				}
				
				// TexMatIndices
				if (this.texMatIndices == null)
					this.texMatIndices = new int[0];
				dos.writeInt(this.texMatIndices.length);
				for (int i = 0; i < this.texMatIndices.length; i++)
				{
					dos.writeInt(this.texMatIndices[i]);
				}
			} else
			{
				dos.writeInt(0);
				dos.writeInt(0);
			}
			
			// test
			/*
			 * if (targetVersion == 13 || targetVersion == 0 || targetVersion == 3) { dos.writeInt(0); return true; }
			 */
			
			if (lightMapBorderSize > 0 && !tgeInterior)
			{
				dos.writeInt(1);
				dos.writeInt(this.lightMapBorderSize);
				
				dos.writeInt(0);
			} else
			{
				dos.writeInt(0);
			}
		}
		
		return true;
	}
	
	public void writePlaneVector(ReverseDataOutputStream dos, int targetVersion) throws IOException
	{
		List<Point3F> uniqueNormals = new ArrayList<Point3F>();
		List<Short> uniqueIndices = new ArrayList<Short>();
		
		for (int i = 0; i < this.planes.length; i++)
		{
			boolean inserted = false;
			for (int j = 0; j < uniqueNormals.size(); j++)
			{
				if (this.planes[i] == uniqueNormals.get(j))
				{
					uniqueIndices.add((short) j);
					inserted = true;
					break;
				}
			}
			
			if (inserted == false)
			{
				uniqueIndices.add((short) uniqueNormals.size());
				uniqueNormals.add(new Point3F(this.planes[i].getX(), this.planes[i].getY(), this.planes[i].getZ()));
			}
		}
		
		dos.writeInt(uniqueNormals.size());
		for (int i = 0; i < uniqueNormals.size(); i++)
		{
			dos.writePoint3F(uniqueNormals.get(i));
		}
		
		dos.writeInt(this.planes.length);
		for (int i = 0; i < this.planes.length; i++)
		{
			dos.writeShort(uniqueIndices.get(i));
			dos.writeFloat(this.planes[i].getD());
		}
		
		/*
		 * dos.writeInt(this.planeNormals.length); for (int i = 0; i < this.planeNormals.length; i++) {
		 * dos.writePoint3F(this.planeNormals[i]); }
		 * 
		 * dos.writeInt(this.planeIndices.length); for (int i = 0; i < this.planeIndices.length; i++) {
		 * dos.writeShort(this.planeIndices[i].getIndex()); dos.writeFloat(this.planeIndices[i].getD()); }
		 */
		
	}
	
	public boolean writeVehicleCollision(ReverseDataOutputStream dos, int targetVersion) throws IOException
	{
		boolean tgeInterior = false;
		if (targetVersion == -1)
		{
			targetVersion = 0;
			tgeInterior = true;
		}
		dos.writeInt(targetVersion);
		
		// Convex Hulls
		if (this.vehicleConvexHulls == null)
			this.vehicleConvexHulls = new ConvexHull[0];
		dos.writeInt(this.vehicleConvexHulls.length);
		for (int i = 0; i < this.vehicleConvexHulls.length; i++)
		{
			dos.writeInt(this.vehicleConvexHulls[i].getHullStart());
			dos.writeShort(this.vehicleConvexHulls[i].getHullCount());
			dos.writeFloat(this.vehicleConvexHulls[i].getMinX());
			dos.writeFloat(this.vehicleConvexHulls[i].getMaxX());
			dos.writeFloat(this.vehicleConvexHulls[i].getMinY());
			dos.writeFloat(this.vehicleConvexHulls[i].getMaxY());
			dos.writeFloat(this.vehicleConvexHulls[i].getMinZ());
			dos.writeFloat(this.vehicleConvexHulls[i].getMaxZ());
			dos.writeInt(this.vehicleConvexHulls[i].getSurfaceStart());
			dos.writeShort(this.vehicleConvexHulls[i].getSurfaceCount());
			dos.writeInt(this.vehicleConvexHulls[i].getPlaneStart());
			dos.writeInt(this.vehicleConvexHulls[i].getPolyListPlaneStart());
			dos.writeInt(this.vehicleConvexHulls[i].getPolyListPointStart());
			dos.writeInt(this.vehicleConvexHulls[i].getPolyListStringStart());
		}
		
		if (this.vehicleConvexHullEmitStrings == null)
			this.vehicleConvexHullEmitStrings = new byte[0];
		dos.writeInt(this.vehicleConvexHullEmitStrings.length);
		for (int i = 0; i < this.vehicleConvexHullEmitStrings.length; i++)
		{
			dos.writeByte(this.vehicleConvexHullEmitStrings[i]);
		}
		
		if (this.vehicleHullIndices == null)
			this.vehicleHullIndices = new int[0];
		dos.writeInt(this.vehicleHullIndices.length);
		for (int i = 0; i < this.vehicleHullIndices.length; i++)
		{
			dos.writeInt(this.vehicleHullIndices[i]);
		}
		
		if (this.vehicleHullPlaneIndices == null)
			this.vehicleHullPlaneIndices = new short[0];
		dos.writeInt(this.vehicleHullPlaneIndices.length);
		for (int i = 0; i < this.vehicleHullPlaneIndices.length; i++)
		{
			dos.writeShort(this.vehicleHullPlaneIndices[i]);
		}
		
		if (this.vehicleHullEmitStringIndices == null)
			this.vehicleHullEmitStringIndices = new int[0];
		dos.writeInt(this.vehicleHullEmitStringIndices.length);
		for (int i = 0; i < this.vehicleHullEmitStringIndices.length; i++)
		{
			dos.writeInt(this.vehicleHullEmitStringIndices[i]);
		}
		
		if (this.vehicleHullSurfaceIndices == null)
			this.vehicleHullSurfaceIndices = new int[0];
		dos.writeInt(this.vehicleHullSurfaceIndices.length);
		for (int i = 0; i < this.vehicleHullSurfaceIndices.length; i++)
		{
			dos.writeInt(this.vehicleHullSurfaceIndices[i]);
		}
		
		if (this.vehiclePolyListPlanes == null)
			this.vehiclePolyListPlanes = new short[0];
		dos.writeInt(this.vehiclePolyListPlanes.length);
		for (int i = 0; i < this.vehiclePolyListPlanes.length; i++)
		{
			dos.writeShort(this.vehiclePolyListPlanes[i]);
		}
		
		if (this.vehiclePolyListPoints == null)
			this.vehiclePolyListPoints = new int[0];
		dos.writeInt(this.vehiclePolyListPoints.length);
		for (int i = 0; i < this.vehiclePolyListPoints.length; i++)
		{
			dos.writeInt(this.vehiclePolyListPoints[i]);
		}
		
		if (this.vehiclePolyListStrings == null)
			this.vehiclePolyListStrings = new byte[0];
		dos.writeInt(this.vehiclePolyListStrings.length);
		for (int i = 0; i < this.vehiclePolyListStrings.length; i++)
		{
			dos.writeByte(this.vehiclePolyListStrings[i]);
		}
		
		if (this.vehicleNullSurfaces == null)
			this.vehicleNullSurfaces = new NullSurface[0];
		dos.writeInt(this.vehicleNullSurfaces.length);
		for (int i = 0; i < this.vehicleNullSurfaces.length; i++)
		{
			dos.writeInt(this.vehicleNullSurfaces[i].getWindingStart());
			dos.writeShort(this.vehicleNullSurfaces[i].getPlaneIndex());
			dos.writeByte(this.vehicleNullSurfaces[i].getSurfaceFlags());
			dos.writeInt(this.vehicleNullSurfaces[i].getWindingCount());
		}
		
		if (!tgeInterior)
		{
			if (this.vehiclePoints == null)
				this.vehiclePoints = new ItrPaddedPoint[0];
			dos.writeInt(this.vehiclePoints.length);
			for (int i = 0; i < this.vehiclePoints.length; i++)
			{
				dos.writePoint3F(this.vehiclePoints[i].getPoint());
			}
			
			if (this.vehiclePlanes == null)
				this.vehiclePlanes = new PlaneF[0];
			dos.writeInt(this.vehiclePlanes.length);
			for (int i = 0; i < this.vehiclePlanes.length; i++)
			{
				dos.writePlaneF(this.vehiclePlanes[i]);
			}
			
			if (this.vehicleWindings == null)
				this.vehicleWindings = new int[0];
			dos.writeInt(this.vehicleWindings.length);
			for (int i = 0; i < this.vehicleWindings.length; i++)
			{
				dos.writeInt(this.vehicleWindings[i]);
			}
			
			if (this.vehicleWindingIndices == null)
				this.vehicleWindingIndices = new TriFan[0];
			dos.writeInt(this.vehicleWindingIndices.length);
			for (int i = 0; i < this.vehicleWindingIndices.length; i++)
			{
				dos.writeInt(this.vehicleWindingIndices[i].getWindingStart());
				dos.writeInt(this.vehicleWindingIndices[i].getWindingCount());
			}
		}
		
		return true;
	}
	
	public MaterialList getMaterialList()
	{
		return this.materialList;
	}
	
	public Edge[] getEdges()
	{
		return this.edges;
	}
	
	public int getFileVersion()
	{
		return this.fileVersion;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof Interior))
			return false;
			
		Interior interior = (Interior) o;
		
		if (this.bspNodes.length != interior.bspNodes.length)
			return false;
			
		for (int i = 0; i < this.bspNodes.length; i++)
		{
			if ((this.fileVersion == interior.fileVersion) || (this.fileVersion < 14 && interior.fileVersion < 14))
			{
				if (!this.bspNodes[i].equals(interior.bspNodes[i]))
				{
					// System.out.println("BSPNode[" + i + "]: " + this.bspNodes[i] + " != " + interior.bspNodes[i] +
					// "!");
					return false;
				}
			} else if ((this.fileVersion >= 14 && interior.fileVersion < 14)
					|| this.fileVersion < 14 && interior.fileVersion >= 14)
			{
				BSPNode bsp1;
				BSPNode bsp2;
				if (this.fileVersion >= 14)
				{
					BSPNode toCopy = this.bspNodes[i];
					bsp1 = new BSPNode(toCopy.getPlaneIndex(), Util.convertInt(toCopy.getFrontIndex()),
							Util.convertInt(toCopy.getBackIndex()), toCopy.getTerminalZone());
							
					bsp2 = interior.bspNodes[i];
				} else
				{
					bsp1 = this.bspNodes[i];
					
					BSPNode toCopy = interior.bspNodes[i];
					bsp2 = new BSPNode(toCopy.getPlaneIndex(), Util.convertInt(toCopy.getFrontIndex()),
							Util.convertInt(toCopy.getBackIndex()), toCopy.getTerminalZone());
				}
				if (!bsp1.equals(bsp2))
				{
					// System.out.println("BSPNode[" + i + "]: " + this.bspNodes[i] + " != " + interior.bspNodes[i] +
					// "!");
					return false;
				}
				
				// if (bsp1.getFrontIndex() == 0 || bsp2.getBackIndex() == 0)
				// System.out.println("000BSPNode[" + i + "]: " + this.bspNodes[i] + " != " + interior.bspNodes[i] +
				// "!");
			}
		}
		
		return true;
	}
	
	public void dumpInfo(BufferedWriter bw) throws IOException
	{
		final String TAB = "    ";
		
		bw.write(TAB + "fileVersion = " + this.fileVersion + "\n");
		bw.write(TAB + "detailLevel = " + this.detailLevel + "\n");
		bw.write(TAB + "minPixels = " + this.minPixels + "\n");
		bw.write(TAB + "boundingBox = " + this.boundingBox + "\n");
		bw.write(TAB + "boundingSphere = " + this.boundingSphere + "\n");
		bw.write(TAB + "hasAlarmState = " + (this.hasAlarmState ? "true" : "false") + "\n");
		bw.write(TAB + "numLightStateEntries = " + this.numLightStateEntries + "\n");
		
		if (this.planes == null)
			this.planes = new PlaneF[0];
		bw.write(TAB + "numPlanes = " + this.planes.length + "\n");
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.planes.length; i++)
		{
			bw.write(TAB + TAB + this.planes[i] + "\n");
		}
		bw.write(TAB + "}\n\n");
		
		if (this.points == null)
			this.points = new ItrPaddedPoint[0];
		bw.write(TAB + "numPoints = " + this.points.length + "\n");
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.points.length; i++)
		{
			bw.write(TAB + TAB + this.points[i] + "\n");
		}
		bw.write(TAB + "}\n\n");
		
		bw.write(TAB + "pointVisibilities = " + Util.getArrayString(this.pointVisibility) + "\n\n");
		
		if (this.texGenEQs == null)
			this.texGenEQs = new TexGenList();
		bw.write(TAB + "numTexGenEQs = " + this.texGenEQs.size());
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.texGenEQs.size(); i++)
		{
			bw.write(TAB + TAB + this.texGenEQs.get(i) + "\n");
		}
		bw.write(TAB + "}\n\n");
		
		if (this.bspNodes == null)
			this.bspNodes = new BSPNode[0];
		bw.write(TAB + "numBSPNodes = " + this.bspNodes.length);
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.bspNodes.length; i++)
		{
			bw.write(TAB + TAB + this.bspNodes[i] + "\n");
		}
		bw.write(TAB + "}\n\n");
		
		if (this.bspSolidLeaves == null)
			this.bspSolidLeaves = new BSPSolidLeaf[0];
		bw.write(TAB + "numBSPSolidLeaves = " + this.bspSolidLeaves.length);
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.bspSolidLeaves.length; i++)
		{
			bw.write(TAB + TAB + this.bspSolidLeaves[i] + "\n");
		}
		bw.write(TAB + "}\n\n");
		
		// Material List
		if (this.materialList == null)
			this.materialList = new MaterialList();
		bw.write(TAB + "new MaterialList() {\n");
		this.materialList.dumpInfo(bw);
		bw.write(TAB + "}\n\n");
		
		// Windings
		bw.write(TAB + "windings = " + Util.getArrayString(this.windings) + "\n\n");
		
		// Winding Indices
		if (this.windingIndices == null)
			this.windingIndices = new TriFan[0];
		bw.write(TAB + "numWindingIndices = " + this.windingIndices.length);
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.windingIndices.length; i++)
		{
			bw.write(TAB + TAB + this.windingIndices[i] + "\n");
		}
		bw.write(TAB + "}\n\n");
		
		// Edges
		if (this.edges == null)
			this.edges = new Edge[0];
		bw.write(TAB + "numEdges = " + this.edges.length);
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.edges.length; i++)
		{
			bw.write(TAB + TAB + this.edges[i] + "\n");
		}
		bw.write(TAB + "}\n\n");
		
		// Zones
		if (this.zones == null)
			this.zones = new Zone[0];
		bw.write(TAB + "numZones = " + this.zones.length);
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.zones.length; i++)
		{
			bw.write(TAB + TAB + this.zones[i] + "\n");
		}
		bw.write(TAB + "}\n\n");
		
		// Zone Surfaces
		bw.write(TAB + "zoneSurfaces = " + Util.getArrayString(this.zoneSurfaces) + "\n\n");
		
		// Zone Static Meshes
		if (zoneStaticMeshes == null)
			zoneStaticMeshes = new int[0];
		bw.write(TAB + "zoneStaticMeshes = " + Util.getArrayString(this.zoneStaticMeshes) + "\n\n");
		
		// Zone Portal List
		bw.write(TAB + "zonePortalList = " + Util.getArrayString(this.zonePortalList) + "\n\n");
		
		// Portals
		if (this.portals == null)
			this.portals = new Portal[0];
		bw.write(TAB + "numPortals = " + this.portals.length);
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.portals.length; i++)
		{
			bw.write(TAB + TAB + this.portals[i] + "\n");
		}
		bw.write(TAB + "}\n\n");
		
		// Surfaces
		if (this.surfaces == null)
			this.surfaces = new Surface[0];
		bw.write(TAB + "numSurfaces = " + this.surfaces.length);
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.surfaces.length; i++)
		{
			bw.write(TAB + TAB + this.surfaces[i] + "\n");
		}
		bw.write(TAB + "}\n\n");
		
		// Normals
		if (this.fileVersion == 4 || this.fileVersion == 5)
		{
			if (this.normalsLegacy == null)
				this.normalsLegacy = new Point3F[0];
			bw.write(TAB + "numNormalsV4 = " + this.normalsLegacy.length);
			bw.write(TAB + "{\n");
			for (int i = 0; i < this.normalsLegacy.length; i++)
			{
				bw.write(TAB + TAB + this.normalsLegacy[i] + "\n");
			}
			bw.write(TAB + "}\n\n");
		} else {
			if (this.normals == null)
				this.normals = new Point3F[0];
			bw.write(TAB + "numNormals = " + this.normals.length);
			bw.write(TAB + "{\n");
			for (int i = 0; i < this.normals.length; i++)
			{
				bw.write(TAB + TAB + this.normals[i] + "\n");
			}
			bw.write(TAB + "}\n\n");
		}
		
		// NormalLMapIndices
		bw.write(TAB + "normalLMapIndices = " + Util.getArrayString(this.normalLMapIndices) + "\n\n");
		
		// AlarmLMapIndices
		bw.write(TAB + "alarmLMapIndices = " + Util.getArrayString(this.alarmLMapIndices) + "\n\n");
		
		// NullSurfaces
		if (this.nullSurfaces == null)
			this.nullSurfaces = new NullSurface[0];
		bw.write(TAB + "numNullSurfaces = " + this.nullSurfaces.length);
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.nullSurfaces.length; i++)
		{
			bw.write(TAB + TAB + this.nullSurfaces[i] + "\n");
		}
		bw.write(TAB + "}\n\n");
		
		// LightMaps
		if (this.lightMaps == null)
			this.lightMaps = new BufferedImage[0];
		bw.write(TAB + "numLightMaps = " + this.lightMaps.length);
		
		// LightDirMaps
		if (this.lightDirMaps == null)
			this.lightDirMaps = new BufferedImage[0];
		bw.write(TAB + "numLightDirMaps = " + this.lightDirMaps.length);
		
		// LightMapKeep
		bw.write(TAB + "lightMapKeep = " + Util.getArrayString(this.lightMapKeep) + "\n\n");
		
		// Solid Leaf Surfaces
		bw.write(TAB + "solidLeafSurfaces = " + Util.getArrayString(this.solidLeafSurfaces) + "\n\n");
		
		// Animated Lights
		if (this.animatedLights == null)
			this.animatedLights = new AnimatedLight[0];
		bw.write(TAB + "numAnimatedLights = " + this.animatedLights.length);
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.animatedLights.length; i++)
		{
			bw.write(TAB + TAB + this.animatedLights[i] + "\n");
		}
		bw.write(TAB + "}\n\n");
		
		// Light States
		if (this.lightStates == null)
			this.lightStates = new LightState[0];
		bw.write(TAB + "numLightStates = " + this.lightStates.length);
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.lightStates.length; i++)
		{
			bw.write(TAB + TAB + this.lightStates[i] + "\n");
		}
		bw.write(TAB + "}\n\n");
		
		// Light State Data
		if (this.lightStateData == null)
			this.lightStateData = new LightStateData[0];
		bw.write(TAB + "numLightStateData = " + this.lightStateData.length);
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.lightStateData.length; i++)
		{
			bw.write(TAB + TAB + this.lightStateData[i] + "\n");
		}
		bw.write(TAB + "}\n\n");
		
		// State Data Buffer
		bw.write(TAB + "stateDataBuffer = " + Util.getArrayString(this.stateDataBuffer) + "\n\n");
		
		// Name Buffer
		bw.write(TAB + "nameBuffer = " + Util.getArrayString(this.nameBuffer) + "\n\n");
		
		// Sub Objects
		if (this.subObjects == null)
			this.subObjects = new InteriorSubObject[0];
		bw.write(TAB + "numSubObjects = " + this.subObjects.length);
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.subObjects.length; i++)
		{
			bw.write(TAB + TAB + this.subObjects[i] + "\n");
		}
		bw.write(TAB + "}\n\n");
		
		// Convex Hulls
		if (this.convexHulls == null)
			this.convexHulls = new ConvexHull[0];
		bw.write(TAB + "numConvexHulls = " + this.convexHulls.length);
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.convexHulls.length; i++)
		{
			bw.write(TAB + TAB + this.convexHulls[i] + "\n");
		}
		bw.write(TAB + "}\n\n");
		
		// Convex Hull Emit Strings
		bw.write(TAB + "convexHullEmitStrings = " + Util.getArrayString(this.convexHullEmitStrings) + "\n\n");
		
		// Hull Indices
		bw.write(TAB + "hullIndices = " + Util.getArrayString(this.hullIndices) + "\n\n");
		
		// Hull Plane Indices
		bw.write(TAB + "hullPlaneIndices = " + Util.getArrayString(this.hullPlaneIndices) + "\n\n");
		
		// Hull Emit String Indices
		bw.write(TAB + "hullEmitStringIndices = " + Util.getArrayString(this.hullEmitStringIndices) + "\n\n");
		
		// Hull Surfaces Indices
		bw.write(TAB + "hullSurfaceIndices = " + Util.getArrayString(this.hullSurfaceIndices) + "\n\n");
		
		// Poly List Planes
		bw.write(TAB + "polyListPlanes = " + Util.getArrayString(this.polyListPlanes) + "\n\n");
		
		// Poly List Points
		bw.write(TAB + "polyListPoints = " + Util.getArrayString(this.polyListPoints) + "\n\n");
		
		// Poly List Strings
		bw.write(TAB + "polyListStrings = " + Util.getArrayString(this.polyListStrings) + "\n\n");
		
		// Coord Bins
		if (this.coordBins == null)
			this.coordBins = new CoordBin[0];
		bw.write(TAB + "numCoordBins = " + this.coordBins.length);
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.coordBins.length; i++)
		{
			bw.write(TAB + TAB + this.coordBins[i] + "\n");
		}
		bw.write(TAB + "}\n\n");
		
		// Coord Bin Indices
		bw.write(TAB + "coordBinIndices = " + Util.getArrayString(this.coordBinIndices) + "\n\n");
		
		bw.write(TAB + "coordBinMode = " + this.coordBinMode + "\n");
		
		bw.write(TAB + "baseAmbient = " + this.baseAmbient + "\n");
		bw.write(TAB + "alarmAmbient = " + this.alarmAmbient + "\n");
		
		// Static Meshes
		if (this.staticMeshes == null)
			this.staticMeshes = new InteriorSimpleMesh[0];
		bw.write(TAB + "numStaticMeshes = " + this.staticMeshes.length);
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.staticMeshes.length; i++)
		{
			bw.write(TAB + TAB + "new StaticMesh(" + i + ") {\n");
			this.staticMeshes[i].dumpInfo(bw);
			bw.write(TAB + TAB + "}\n\n");
		}
		bw.write(TAB + "}\n\n");
		
		// Tex Matrices
		if (this.texMatrices == null)
			this.texMatrices = new TexMatrix[0];
		bw.write(TAB + "numTexMatrices = " + this.texMatrices.length);
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.texMatrices.length; i++)
		{
			bw.write(TAB + TAB + this.texMatrices[i] + "\n");
		}
		bw.write(TAB + "}\n\n");
		
		// Tex Mat Indices
		if (texMatIndices == null)
			texMatIndices = new int[0];
		bw.write(TAB + "texMatIndices = " + Util.getArrayString(this.texMatIndices) + "\n\n");
		
		bw.write(TAB + "lightMapBorderSize = " + this.lightMapBorderSize + "\n");
	}
	
	public void dumpVehicleCollisions(BufferedWriter bw) throws IOException
	{
		final String TAB = "    ";
		
		// Vehicle Convex Hulls
		bw.write(TAB + "numVehicleConvexHulls = " + this.vehicleConvexHulls.length);
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.vehicleConvexHulls.length; i++)
		{
			bw.write(TAB + TAB + this.vehicleConvexHulls[i] + "\n");
		}
		bw.write(TAB + "}\n\n");
		
		// VehicleConvexHullEmitStrings
		bw.write(TAB + "vehicleConvexHullEmitStrings = " + Util.getArrayString(this.vehicleConvexHullEmitStrings)
				+ "\n\n");
				
		// VehicleHullIndices
		bw.write(TAB + "vehicleHullIndices = " + Util.getArrayString(this.vehicleHullIndices) + "\n\n");
		
		// VehicleHullPlaneIndices
		bw.write(TAB + "vehicleHullPlaneIndices = " + Util.getArrayString(this.vehicleHullPlaneIndices) + "\n\n");
		
		// VehicleHullEmitStringIndices
		bw.write(TAB + "vehicleHullEmitStringIndices = " + Util.getArrayString(this.vehicleHullEmitStringIndices)
				+ "\n\n");
				
		// VehicleHullSurfaceIndices
		bw.write(TAB + "vehicleHullSurfaceIndices = " + Util.getArrayString(this.vehicleHullSurfaceIndices) + "\n\n");
		
		// VehiclePolyListPlanes
		bw.write(TAB + "vehiclePolyListPlanes = " + Util.getArrayString(this.vehiclePolyListPlanes) + "\n\n");
		
		// VehiclePolyListPoints
		bw.write(TAB + "vehiclePolyListPoints = " + Util.getArrayString(this.vehiclePolyListPoints) + "\n\n");
		
		// VehiclePolyListStrings
		bw.write(TAB + "vehiclePolyListStrings = " + Util.getArrayString(this.vehiclePolyListStrings) + "\n\n");
		
		// VehicleNullSurfaces
		bw.write(TAB + "numVehicleNullSurfaces = " + this.vehicleNullSurfaces.length);
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.vehicleNullSurfaces.length; i++)
		{
			bw.write(TAB + TAB + this.vehicleNullSurfaces[i] + "\n");
		}
		bw.write(TAB + "}\n\n");
		
		// VehiclePoints
		bw.write(TAB + "numVehiclePoints = " + this.vehiclePoints.length);
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.vehiclePoints.length; i++)
		{
			bw.write(TAB + TAB + this.vehiclePoints[i] + "\n");
		}
		bw.write(TAB + "}\n\n");
		
		// VehiclePlanes
		bw.write(TAB + "numVehiclePlanes = " + this.vehiclePlanes.length);
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.vehiclePlanes.length; i++)
		{
			bw.write(TAB + TAB + this.vehiclePlanes[i] + "\n");
		}
		bw.write(TAB + "}\n\n");
		
		// VehicleWindings
		bw.write(TAB + "vehicleWindings = " + Util.getArrayString(this.vehicleWindings) + "\n\n");
		
		// VehicleWindingIndices
		bw.write(TAB + "numVehicleWindingIndices = " + this.vehicleWindingIndices.length);
		bw.write(TAB + "{\n");
		for (int i = 0; i < this.vehicleWindingIndices.length; i++)
		{
			bw.write(TAB + TAB + this.vehicleWindingIndices[i] + "\n");
		}
		bw.write(TAB + "}\n\n");
	}
	
	public void exportMap(BufferedWriter bw) throws IOException
	{
		final String TAB = "    ";
		
		bw.write("// worldspawn\n");
		bw.write("{\n");
		
		Map<String, String> propertyMap = new HashMap<String, String>();
		
		propertyMap.put("classname", "worldspawn");
		propertyMap.put("detail_number", "" + this.detailLevel);
		propertyMap.put("min_pixels", "" + this.minPixels);
		propertyMap.put("geometry_scale", "32.0");
		propertyMap.put("light_geometry_scale", "32.0");
		propertyMap.put("ambient_color",
				this.baseAmbient.getRed() + " " + this.baseAmbient.getGreen() + " " + this.baseAmbient.getBlue());
		propertyMap.put("emergency_ambient_color",
				this.alarmAmbient.getRed() + " " + this.alarmAmbient.getGreen() + " " + this.alarmAmbient.getBlue());
		propertyMap.put("mapversion", "220");
		
		String[] keys = new String[propertyMap.size()];
		propertyMap.keySet().toArray(keys);
		String[] values = new String[propertyMap.size()];
		propertyMap.values().toArray(values);
		
		for (int i = 0; i < propertyMap.size(); i++)
		{
			bw.write(TAB + "\"" + keys[i] + "\" \"" + values[i] + "\"\n");
		}
		
		bw.write(TAB + "\n");
		
		Brush[] brushes = this.decompileBrushes();
		for (int i = 0; i < brushes.length; i++)
		{
			bw.write(TAB + "// Brush " + (i + 1) + "\n");
			bw.write(TAB + "{\n");
			
			List<BrushPlane> planes = brushes[i].getPlanes();
			
			// int index = 0;
			for (BrushPlane plane : planes)
			{
				// index++;
				// bw.write("// plane " + index + "\n");
				bw.write(TAB + TAB + plane + "\n");
			}
			bw.write(TAB + "}\n\n");
		}
		
		bw.write("}\n");
	}
	
	public void exportCsx(BufferedWriter bw) throws IOException
	{
		// final String TAB = " ";
		// Based on my analysis, CSX files are really just DIF files, but written in an XML format. This may come in
		// very handy... - Anthony
		bw.write(
				"<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\" ?>\n\t<ConstructorScene version=\"4\">\n\t\t<DetailLevels current=\"0\">\n\t\t\t<DetailLevel>\n\t\t\t\t<InteriorMap brushScale=\"32\" lightScale=\"32\">\n\t\t\t\t\t<Entities nextEntityID=\"1\">\n\t\t\t\t\t\t<Entity id=\"0\" classname=\"worldspawn\">\n");
		bw.write("\t\t\t\t\t\t\t<Properties detail_number=\"0\" min_pixels=\"" + this.minPixels
				+ "\" mapVersion=\"220\" />\n");
		bw.write("\t\t\t\t\t\t</Entity>\n\t\t\t\t\t</Entities>\n\t\t\t\t\t<Brushes>\n");
		
		Brush[] brushes = this.decompileBrushes();
		brushes = fixBrushes(brushes);
		for (int curBrushIndex = 0; curBrushIndex < brushes.length; curBrushIndex++)
		{
			Brush curBrush = brushes[curBrushIndex];
			ConvexHull curHull = curBrush.getHull();
			List<Point3F> curVertices = curBrush.getPoints();
			List<BrushPlane> curFaces = curBrush.getPlanes();
			int numVertices = curVertices.size();
			int numFaces = curFaces.size();
			// int numEdges = ((curHull.getHullCount() - curHull.getHullStart()) / numFaces);
			// List<Integer> curIndices = getIndices(curBrush.getHull().getHullStart(),
			// curBrush.getHull().getHullCount(), false);
			// int numIndices = curIndices.size();
			// boolean isMalformed = numVertices - numEdges + numFaces != 2;
			// if (isMalformed)
			// continue;
			
			bw.write("\t\t\t\t\t\t<Brush id=\"" + curBrushIndex + "\" owner=\"0\" type=\"0\" pos=\"" + curHull.getMinX()
					+ " " + curHull.getMinY() + " " + curHull.getMinZ() + "\" transform=\"1 0 0 " + curHull.getMinX()
					+ " 0 1 0 " + curHull.getMinY() + " 0 0 1 " + curHull.getMinZ()
					+ " 0 0 0 1\" group=\"-1\" locked=\"0\" nextFaceID=\"" + (numFaces + 1) + "\" nextVertexID=\""
					+ (numVertices + 1) + "\">\n\t\t\t\t\t\t<Vertices>\n");
			for (int j = 0; j < numVertices; j++)
			{
				// write out the vertices for the brush - they are always all included.
				Point3F posVertex = curVertices.get(j);
				bw.write("\t\t\t\t\t\t\t<Vertex pos=\"" + posVertex.getPointString() + "\" />\n");
			}
			bw.write("\t\t\t\t\t\t</Vertices>\n");
			for (int j = 0; j < curFaces.size(); j++)
			{
				BrushPlane curFace = curFaces.get(j);
				String album;
				String texture;
				if (curFace.getTexture().contains("/"))
				{
					album = curFace.getTexture().split("/")[0];
					texture = curFace.getTexture().split("/")[1];
				} else
				{
					album = "";
					texture = curFace.getTexture();
				}
				// Put the original brush that isn't changed into MAP's format but uses XYZD here...
				bw.write("\t\t\t\t\t\t<Face id=\"" + j + "\" plane=\"" + curFace.getPlane().getX() + " "
						+ curFace.getPlane().getY() + " " + curFace.getPlane().getZ() + " " + curFace.getPlane().getD()
						+ "\"  album=\"" + album + "\" material=\"" + texture
						+ "\" texgens=\"-1 0 0 16 0 -1 0 -16 0 1 1\" texRot=\"0\" texScale=\"1 1\" texDiv=\"128 128\">\n");
				StringBuilder sb = new StringBuilder();
				for (int windingIndex : curFace.getIndices())
				{
					sb.append(" " + (windingIndex - curBrush.getHull().getHullStart()));
				}
				bw.write("\t\t\t\t\t\t\t<Indices indices=\"" + sb.toString() + "\">\n");
				bw.write("\t\t\t\t\t\t</Face>\n");
			}
			bw.write("\t\t\t\t\t</Brush>\n");
		}
		bw.write(
				"\t\t\t\t</Brushes>\n\t\t\t</InteriorMap>\n\t\t</DetailLevel>\n\t</DetailLevels>\n</ConstructorScene>");
	}
	
	public void exportRaw(BufferedWriter bw) throws IOException
	{
		// final String TAB = " ";
		
		TriangleF[] tris = getTriangles();
		
		for (TriangleF tri : tris)
		{
			bw.write(tri.getPoint1().getPointString() + " ");
			bw.write(tri.getPoint2().getPointString() + " ");
			bw.write(tri.getPoint3().getPointString() + "\n");
		}
	}
	
	private TriangleF[] getTriangles()
	{
		List<TriangleF> tris = new ArrayList<TriangleF>();
		
		for (int i = 0; i < this.convexHulls.length; i++)
		{
			ConvexHull hull = this.convexHulls[i];
			
			int surfaceStart = hull.getSurfaceStart();
			int surfaceCount = hull.getSurfaceCount();
			
			for (int j = surfaceStart; j < surfaceStart + surfaceCount; j++)
			{
				Surface surface = this.surfaces[j];
				
				int windingStart = surface.getWindingStart();
				int windingCount = surface.getWindingCount();
				
				List<Point3F> points = new ArrayList<Point3F>();
				for (int k = windingStart; k < windingStart + windingCount; k++)
				{
					int winding = this.windings[k];
					points.add(this.points[winding].getPoint());
				}
				
				/*
				 * System.out.println("IN THIS FACE:"); for (Point3F pt : points) { System.out.println(pt); }
				 */
				
				for (int k = 0; k < points.size() - 2; k++)
				{
					TriangleF tri;
					if (k % 2 == 0)
						tri = new TriangleF(points.get(k), points.get(k + 1), points.get(k + 2));
					else
						tri = new TriangleF(points.get(k + 2), points.get(k + 1), points.get(k));
					tris.add(tri);
				}
				
				/*
				 * if (points.size() == 3) { tris.add(new TriangleF(points.get(0), points.get(1), points.get(2))); }
				 * else if (points.size() == 4) { QuadF quad = new QuadF(points.get(0), points.get(1), points.get(2),
				 * points.get(3)); TriangleF[] faceTris = quad.getTriangles();
				 * 
				 * for (TriangleF tri : faceTris) { tris.add(tri); } } else { System.out.println("A face with " +
				 * points.size() + " points! CALLING ALL WHIRLIGIG's TO THE MEETING ROOM!"); continue; }
				 */
			}
		}
		
		TriangleF[] result = new TriangleF[tris.size()];
		tris.toArray(result);
		
		return result;
	}
	
	public Brush[] fixBrushes(Brush[] brushes)
	{
		for (int curBrushIndex = 0; curBrushIndex < brushes.length; curBrushIndex++)
		{
			Brush curBrush = brushes[curBrushIndex];
			ConvexHull curHull = curBrush.getHull();
			List<Point3F> curVertices = curBrush.getPoints();
			List<BrushPlane> curFaces = curBrush.getPlanes();
			int numVertices = curVertices.size();
			System.out.println("(8) Number of Vertices for Brush " + curBrushIndex + ": " + numVertices);
			int numFaces = curFaces.size();
			System.out.println("(6) Number of Faces for Brush " + curBrushIndex + ": " + numFaces);
			
			List<Integer> curIndices = getIndices(curBrush.getHull().getHullStart(), curBrush.getHull().getHullCount(),
					false);
			int numIndices = curIndices.size();
			
			List<Integer> timesVertexRepeated = new ArrayList<Integer>();
			for (int i = 0; i < numIndices; i++)
			{
				int numMatches = Util.arrayMatch(curIndices, i);
				System.out.println("Match Count: " + numMatches);
				if (numMatches > 0)
				{
					timesVertexRepeated.add(numMatches);
				} else
				{
					break;
				}
			}
			
			// This isn't always true, but let's assume it is for now.
			int numEdges = (curHull.getHullCount() * 3 / 2);
			System.out.println("(12) Number of Edges for Brush " + curBrushIndex + ": " + numEdges);
			
			int numMissingFaces = Math.abs(numVertices - numEdges + numFaces - 2);
			boolean isBeyondRepair = numFaces < 4 || numVertices < 3;
			
			if (numMissingFaces > 0 && !isBeyondRepair)
			{
				
				System.out.println(
						"(0 or 2) Number of Missing Faces for Brush " + curBrushIndex + ": " + numMissingFaces);
						
				if (timesVertexRepeated.size() > 0
						&& Collections.min(timesVertexRepeated) == Collections.max(timesVertexRepeated))
				{
					// top and bottom faces missing
					/*
					 * // need to find the four vertices never used in combination by the current faces somehow int
					 * xAxisChanges = 0; int yAxisChanges = 0; int zAxisChanges = 0; for (int j = 0; j < numFaces; j++)
					 * { xAxisChanges += Math.abs(curFaces.get(j).getPlane().getX()); } for (int j = 0; j < numFaces;
					 * j++) { yAxisChanges += Math.abs(curFaces.get(j).getPlane().getY()); } for (int j = 0; j <
					 * numFaces; j++) { zAxisChanges += Math.abs(curFaces.get(j).getPlane().getZ()); } if (xAxisChanges
					 * > 0) { // "1 0 0 " + curHull.getMinX(); // "1 0 0 " + curHull.getMaxX(); } if (yAxisChanges > 0)
					 * { // "0 1 0 " + curHull.getMinY(); // "0 1 0 " + curHull.getMaxY(); } if (zAxisChanges > 0) { //
					 * "0 0 1 " + curHull.getMinZ(); // "0 0 1 " + curHull.getMaxZ(); }
					 */
					// for now assume the face is the same one from the previous valid brush
					// and since we're validating these in a row, the previous valid brush should be the last one
					for (int i = 0; i < numMissingFaces; i++)
					{
						System.out.println("Adding plane " + i + " from brush" + (curBrushIndex - 1));
						BrushPlane newBrushPlane = brushes[curBrushIndex - 1].getPlanes().get(i);
						List<Integer> newIndices = new ArrayList<Integer>();
						for (int j = 0; j < newBrushPlane.getIndices().size(); j++)
						{
							newIndices.add(newBrushPlane.getIndices().get(j)
									- brushes[curBrushIndex - 1].getHull().getHullStart()
									+ curBrush.getHull().getHullStart());
						}
						curBrush.addPlane(new BrushPlane(newIndices, newBrushPlane.getPlane(),
								newBrushPlane.getTexture(), newBrushPlane.getTexturePlane1(),
								newBrushPlane.getTexturePlane2(), newBrushPlane.getTextureRotation(),
								newBrushPlane.getTextureScaleX(), newBrushPlane.getTextureScaleY()));
					}
				} else
				{
					// adjacent faces missing
					// Use the amount of planes each vertex is sharing to tell where the new face goes.
					// Loop through and populate the faces we're going to make.
					for (int i = 0; i < numMissingFaces; i++)
					{
						// the vertices we'll use to make the new face. Translate these to a plane.
						List<Point3F> verticesNew = new ArrayList<Point3F>();
						for (int j = 0; j < timesVertexRepeated.size(); j++)
						{
							if (timesVertexRepeated.get(j) == Collections.min(timesVertexRepeated))
							{
								verticesNew.add(curVertices.get(j));
							}
							if (timesVertexRepeated.get(j) < Collections.max(timesVertexRepeated)
									&& verticesNew.indexOf(curBrush.getPoints().get(j)) == -1
									&& verticesNew.size() % 4 != 0)
							{
								verticesNew.add(curVertices.get(j));
							}
						}
						// curBrush.addPlane(new BrushPlane(verticesToFacePlane(verticesNew.get(0), verticesNew.get(1),
						// verticesNew.get(2))));
					}
					// we now have the four vertices these brushes use. Use them to make the planes.
					// need a function to make a plane from four vertices here
				}
			} else
			{
				// brush is already valid, validate the next brush
				continue;
			}
		}
		return brushes;
	}
	
	public Brush orderIndices(Brush brush)
	{
		// must be counter-clockwise
		// figure this out eventually
		return brush;
	}
	
	public PlaneF verticesToFacePlane(Point3F Point1, Point3F Point2, Point3F Point3)
	{
		float x1 = Point1.getX();
		float y1 = Point1.getY();
		float z1 = Point1.getZ();
		
		float x2 = Point2.getX();
		float y2 = Point2.getY();
		float z2 = Point2.getZ();
		
		float x3 = Point3.getX();
		float y3 = Point3.getY();
		float z3 = Point3.getZ();
		
		// Cross computing vectors, as Whirli called it.
		float x = y1 * z2 - y1 * z3 + y3 * z1 - y2 * z1 + y2 * z3 - y3 * z2;
		float y = x1 * z2 - x1 * z3 + x3 * z1 - x2 * z1 + y2 * z3 - x3 * z2;
		float z = x1 * y2 - x1 * y3 + x3 * y1 - x2 * y1 + x2 * y3 - x3 * y2;
		float d = x1 * y2 * z3 - x1 * z2 * y3 + y1 * z2 * x3 - y1 * x2 * z3 + z1 * x2 * y3 - z1 * y2 * x3;
		
		return new PlaneF(x, y, z, d);
	}
	
	private Brush[] decompileBrushes()
	{
		List<Brush> brushes = new ArrayList<Brush>();
		
		// Test Brush
		/*
		 * Brush testBrush = new Brush();
		 * 
		 * // Test Brush Plane 1 Point3F test1Point1 = new Point3F(16, -16, -16); Point3F test1Point2 = new Point3F(16,
		 * -16, 16); Point3F test1Point3 = new Point3F(16, 16, 16); PlaneF test1Plane1 = new PlaneF(0, 1, 0, 16); PlaneF
		 * test1Plane2 = new PlaneF(0, 0, -1, -16); testBrush.addPlane(new BrushPlane(test1Point1, test1Point2,
		 * test1Point3, "tile_advanced", test1Plane1, test1Plane2, 0, 1, 1));
		 * 
		 * // Test Brush Plane 2 Point3F test2Point1 = new Point3F(-16, -16, -16); Point3F test2Point2 = new
		 * Point3F(-16, 16, -16); Point3F test2Point3 = new Point3F(-16, 16, 16); PlaneF test2Plane1 = new PlaneF(0, -1,
		 * 0, 16); PlaneF test2Plane2 = new PlaneF(0, 0, -1, -16); testBrush.addPlane(new BrushPlane(test2Point1,
		 * test2Point2, test2Point3, "tile_advanced", test2Plane1, test2Plane2, 0, 1, 1));
		 * 
		 * brushes.add(testBrush);
		 */
		
		// Loop Through Brushes
		for (int i = 0; i < this.convexHulls.length; i++)
		{
			Brush brush = new Brush(convexHulls[i]);
			int surfaceStart = this.convexHulls[i].getSurfaceStart();
			int surfaceCount = this.convexHulls[i].getSurfaceCount();
			
			boolean invalidBrush = false;
			
			int hullStart = this.convexHulls[i].getHullStart();
			int hullCount = this.convexHulls[i].getHullCount();
			
			for (int j = hullStart; j < hullStart + hullCount; j++)
			{
				Point3F point = this.points[this.windings[j]].getPoint();
				brush.addPoint(point);
			}
			
			// Loop Through Surfaces
			for (int j = surfaceStart; j < surfaceStart + surfaceCount; j++)
			{
				// if (j >= this.surfaces.length || j < 0)
				// continue;
				Surface surface = this.surfaces[this.hullSurfaceIndices[j]];
				
				// Make sure it's valid
				// if (surface.getPlaneIndex() >= this.planes.length || surface.getPlaneIndex() < 0)
				// continue;
				
				/*
				 * PlaneF texPlane1 = new PlaneF(0, -1, 0, 16); PlaneF texPlane2 = new PlaneF(0, 0, -1, -16); float
				 * textureRotation = 0; float textureScaleX = 1; float textureScaleY = 1; String texture =
				 * this.materialList.getMaterials().get(surface.getTextureIndex()).getName();
				 * 
				 * int planeIndex = surface.getPlaneIndex(); PlaneF plane = this.planes[planeIndex];
				 * 
				 * BrushPlane brushPlane = new BrushPlane(plane, texture, texPlane1, texPlane2, textureRotation,
				 * textureScaleX, textureScaleY); brush.addPlane(brushPlane);
				 */
				
				int windingStart = surface.getWindingStart();
				int windingCount = surface.getWindingCount();
				
				List<Point3F> points = getPoints(windingStart, windingCount, false);
				List<Integer> indices = getIndices(windingStart, windingCount, false);
				
				if (points.size() < 3)
				{
					invalidBrush = true;
					System.out.println("Invalid Brush!");
					break;
				}
				
				Point3F point1 = points.get(0);
				Point3F point2 = points.get(1);
				Point3F point3 = points.get(2);
				
				/*
				 * if (point1.hasZero() || point2.hasZero() || point3.hasZero()) { points = getPoints(windingStart,
				 * windingCount, true); point1 = points.get(0); point2 = points.get(1); point3 = points.get(2); }
				 */
				
				PlaneF texPlane1 = new PlaneF(0, -1, 0, 16);
				PlaneF texPlane2 = new PlaneF(0, 0, -1, -16);
				float textureRotation = 0;
				float textureScaleX = 1;
				float textureScaleY = 1;
				String texture = surface.getTexture();
				
				BrushPlane brushPlane = new BrushPlane(indices, point1, point2, point3,
						this.planes[surface.getPlaneIndex()], texture, texPlane1, texPlane2, textureRotation,
						textureScaleX, textureScaleY);
				brush.addPlane(brushPlane);
			}
			
			if (brush.getPlanes().size() > 0 && !invalidBrush)
				brushes.add(brush);
		}
		
		Brush[] result = new Brush[brushes.size()];
		brushes.toArray(result);
		
		return result;
	}
	
	public List<Point3F> getPoints(int windingStart, int windingCount, boolean reverse)
	{
		List<Point3F> points = new ArrayList<Point3F>();
		if (reverse)
		{
			for (int k = windingStart + windingCount - 1; k >= windingStart; k--)
			{
				int winding = this.windings[k];
				Point3F point = this.points[winding].getPoint().mul(32);
				points.add(point);
			}
		} else
		{
			for (int k = windingStart; k < windingStart + windingCount; k++)
			{
				int winding = this.windings[k];
				Point3F point = this.points[winding].getPoint().mul(32);
				points.add(point);
			}
		}
		return points;
	}
	
	public List<Integer> getIndices(int windingStart, int windingCount, boolean reverse)
	{
		List<Integer> points = new ArrayList<Integer>();
		if (reverse)
		{
			for (int k = windingStart + windingCount - 1; k >= windingStart; k--)
			{
				int winding = this.windings[k];
				points.add(winding);
			}
		} else
		{
			for (int k = windingStart; k < windingStart + windingCount; k++)
			{
				int winding = this.windings[k];
				points.add(winding);
			}
		}
		return points;
	}
	
	/*public boolean processCollada(Collada dae)
	{
		List<Material> materials = dae.getLibraryMaterials().getMaterials();
		
		this.materialList = new MaterialList();
		for (Material material : materials)
		{
			this.materialList.getMaterials().add(new TorqueMaterial(material.getName()));
		}
		
		List<Geometry> geometries = dae.getLibraryGeometries().getGeometries();
		
		List<ItrPaddedPoint> points = new ArrayList<ItrPaddedPoint>();
		List<Integer> windings = new ArrayList<Integer>();
		List<PlaneF> planes = new ArrayList<PlaneF>();
		List<Surface> surfaces = new ArrayList<Surface>();
		List<ConvexHull> hulls = new ArrayList<ConvexHull>();
		
		for (Geometry geo : geometries)
		{
			Mesh m = geo.getMesh();
			for (Primitives p : m.getPrimitives())
			{
				// String material = p.getMaterial();
				// int materialIndex = this.materialList.getIndex(material);
				int[] data = p.getData();
				
				int startIndex = points.size();
				int count = data.length;
				
				for (int i = 0; i < data.length; i += 3)
				{
					Point3F point = new Point3F(data[i], data[i + 1], data[i + 2]);
					
					points.add(new ItrPaddedPoint(point));
				}
				
				for (int i = startIndex; i < startIndex + count; i++)
				{
					windings.add(i);
				}
				float minX = 0;
				float minY = 0;
				float minZ = 0;
				float maxX = 0;
				float maxY = 0;
				float maxZ = 0;
				int hullStart = startIndex;
				int surfaceStart = 0;
				int planeStart = 0;
				short hullCount = (short) count;
				short surfaceCount = 0;
				int polyListPlaneStart = 0;
				int polyListPointStart = 0;
				int polyListStringStart = 0;
				short searchTag = 0;
				boolean staticMesh = false;
				
				ConvexHull hull = new ConvexHull(minX, maxX, minY, maxY, minZ, maxZ, hullStart, surfaceStart,
						planeStart, hullCount, surfaceCount, polyListPlaneStart, polyListPointStart,
						polyListStringStart, searchTag, staticMesh);
						
				hulls.add(hull);
				
			}
		}
		
		this.points = new ItrPaddedPoint[points.size()];
		points.toArray(this.points);
		
		this.windings = new int[windings.size()];
		for (int i = 0; i < windings.size(); i++)
		{
			this.windings[i] = windings.get(i);
		}
		
		this.convexHulls = new ConvexHull[hulls.size()];
		hulls.toArray(this.convexHulls);
		
		this.planes = new PlaneF[planes.size()];
		planes.toArray(this.planes);
		
		this.surfaces = new Surface[surfaces.size()];
		surfaces.toArray(this.surfaces);
		
		this.pointVisibility = new byte[0];
		this.texGenEQs = new TexGenList();
		this.bspNodes = new BSPNode[0];
		this.bspSolidLeaves = new BSPSolidLeaf[0];
		this.windingIndices = new TriFan[0];
		this.zones = new Zone[0];
		this.zoneSurfaces = new short[0];
		this.zonePortalList = new short[0];
		this.portals = new Portal[0];
		this.normalLMapIndices = new int[0];
		this.alarmLMapIndices = new int[0];
		this.nullSurfaces = new NullSurface[0];
		this.lightMaps = new BufferedImage[0];
		this.lightDirMaps = new BufferedImage[0];
		this.lightMapKeep = new boolean[0];
		this.solidLeafSurfaces = new int[0];
		this.animatedLights = new AnimatedLight[0];
		this.lightStates = new LightState[0];
		this.lightStateData = new LightStateData[0];
		this.stateDataBuffer = new byte[0];
		this.nameBuffer = new char[0];
		this.subObjects = new InteriorSubObject[0];
		this.convexHullEmitStrings = new byte[0];
		this.hullIndices = new int[0];
		this.hullPlaneIndices = new short[0];
		this.hullEmitStringIndices = new int[0];
		this.hullSurfaceIndices = new int[0];
		this.polyListPlanes = new short[0];
		this.polyListPoints = new int[0];
		this.polyListStrings = new byte[0];
		
		final int numCoordBins = 16;
		
		this.coordBins = new CoordBin[numCoordBins * numCoordBins];
		this.coordBinIndices = new short[0];
		this.coordBinMode = 0;
		this.baseAmbient = new ColorI((byte) 0, (byte) 0, (byte) 0, (byte) 255);
		this.alarmAmbient = new ColorI((byte) 0, (byte) 0, (byte) 0, (byte) 255);
		
		this.texMatrices = new TexMatrix[0];
		this.texMatIndices = new int[0];
		
		return true;
	}*/
	
	public boolean processOBJ(OBJModel model)
	{
		this.boundingBox = new Box3F(new Point3F(), new Point3F());
		
		this.materialList = new MaterialList();
		this.texGenEQs = new TexGenList();
		this.points = new ItrPaddedPoint[model.getPositions().size()];
		for (int i = 0; i < this.points.length; i++)
		{
			Point3F point = model.getPositions().get(i);
			
			this.points[i] = new ItrPaddedPoint(point);
		}
		List<Surface> surfaces = new ArrayList<Surface>();
		List<Integer> hullSurfaceIndices = new ArrayList<Integer>();
		List<Integer> windings = new ArrayList<Integer>();
		this.convexHulls = new ConvexHull[1];//model.getMeshes().size()];
		//for (int i = 0; i < this.convexHulls.length; i++)
		//{
			List<OBJIndex> faces = model.getIndices();
			//int surfaceStart = surfaces.size();
			short surfaceCount = 0;
			for (int j = 0; j < faces.size(); j++)
			{
				OBJIndex face = faces.get(j);
				String texture = "POTATO";
				if (!materialList.contains(texture))
					materialList.addMaterial(texture);
				//Point3I vIndex = face.getVertexIndex();
				//Point3I tIndex = face.getTextureIndex();
				
				//Point3F point = model.getPositions().get(face.getVertexIndex());
				
				//int ix = vIndex.getX();
				//int iy = vIndex.getY();
				//int iz = vIndex.getZ();
				
				//Point3F p1 = this.points[ix].getPoint();
				int windingStart = 0;//windings.size();
				int windingCount = model.getPositions().size();//3;
				
				windings.add(face.getVertexIndex());
				windings.add(face.getVertexIndex()+1);
				windings.add(face.getVertexIndex()+2);
				
				short planeIndex = 0;
				short textureIndex = 0;
				int texGenIndex = 0;
				short lightCount = 0;
				byte surfaceFlags = 0;
				int fanMask = 0;
				int lightStateInfoStart = 0;
				int mapOffsetX = 0;
				int mapOffsetY = 0;
				int mapSizeX = 0;
				int mapSizeY = 0;
				boolean unused = false;
				int brushId = 0;
				
				Surface surface = new Surface(windingStart, planeIndex, textureIndex, texGenIndex, lightCount, surfaceFlags, windingCount, fanMask, lightStateInfoStart, mapOffsetX, mapOffsetY, mapSizeX, mapSizeY, unused, brushId);
				surfaces.add(surface);
				
				surfaceCount++;

				//hullSurfaceIndices.add(j);
			}

			surfaceCount = 5;
			for (int j = 0; j < surfaceCount; j++)
			{
				hullSurfaceIndices.add(j);
			}

			
			//if (surfaceStart == surfaces.size())
				//surfaceStart--;
			
			//surfaceCount = (short)surfaces.size();
			
			int surfaceStart2 = 0;//hullSurfaceIndices.size();
			//hullSurfaceIndices.add(surfaceStart);
			
			int planeStart = 0;
			this.convexHulls[0] = new ConvexHull(0, 0, 0, 0, 0, 0, 0, surfaceStart2, planeStart, (short)0, surfaceCount, 0, 0, 0, (short)0, false);
		//}
		
		this.surfaces = new Surface[surfaces.size()];
		for (int i = 0; i < surfaces.size(); i++)
		{
			this.surfaces[i] = surfaces.get(i);
		}
		this.hullSurfaceIndices = new int[hullSurfaceIndices.size()];
		for (int i = 0; i < hullSurfaceIndices.size(); i++)
		{
			this.hullSurfaceIndices[i] = hullSurfaceIndices.get(i);
		}
		
		this.windings = new int[windings.size()];
		for (int i = 0; i < windings.size(); i++)
		{
			this.windings[i] = windings.get(i);
		}
		this.texGenEQs.add(new TexGenPlanes());
		this.planes = new PlaneF[1];
		this.planes[0] = new PlaneF(0.0f, 0.0f, 1.0f, 0.0f);
		this.planeNormals = new Point3F[1];
		this.planeNormals[0] = new Point3F();
		this.processData();
		return true;
	}
	
	public boolean verify(int version)
	{
		this.errors.clear();
		if (version == 4 || version == 5)
		{
			if (!Util.hasElements(this.normalsLegacy))
			{
				this.errors.add("No Legacy Normals!");
				return false;
			}
		}
		return true;
	}
	
	public int getDetailLevel()
	{
		return detailLevel;
	}
	
	public void setDetailLevel(int detailLevel)
	{
		this.detailLevel = detailLevel;
	}
	
	public int getMinPixels()
	{
		return minPixels;
	}
	
	public void setMinPixels(int minPixels)
	{
		this.minPixels = minPixels;
	}
	
	public Box3F getBoundingBox()
	{
		return boundingBox;
	}
	
	public void setBoundingBox(Box3F boundingBox)
	{
		this.boundingBox = boundingBox;
	}
	
	public SphereF getBoundingSphere()
	{
		return boundingSphere;
	}
	
	public void setBoundingSphere(SphereF boundingSphere)
	{
		this.boundingSphere = boundingSphere;
	}
	
	public boolean isHasAlarmState()
	{
		return hasAlarmState;
	}
	
	public void setHasAlarmState(boolean hasAlarmState)
	{
		this.hasAlarmState = hasAlarmState;
	}
	
	public int getNumLightStateEntries()
	{
		return numLightStateEntries;
	}
	
	public void setNumLightStateEntries(int numLightStateEntries)
	{
		this.numLightStateEntries = numLightStateEntries;
	}
	
	public PlaneF[] getPlanes()
	{
		return planes;
	}
	
	public void setPlanes(PlaneF[] planes)
	{
		this.planes = planes;
	}
	
	public void setPlanes(List<PlaneF> planes)
	{
		this.planes = new PlaneF[planes.size()];
		for (int i = 0; i < planes.size(); i++)
		{
			this.planes[i] = planes.get(i);
		}
	}
	
	public ItrPaddedPoint[] getPoints()
	{
		return points;
	}
	
	public void setPoints(ItrPaddedPoint[] points)
	{
		this.points = points;
	}
	
	public byte[] getPointVisibility()
	{
		return pointVisibility;
	}
	
	public void setPointVisibility(byte[] pointVisibility)
	{
		this.pointVisibility = pointVisibility;
	}
	
	public TexGenList getTexGenEQs()
	{
		return texGenEQs;
	}
	
	public void setTexGenEQs(TexGenList texGenEQs)
	{
		this.texGenEQs = texGenEQs;
	}
	
	public BSPNode[] getBspNodes()
	{
		return bspNodes;
	}
	
	public void setBspNodes(BSPNode[] bspNodes)
	{
		this.bspNodes = bspNodes;
	}
	
	public BSPSolidLeaf[] getBspSolidLeaves()
	{
		return bspSolidLeaves;
	}
	
	public void setBspSolidLeaves(BSPSolidLeaf[] bspSolidLeaves)
	{
		this.bspSolidLeaves = bspSolidLeaves;
	}
	
	public int[] getWindings()
	{
		return windings;
	}
	
	public void setWindings(int[] windings)
	{
		this.windings = windings;
	}
	
	public TriFan[] getWindingIndices()
	{
		return windingIndices;
	}
	
	public void setWindingIndices(TriFan[] windingIndices)
	{
		this.windingIndices = windingIndices;
	}
	
	public Zone[] getZones()
	{
		return zones;
	}
	
	public void setZones(Zone[] zones)
	{
		this.zones = zones;
	}
	
	public short[] getZoneSurfaces()
	{
		return zoneSurfaces;
	}
	
	public void setZoneSurfaces(short[] zoneSurfaces)
	{
		this.zoneSurfaces = zoneSurfaces;
	}
	
	public int[] getZoneStaticMeshes()
	{
		return zoneStaticMeshes;
	}
	
	public void setZoneStaticMeshes(int[] zoneStaticMeshes)
	{
		this.zoneStaticMeshes = zoneStaticMeshes;
	}
	
	public short[] getZonePortalList()
	{
		return zonePortalList;
	}
	
	public void setZonePortalList(short[] zonePortalList)
	{
		this.zonePortalList = zonePortalList;
	}
	
	public Portal[] getPortals()
	{
		return portals;
	}
	
	public void setPortals(Portal[] portals)
	{
		this.portals = portals;
	}
	
	public Surface[] getSurfaces()
	{
		return surfaces;
	}
	
	public void setSurfaces(Surface[] surfaces)
	{
		this.surfaces = surfaces;
	}
	
	public void setSurfaces(List<Surface> surfaces)
	{
		this.surfaces = new Surface[surfaces.size()];
		
		for (int i = 0; i < surfaces.size(); i++)
			this.surfaces[i] = surfaces.get(i);
	}
	
	public TexGenPlanes[] getLmTexGenEQs()
	{
		return lmTexGenEQs;
	}
	
	public void setLmTexGenEQs(TexGenPlanes[] lmTexGenEQs)
	{
		this.lmTexGenEQs = lmTexGenEQs;
	}
	
	public int[] getNormalLMapIndices()
	{
		return normalLMapIndices;
	}
	
	public void setNormalLMapIndices(int[] normalLMapIndices)
	{
		this.normalLMapIndices = normalLMapIndices;
	}
	
	public int[] getAlarmLMapIndices()
	{
		return alarmLMapIndices;
	}
	
	public void setAlarmLMapIndices(int[] alarmLMapIndices)
	{
		this.alarmLMapIndices = alarmLMapIndices;
	}
	
	public NullSurface[] getNullSurfaces()
	{
		return nullSurfaces;
	}
	
	public void setNullSurfaces(NullSurface[] nullSurfaces)
	{
		this.nullSurfaces = nullSurfaces;
	}
	
	public BufferedImage[] getLightMaps()
	{
		return lightMaps;
	}
	
	public void setLightMaps(BufferedImage[] lightMaps)
	{
		this.lightMaps = lightMaps;
	}
	
	public BufferedImage[] getLightDirMaps()
	{
		return lightDirMaps;
	}
	
	public void setLightDirMaps(BufferedImage[] lightDirMaps)
	{
		this.lightDirMaps = lightDirMaps;
	}
	
	public boolean[] getLightMapKeep()
	{
		return lightMapKeep;
	}
	
	public void setLightMapKeep(boolean[] lightMapKeep)
	{
		this.lightMapKeep = lightMapKeep;
	}
	
	public int[] getSolidLeafSurfaces()
	{
		return solidLeafSurfaces;
	}
	
	public void setSolidLeafSurfaces(int[] solidLeafSurfaces)
	{
		this.solidLeafSurfaces = solidLeafSurfaces;
	}
	
	public int getNumTriggerableLights()
	{
		return numTriggerableLights;
	}
	
	public void setNumTriggerableLights(int numTriggerableLights)
	{
		this.numTriggerableLights = numTriggerableLights;
	}
	
	public AnimatedLight[] getAnimatedLights()
	{
		return animatedLights;
	}
	
	public void setAnimatedLights(AnimatedLight[] animatedLights)
	{
		this.animatedLights = animatedLights;
	}
	
	public LightState[] getLightStates()
	{
		return lightStates;
	}
	
	public void setLightStates(LightState[] lightStates)
	{
		this.lightStates = lightStates;
	}
	
	public LightStateData[] getLightStateData()
	{
		return lightStateData;
	}
	
	public void setLightStateData(LightStateData[] lightStateData)
	{
		this.lightStateData = lightStateData;
	}
	
	public byte[] getStateDataBuffer()
	{
		return stateDataBuffer;
	}
	
	public void setStateDataBuffer(byte[] stateDataBuffer)
	{
		this.stateDataBuffer = stateDataBuffer;
	}
	
	public char[] getNameBuffer()
	{
		return nameBuffer;
	}
	
	public void setNameBuffer(char[] nameBuffer)
	{
		this.nameBuffer = nameBuffer;
	}
	
	public InteriorSubObject[] getSubObjects()
	{
		return subObjects;
	}
	
	public void setSubObjects(InteriorSubObject[] subObjects)
	{
		this.subObjects = subObjects;
	}
	
	public ConvexHull[] getConvexHulls()
	{
		return convexHulls;
	}
	
	public void setConvexHulls(ConvexHull[] convexHulls)
	{
		this.convexHulls = convexHulls;
	}
	
	public byte[] getConvexHullEmitStrings()
	{
		return convexHullEmitStrings;
	}
	
	public void setConvexHullEmitStrings(byte[] convexHullEmitStrings)
	{
		this.convexHullEmitStrings = convexHullEmitStrings;
	}
	
	public int[] getHullIndices()
	{
		return hullIndices;
	}
	
	public void setHullIndices(int[] hullIndices)
	{
		this.hullIndices = hullIndices;
	}
	
	public short[] getHullPlaneIndices()
	{
		return hullPlaneIndices;
	}
	
	public void setHullPlaneIndices(short[] hullPlaneIndices)
	{
		this.hullPlaneIndices = hullPlaneIndices;
	}
	
	public int[] getHullEmitStringIndices()
	{
		return hullEmitStringIndices;
	}
	
	public void setHullEmitStringIndices(int[] hullEmitStringIndices)
	{
		this.hullEmitStringIndices = hullEmitStringIndices;
	}
	
	public int[] getHullSurfaceIndices()
	{
		return hullSurfaceIndices;
	}
	
	public void setHullSurfaceIndices(int[] hullSurfaceIndices)
	{
		this.hullSurfaceIndices = hullSurfaceIndices;
	}
	
	public short[] getPolyListPlanes()
	{
		return polyListPlanes;
	}
	
	public void setPolyListPlanes(short[] polyListPlanes)
	{
		this.polyListPlanes = polyListPlanes;
	}
	
	public int[] getPolyListPoints()
	{
		return polyListPoints;
	}
	
	public void setPolyListPoints(int[] polyListPoints)
	{
		this.polyListPoints = polyListPoints;
	}
	
	public byte[] getPolyListStrings()
	{
		return polyListStrings;
	}
	
	public void setPolyListStrings(byte[] polyListStrings)
	{
		this.polyListStrings = polyListStrings;
	}
	
	public CoordBin[] getCoordBins()
	{
		return coordBins;
	}
	
	public void setCoordBins(CoordBin[] coordBins)
	{
		this.coordBins = coordBins;
	}
	
	public short[] getCoordBinIndices()
	{
		return coordBinIndices;
	}
	
	public void setCoordBinIndices(short[] coordBinIndices)
	{
		this.coordBinIndices = coordBinIndices;
	}
	
	public int getCoordBinMode()
	{
		return coordBinMode;
	}
	
	public void setCoordBinMode(int coordBinMode)
	{
		this.coordBinMode = coordBinMode;
	}
	
	public ColorI getBaseAmbient()
	{
		return baseAmbient;
	}
	
	public void setBaseAmbient(ColorI baseAmbient)
	{
		this.baseAmbient = baseAmbient;
	}
	
	public ColorI getAlarmAmbient()
	{
		return alarmAmbient;
	}
	
	public void setAlarmAmbient(ColorI alarmAmbient)
	{
		this.alarmAmbient = alarmAmbient;
	}
	
	public InteriorSimpleMesh[] getStaticMeshes()
	{
		return staticMeshes;
	}
	
	public void setStaticMeshes(InteriorSimpleMesh[] staticMeshes)
	{
		this.staticMeshes = staticMeshes;
	}
	
	public Point3F[] getNormals()
	{
		return normals;
	}
	
	public void setNormals(Point3F[] normals)
	{
		this.normals = normals;
	}
	
	public Point3F[] getNormalsLegacy()
	{
		return normalsLegacy;
	}
	
	public void setNormalsLegacy(Point3F[] normalsLegacy)
	{
		this.normalsLegacy = normalsLegacy;
	}
	
	public TexMatrix[] getTexMatrices()
	{
		return texMatrices;
	}
	
	public void setTexMatrices(TexMatrix[] texMatrices)
	{
		this.texMatrices = texMatrices;
	}
	
	public int[] getTexMatIndices()
	{
		return texMatIndices;
	}
	
	public void setTexMatIndices(int[] texMatIndices)
	{
		this.texMatIndices = texMatIndices;
	}
	
	public int getLightMapBorderSize()
	{
		return lightMapBorderSize;
	}
	
	public void setLightMapBorderSize(int lightMapBorderSize)
	{
		this.lightMapBorderSize = lightMapBorderSize;
	}
	
	public ConvexHull[] getVehicleConvexHulls()
	{
		return vehicleConvexHulls;
	}
	
	public void setVehicleConvexHulls(ConvexHull[] vehicleConvexHulls)
	{
		this.vehicleConvexHulls = vehicleConvexHulls;
	}
	
	public byte[] getVehicleConvexHullEmitStrings()
	{
		return vehicleConvexHullEmitStrings;
	}
	
	public void setVehicleConvexHullEmitStrings(byte[] vehicleConvexHullEmitStrings)
	{
		this.vehicleConvexHullEmitStrings = vehicleConvexHullEmitStrings;
	}
	
	public int[] getVehicleHullIndices()
	{
		return vehicleHullIndices;
	}
	
	public void setVehicleHullIndices(int[] vehicleHullIndices)
	{
		this.vehicleHullIndices = vehicleHullIndices;
	}
	
	public short[] getVehicleHullPlaneIndices()
	{
		return vehicleHullPlaneIndices;
	}
	
	public void setVehicleHullPlaneIndices(short[] vehicleHullPlaneIndices)
	{
		this.vehicleHullPlaneIndices = vehicleHullPlaneIndices;
	}
	
	public int[] getVehicleHullEmitStringIndices()
	{
		return vehicleHullEmitStringIndices;
	}
	
	public void setVehicleHullEmitStringIndices(int[] vehicleHullEmitStringIndices)
	{
		this.vehicleHullEmitStringIndices = vehicleHullEmitStringIndices;
	}
	
	public int[] getVehicleHullSurfaceIndices()
	{
		return vehicleHullSurfaceIndices;
	}
	
	public void setVehicleHullSurfaceIndices(int[] vehicleHullSurfaceIndices)
	{
		this.vehicleHullSurfaceIndices = vehicleHullSurfaceIndices;
	}
	
	public short[] getVehiclePolyListPlanes()
	{
		return vehiclePolyListPlanes;
	}
	
	public void setVehiclePolyListPlanes(short[] vehiclePolyListPlanes)
	{
		this.vehiclePolyListPlanes = vehiclePolyListPlanes;
	}
	
	public int[] getVehiclePolyListPoints()
	{
		return vehiclePolyListPoints;
	}
	
	public void setVehiclePolyListPoints(int[] vehiclePolyListPoints)
	{
		this.vehiclePolyListPoints = vehiclePolyListPoints;
	}
	
	public byte[] getVehiclePolyListStrings()
	{
		return vehiclePolyListStrings;
	}
	
	public void setVehiclePolyListStrings(byte[] vehiclePolyListStrings)
	{
		this.vehiclePolyListStrings = vehiclePolyListStrings;
	}
	
	public NullSurface[] getVehicleNullSurfaces()
	{
		return vehicleNullSurfaces;
	}
	
	public void setVehicleNullSurfaces(NullSurface[] vehicleNullSurfaces)
	{
		this.vehicleNullSurfaces = vehicleNullSurfaces;
	}
	
	public ItrPaddedPoint[] getVehiclePoints()
	{
		return vehiclePoints;
	}
	
	public void setVehiclePoints(ItrPaddedPoint[] vehiclePoints)
	{
		this.vehiclePoints = vehiclePoints;
	}
	
	public PlaneF[] getVehiclePlanes()
	{
		return vehiclePlanes;
	}
	
	public void setVehiclePlanes(PlaneF[] vehiclePlanes)
	{
		this.vehiclePlanes = vehiclePlanes;
	}
	
	public int[] getVehicleWindings()
	{
		return vehicleWindings;
	}
	
	public void setVehicleWindings(int[] vehicleWindings)
	{
		this.vehicleWindings = vehicleWindings;
	}
	
	public TriFan[] getVehicleWindingIndices()
	{
		return vehicleWindingIndices;
	}
	
	public void setVehicleWindingIndices(TriFan[] vehicleWindingIndices)
	{
		this.vehicleWindingIndices = vehicleWindingIndices;
	}
	
	public List<Short> getNormalIndices()
	{
		return normalIndices;
	}
	
	public void setNormalIndices(List<Short> normalIndices)
	{
		this.normalIndices = normalIndices;
	}
	
	public int getFlags()
	{
		return flags;
	}
	
	public void setFlags(int flags)
	{
		this.flags = flags;
	}
	
	public void setMaterialList(MaterialList materialList)
	{
		this.materialList = materialList;
	}
	
	public void setEdges(Edge[] edges)
	{
		this.edges = edges;
	}
	
	public void setFileVersion(int fileVersion)
	{
		this.fileVersion = fileVersion;
	}
	
	public Point3F[] getPlaneNormals()
	{
		return planeNormals;
	}
	
	public void setPlaneNormals(Point3F[] planeNormals)
	{
		this.planeNormals = planeNormals;
	}

	public ErrorList getErrors()
	{
		return errors;
	}
}