package com.matt.difinspector.interior;

import java.io.BufferedWriter;
import java.io.IOException;

import com.matt.difinspector.io.ReverseDataInputStream;
import com.matt.difinspector.io.ReverseDataOutputStream;
import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.math.QuatF;
import com.matt.difinspector.structures.WayPoint;

public class InteriorPathFollower
{
	private String name;
	private String dataBlock;
	private int interiorResIndex;
	private int pathIndex;
	private Point3F offset;
	private int[] triggerIds;
	private WayPoint[] wayPoints;
	private int totalMS;
	private InteriorDictionary dictionary;
	
	public InteriorPathFollower()
	{
		this.name = "";
		this.pathIndex = 0;
		this.offset = new Point3F(0, 0, 0);
	}
	
	public boolean read(ReverseDataInputStream dis) throws IOException
	{
		this.name = dis.readString();
		this.dataBlock = dis.readString();
		this.interiorResIndex = dis.readInt();
		this.offset = dis.readPoint3F();
		
		this.dictionary = new InteriorDictionary();
		this.dictionary.read(dis);
		
		int numTriggers = dis.readInt();
		this.triggerIds = new int[numTriggers];
		for (int i = 0; i < numTriggers; i++)
		{
			this.triggerIds[i] = dis.readInt();
		}
		
		int numWayPoints = dis.readInt();
		this.wayPoints = new WayPoint[numWayPoints];
		for (int i = 0; i < numWayPoints; i++)
		{
			Point3F pos = dis.readPoint3F();
			QuatF rot = dis.readQuatF();
			int msToNext = dis.readInt();
			int smoothingType = dis.readInt();
			this.wayPoints[i] = new WayPoint(pos, rot, msToNext, smoothingType);
		}
		
		this.totalMS = dis.readInt();
		
		return true;
	}
	
	public boolean write(ReverseDataOutputStream dos) throws IOException
	{
		dos.writeString(this.name);
		dos.writeString(this.dataBlock);
		dos.writeInt(this.interiorResIndex);
		dos.writePoint3F(this.offset);
		
		this.dictionary.write(dos);
		
		dos.writeInt(this.triggerIds.length);
		for (int i = 0; i < this.triggerIds.length; i++)
		{
			dos.writeInt(this.triggerIds[i]);
		}
		
		dos.writeInt(this.wayPoints.length);
		for (int i = 0; i < this.wayPoints.length; i++)
		{
			dos.writePoint3F(this.wayPoints[i].getPos());
			dos.writeQuatF(this.wayPoints[i].getRot());
			dos.writeInt(this.wayPoints[i].getMsToNext());
			dos.writeInt(this.wayPoints[i].getSmoothingType());
		}
		
		dos.writeInt(this.totalMS);
		
		return true;
	}
	
	public void dumpInfo(BufferedWriter bw) throws IOException
	{
		bw.write("<InteriorPathFollower>");
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public String getDataBlock()
	{
		return this.dataBlock;
	}
	
	public int getInteriorResIndex()
	{
		return this.interiorResIndex;
	}
	
	public int getPathIndex()
	{
		return this.pathIndex;
	}
	
	public Point3F getOffset()
	{
		return this.offset;
	}
	
	public int[] getTriggerIds()
	{
		return this.triggerIds;
	}
	
	public WayPoint[] getWayPoints()
	{
		return this.wayPoints;
	}
	
	public int getTotalMS()
	{
		return this.totalMS;
	}
	
	public InteriorDictionary getDictionary()
	{
		return this.dictionary;
	}
}
