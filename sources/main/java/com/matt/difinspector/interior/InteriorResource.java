package com.matt.difinspector.interior;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import com.matt.difinspector.io.ReverseDataInputStream;
import com.matt.difinspector.io.ReverseDataOutputStream;
import com.matt.difinspector.main.DifInspector;
import com.matt.difinspector.math.Point2F;
import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.mesh.InteriorBuilder;
import com.matt.difinspector.models.OBJModel;
import com.matt.difinspector.models.OBJModel.OBJIndex;
import com.matt.jmatt.lists.ErrorList;

public class InteriorResource
{
	private boolean						hasVehicleCollisions;
	private BufferedImage				previewImage;
	private List<Interior>				detailLevels;
	private List<Interior>				subObjects;
	private List<InteriorResTrigger>	triggers;
	private List<InteriorPathFollower>	interiorPathFollowers;
	private List<ForceField>			forceFields;
	private List<AISpecialNode>			aiSpecialNodes;
	private List<ItrGameEntity>			gameEntities;
	private ErrorList					errors;
										
	public InteriorResource()
	{
		this.detailLevels = new ArrayList<Interior>();
		this.subObjects = new ArrayList<Interior>();
		this.triggers = new ArrayList<InteriorResTrigger>();
		this.interiorPathFollowers = new ArrayList<InteriorPathFollower>();
		this.forceFields = new ArrayList<ForceField>();
		this.aiSpecialNodes = new ArrayList<AISpecialNode>();
		this.gameEntities = new ArrayList<ItrGameEntity>();
		this.errors = new ErrorList();
	}
	
	public boolean read(File file)
	{
		if (!file.exists())
		{
			System.err.println("File '" + file.toString() + "' does not exist!");
			return false;
		}
		
		try
		{
			FileInputStream is = new FileInputStream(file);
			ReverseDataInputStream dis = new ReverseDataInputStream(new BufferedInputStream(is));
			
			int fileVersion = dis.readInt();
			if (fileVersion != 44)
			{
				System.err.println("Incompatible DIF File: " + fileVersion);
				dis.close();
				return false;
			}
			
			boolean hasPreview = dis.readBoolean();
			
			if (hasPreview)
				this.previewImage = dis.readImage();
				
			int numDetailLevels = dis.readInt();
			for (int i = 0; i < numDetailLevels; i++)
			{
				Interior detail = new Interior();
				if (!detail.read(dis))
				{
					System.err.println("Unable to read detail level " + i + " in interior resource");
					return false;
				}
				this.detailLevels.add(detail);
			}
			
			int numSubObjects = dis.readInt();
			for (int i = 0; i < numSubObjects; i++)
			{
				Interior subObject = new Interior();
				if (!subObject.read(dis))
				{
					System.err.println("Unable to read subobject " + i + " in interior resource");
					return false;
				}
				this.subObjects.add(subObject);
			}
			
			int numTriggers = dis.readInt();
			for (int i = 0; i < numTriggers; i++)
			{
				InteriorResTrigger trigger = new InteriorResTrigger();
				if (!trigger.read(dis))
				{
					System.err.println("Unable to read trigger " + i + " in interior resource");
					return false;
				}
				this.triggers.add(trigger);
			}
			
			int numChildren = dis.readInt();
			for (int i = 0; i < numChildren; i++)
			{
				InteriorPathFollower follower = new InteriorPathFollower();
				if (!follower.read(dis))
				{
					System.err.println("Unable to read child " + i + " in interior resource");
					return false;
				}
				this.interiorPathFollowers.add(follower);
			}
			
			int numFields = dis.readInt();
			for (int i = 0; i < numFields; i++)
			{
				ForceField field = new ForceField();
				if (!field.read(dis))
				{
					System.err.println("Unable to read field " + i + "in interior resource");
					return false;
				}
				this.forceFields.add(field);
			}
			
			int numSpecNodes = dis.readInt();
			for (int i = 0; i < numSpecNodes; i++)
			{
				AISpecialNode node = new AISpecialNode();
				if (!node.read(dis))
				{
					System.err.println("Unable to read SpecNode " + i + " in interior resource");
					return false;
				}
				this.aiSpecialNodes.add(node);
			}
			
			int dummyInt = dis.readInt();
			if (dummyInt == 1)
			{
				this.hasVehicleCollisions = true;
				if (this.detailLevels.size() != 0)
				{
					this.detailLevels.get(0).readVehicleCollision(dis);
				}
			}
			
			dummyInt = dis.readInt();
			if (dummyInt == 2)
			{
				// this.hasGameEntities = true;
				int numGameEntities = dis.readInt();
				for (int i = 0; i < numGameEntities; i++)
				{
					ItrGameEntity gameEntity = new ItrGameEntity();
					if (!gameEntity.read(dis))
					{
						System.err.println("Unable to read GameEntity " + i + " in interior resource");
						return false;
					}
					this.gameEntities.add(gameEntity);
				}
				dummyInt = dis.readInt();
				// this.dummy = dummyInt;
			}
			
			dis.close();
			
			return true;
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean write(File file, int targetVersion)
	{
		FileOutputStream os;
		try
		{
			os = new FileOutputStream(file);
			ReverseDataOutputStream dos = new ReverseDataOutputStream(os);
			
			final int VERSION = 44;
			dos.writeInt(VERSION);
			
			if (this.previewImage != null)
			{
				dos.writeBoolean(true);
				dos.writeImage(this.previewImage);
			} else
			{
				dos.writeBoolean(false);
			}
			
			dos.writeInt(this.detailLevels.size());
			for (int i = 0; i < this.detailLevels.size(); i++)
			{
				if (!this.detailLevels.get(i).write(dos, targetVersion))
				{
					return false;
				}
			}
			
			dos.writeInt(this.subObjects.size());
			for (int i = 0; i < this.subObjects.size(); i++)
			{
				if (!this.subObjects.get(i).write(dos, targetVersion))
				{
					return false;
				}
			}
			
			dos.writeInt(0);
			/*
			 * dos.writeInt(this.triggers.size()); for (int i = 0; i < this.triggers.size(); i++) {
			 * this.triggers.get(i).write(dos); }
			 */
			
			dos.writeInt(this.interiorPathFollowers.size());
			for (int i = 0; i < this.interiorPathFollowers.size(); i++)
			{
				this.interiorPathFollowers.get(i).write(dos);
			}
			
			dos.writeInt(0);
			/*
			 * dos.writeInt(this.forceFields.size()); for (int i = 0; i < this.forceFields.size(); i++) {
			 * this.forceFields.get(i).write(dos); }
			 */
			
			dos.writeInt(0);
			/*
			 * dos.writeInt(this.aiSpecialNodes.size()); for (int i = 0; i < this.aiSpecialNodes.size(); i++) {
			 * this.aiSpecialNodes.get(i).write(dos); }
			 */
			
			dos.writeInt(1);
			if (this.detailLevels.size() != 0)
				this.detailLevels.get(0).writeVehicleCollision(dos, targetVersion);
				
			if (targetVersion == -1)
			{
				dos.writeInt(0);
				dos.writeInt(0);
				dos.writeInt(0);
				dos.writeInt(0);
			}
			
			
			if (this.gameEntities.size() > 0)
			{
				dos.writeInt(2);
				dos.writeInt(this.gameEntities.size());
				for (int i = 0; i < this.gameEntities.size(); i++)
				{
					this.gameEntities.get(i).write(dos);
				}
				dos.writeInt(0);
			} else {
				dos.writeInt(0);
			}
			
			if (targetVersion == -1)
				dos.writeInt(0);
				
			dos.flush();
			dos.close();
			
			return true;
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		
		return false;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof InteriorResource))
			return false;
			
		InteriorResource resource = (InteriorResource) o;
		
		if (this.getDetailLevels().size() != resource.getDetailLevels().size())
			return false;
			
		for (int i = 0; i < this.getDetailLevels().size(); i++)
		{
			if (!this.getDetailLevels().get(i).equals(resource.getDetailLevels().get(i)))
				return false;
		}
		
		return true;
	}
	
	public boolean dumpInfo(File txtFile)
	{
		try
		{
			FileOutputStream fos = new FileOutputStream(txtFile);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			
			//final String TAB = "    ";
			
			bw.write("fileVersion = " + 44 + "\n\n");
			bw.write("numDetailLevels = " + this.detailLevels.size() + "\n");
			for (int i = 0; i < this.detailLevels.size(); i++)
			{
				bw.write("new DetailLevel(" + i + ") {\n");
				this.detailLevels.get(i).dumpInfo(bw);
				bw.write("}\n\n");
			}
			
			bw.write("numSubObjects = " + this.subObjects.size() + "\n");
			for (int i = 0; i < this.subObjects.size(); i++)
			{
				bw.write("new SubObject(" + i + ") {\n");
				this.subObjects.get(i).dumpInfo(bw);
				bw.write("}\n\n");
			}
			
			bw.write("numTriggers = " + this.triggers.size() + "\n");
			for (int i = 0; i < this.triggers.size(); i++)
			{
				bw.write("new Trigger(" + i + ") {\n");
				this.triggers.get(i).dumpInfo(bw);
				bw.write("}\n\n");
			}
			
			bw.write("numInteriorPathFollowers = " + this.interiorPathFollowers.size() + "\n");
			for (int i = 0; i < this.interiorPathFollowers.size(); i++)
			{
				bw.write("new InteriorPathFollower(" + i + ") {\n");
				this.interiorPathFollowers.get(i).dumpInfo(bw);
				bw.write("}\n\n");
			}
			
			bw.write("numForceFields = " + this.forceFields.size() + "\n");
			for (int i = 0; i < this.forceFields.size(); i++)
			{
				bw.write("new ForceField(" + i + ") {\n");
				this.forceFields.get(i).dumpInfo(bw);
				bw.write("}\n\n");
			}
			
			bw.write("numAISpecialNodes = " + this.aiSpecialNodes.size() + "\n");
			for (int i = 0; i < this.aiSpecialNodes.size(); i++)
			{
				bw.write("new AISpecialNode(" + i + ") {\n");
				this.aiSpecialNodes.get(i).dumpInfo(bw);
				bw.write("}\n\n");
			}
			
			bw.write("hasVehicleCollisions = " + (this.hasVehicleCollisions ? "true" : "false") + "\n");
			if (this.hasVehicleCollisions)
			{
				bw.write("new VehicleCollision() {\n");
				this.detailLevels.get(0).dumpVehicleCollisions(bw);
				bw.write("}\n\n");
			}
			
			bw.write("numGameEntities = " + this.gameEntities.size() + "\n");
			for (int i = 0; i < this.gameEntities.size(); i++)
			{
				bw.write("new GameEntity(" + i + ") {\n");
				this.gameEntities.get(i).dumpInfo(bw);
				bw.write("}\n\n");
			}
			
			bw.flush();
			bw.close();
		} catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean exportMap(File mapFile)
	{
		if (this.detailLevels.size() == 0)
			return false;
		try
		{
			FileOutputStream fos = new FileOutputStream(mapFile);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			
			this.detailLevels.get(0).exportMap(bw);
			
			bw.flush();
			bw.close();
		} catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean exportCsx(File csxFile)
	{
		if (this.detailLevels.size() == 0)
			return false;
		try
		{
			FileOutputStream fos = new FileOutputStream(csxFile);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			
			this.detailLevels.get(0).exportCsx(bw);
			
			bw.flush();
			bw.close();
		} catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean exportRaw(File rawFile)
	{
		if (this.detailLevels.size() == 0)
			return false;
		try
		{
			FileOutputStream fos = new FileOutputStream(rawFile);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			
			this.detailLevels.get(0).exportRaw(bw);
			
			bw.flush();
			bw.close();
		} catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean readCollada(File file)
	{
		/*if (!file.exists())
		{
			System.err.println("File '" + file.toString() + "' does not exist!");
			return false;
		}
		
		try
		{
			Collada dae = Collada.readFile(file.toURI().toURL());
			
			Interior interior = new Interior();
			if (interior.processCollada(dae))
			{
				this.detailLevels.add(interior);
			} else
			{
				return false;
			}
			
			return true;
		} catch (IOException e)
		{
			e.printStackTrace();
		} catch (SAXException e)
		{
			e.printStackTrace();
		}*/
		return false;
	}
	
	public boolean readOBJ(File file, float scale)
	{
		if (!file.exists())
		{
			System.err.println("File '" + file.toString() + "' does not exist!");
			return false;
		}
		
		try
		{
			DifInspector inspector = DifInspector.getInstance();
			inspector.displayProgress(true);
			inspector.displayProgress("Importing OBJ File", 0);
			
			System.out.println("Importing OBJ File");
			OBJModel model = new OBJModel(file, scale);
			
			System.out.println("Converting To Interior");
			InteriorBuilder ib = new InteriorBuilder("tile_advanced");
			
			for (OBJIndex index : model.getIndices())
			{
				Point3F vertex = model.getPositions().get(index.getVertexIndex());
				Point2F texCoord = new Point2F(0, 0);
				if (index.getTexCoordIndex() > -1)
					texCoord = model.getTexCoords().get(index.getTexCoordIndex());
				String texture = model.getTextures().get(index.getTextureIndex());
				
				ib.addVertex(vertex, texCoord, texture);
			}

			System.out.println("Building Interior");
			Interior interior = ib.build();

			System.out.println("Adding Detail Level 0");
			//if (interior.processOBJ(model))
			if (interior != null)
			{
				this.detailLevels.add(interior);
			} else
			{
				return false;
			}
			
			return true;
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return false;
	}
	
	public void recalculateBounds()
	{
		for (Interior interior : this.detailLevels)
			interior.recalculateBounds();
		for (Interior interior : this.subObjects)
			interior.recalculateBounds();
	}
	
	public List<Interior> getDetailLevels()
	{
		return this.detailLevels;
	}
	
	public List<Interior> getSubObjects()
	{
		return this.subObjects;
	}
	
	public List<InteriorResTrigger> getTriggers()
	{
		return this.triggers;
	}
	
	public List<InteriorPathFollower> getInteriorPathFollowers()
	{
		return this.interiorPathFollowers;
	}
	
	public List<ForceField> getForceFields()
	{
		return this.forceFields;
	}
	
	public List<AISpecialNode> getAISpecialNodes()
	{
		return this.aiSpecialNodes;
	}
	
	public List<ItrGameEntity> getGameEntities()
	{
		return this.gameEntities;
	}
	
	public boolean getHasVehicleCollisions()
	{
		return this.hasVehicleCollisions;
	}

	public ErrorList getErrors()
	{
		ErrorList errorList = new ErrorList();
		errorList.append(this.errors);
		for (Interior interior : this.detailLevels)
		{
			errorList.append(interior.getErrors());
		}
		
		if (errorList.getCount() == 0)
		{
			errorList.add();
		}
		
		return errorList;
	}
	
}
