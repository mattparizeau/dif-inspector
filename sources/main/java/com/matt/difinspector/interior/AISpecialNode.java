package com.matt.difinspector.interior;

import java.io.BufferedWriter;
import java.io.IOException;

import com.matt.difinspector.io.ReverseDataInputStream;
import com.matt.difinspector.math.Point3F;

public class AISpecialNode
{
	private String name;
	private Point3F pos;
	
	public AISpecialNode()
	{
		this.name = "";
		this.pos = new Point3F(0, 0, 0);
	}
	
	public boolean read(ReverseDataInputStream dis) throws IOException
	{
		this.name = dis.readString();
		this.pos = dis.readPoint3F();
		
		return true;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public Point3F getPos()
	{
		return this.pos;
	}
	
	public void dumpInfo(BufferedWriter bw) throws IOException
	{
		bw.write("<AISpecialNode>");
	}
}
