package com.matt.difinspector.interior;

import java.io.BufferedWriter;
import java.io.IOException;

import com.matt.difinspector.io.ReverseDataInputStream;
import com.matt.difinspector.io.ReverseDataOutputStream;
import com.matt.difinspector.math.Point3F;

public class ItrGameEntity
{
	private String dataBlock;
	private String gameClass;
	private Point3F pos;
	private InteriorDictionary dictionary;
	
	public ItrGameEntity()
	{
		this.dataBlock = "";
		this.gameClass = "";
		this.pos = new Point3F(0, 0, 0);
	}
	
	public boolean read(ReverseDataInputStream dis) throws IOException
	{
		this.dataBlock = dis.readString();
		this.gameClass = dis.readString();
		
		this.pos = dis.readPoint3F();
		
		this.dictionary = new InteriorDictionary();
		this.dictionary.read(dis);
		
		return true;
	}
	
	public boolean write(ReverseDataOutputStream dos) throws IOException
	{
		dos.writeString(this.dataBlock);
		dos.writeString(this.gameClass);
		
		dos.writePoint3F(this.pos);
		
		this.dictionary.write(dos);
		
		return true;
	}
	
	public void dumpInfo(BufferedWriter bw) throws IOException
	{	
		final String TAB = "    ";
		
		bw.write(TAB + "dataBlock = " + this.dataBlock + "\n");
		bw.write(TAB + "gameClass = " + this.gameClass + "\n");
		bw.write(TAB + "pos = " + this.pos + "\n");
		
		bw.write(TAB + "new Dictionary() {\n");
		this.dictionary.dumpInfo(bw);
		bw.write(TAB + "}\n");
	}
}
