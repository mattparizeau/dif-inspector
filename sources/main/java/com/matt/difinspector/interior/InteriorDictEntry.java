package com.matt.difinspector.interior;

public class InteriorDictEntry
{
	private String name;
	private String value;
	
	public InteriorDictEntry(String name, String value)
	{
		this.name = name;
		this.value = value;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public String getValue()
	{
		return this.value;
	}
}
