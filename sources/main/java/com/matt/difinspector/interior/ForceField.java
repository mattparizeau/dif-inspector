package com.matt.difinspector.interior;

import java.io.BufferedWriter;
import java.io.IOException;

import com.matt.difinspector.io.ReverseDataInputStream;
import com.matt.difinspector.math.Box3F;
import com.matt.difinspector.math.PlaneF;
import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.math.SphereF;
import com.matt.difinspector.structures.BSPNode;
import com.matt.difinspector.structures.BSPSolidLeaf;
import com.matt.difinspector.structures.ColorF;
import com.matt.difinspector.structures.Surface;

public class ForceField
{
	private String name;
	private ColorF color;
	private String[] triggers;
	private Box3F boundingBox;
	private SphereF boundingSphere;
	private PlaneF[] planes;
	private Point3F[] points;
	private BSPNode[] bspNodes;
	private BSPSolidLeaf[] bspSolidLeaves;
	private int[] solidLeafSurfaces;
	private boolean preppedForRender;
	private int[] windings;
	private Surface[] surfaces;
	
	public ForceField()
	{
		
	}
	
	public boolean read(ReverseDataInputStream dis) throws IOException
	{
		// Version this stream
		int fileVersion = dis.readInt();
		if (fileVersion != 0)
		{
			System.err.println("incompatible forcefield version found.");
			return false;
		}
		
		this.name = dis.readString();
		
		int numTriggers = dis.readInt();
		this.triggers = new String[numTriggers];
		for (int i = 0; i < numTriggers; i++)
		{
			this.triggers[i] = dis.readString();
		}
		
		// Geometry factors...
		this.boundingBox = dis.readBox3F();
		this.boundingSphere = dis.readSphereF();
		
		// Planes
		this.readPlaneVector(dis);
		
		// Points
		int numPoints = dis.readInt();
		this.points = new Point3F[numPoints];
		for (int i = 0; i < numPoints; i++)
		{
			this.points[i] = dis.readPoint3F();
		}
		
		// BSPNodes
		int numBSPNodes = dis.readInt();
		this.bspNodes = new BSPNode[numBSPNodes];
		for (int i = 0; i < numBSPNodes; i++)
		{
			short planeIndex = dis.readShort();
			int frontIndex = dis.readInt();
			int backIndex = dis.readInt();
			
			this.bspNodes[i] = new BSPNode(planeIndex, frontIndex, backIndex);
		}
		
		// BSPSolidLeaves
		int numBSPSolidLeaves = dis.readInt();
		this.bspSolidLeaves = new BSPSolidLeaf[numBSPSolidLeaves];
		for (int i = 0; i < numBSPSolidLeaves; i++)
		{
			int surfaceIndex = dis.readInt();
			short surfaceCount = dis.readShort();
			
			this.bspSolidLeaves[i] = new BSPSolidLeaf(surfaceIndex, surfaceCount);
		}
		
		// Windings
		int numWindings = dis.readInt();
		this.windings = new int[numWindings];
		for (int i = 0; i < numWindings; i++)
		{
			this.windings[i] = dis.readInt();
		}
		
		// Surfaces
		int numSurfaces = dis.readInt();
		this.surfaces = new Surface[numSurfaces];
		for (int i = 0; i < numSurfaces; i++)
		{
			int windingStart = dis.readInt();
			byte windingCount = dis.readByte();
			short planeIndex = dis.readShort();
			byte surfaceFlags = dis.readByte();
			int fanMask = dis.readInt();
			
			this.surfaces[i] = new Surface(windingStart, fanMask, planeIndex, windingCount, surfaceFlags, false);
		}
		
		// SolidLeafSurfaces
		int numSolidLeafSurfaces = dis.readInt();
		this.solidLeafSurfaces = new int[numSolidLeafSurfaces];
		for (int i = 0; i < numSolidLeafSurfaces; i++)
		{
			this.solidLeafSurfaces[i] = dis.readInt();
		}
		
		this.color = dis.readColorF();
		
		return false;
	}
	
	public void readPlaneVector(ReverseDataInputStream dis) throws IOException
	{
		int numNormals = dis.readInt();
		Point3F[] normals = new Point3F[numNormals];
		for (int i = 0; i < numNormals; i++)
		{
			normals[i] = dis.readPoint3F();
		}
		
		int numPlanes = dis.readInt();
		this.planes = new PlaneF[numPlanes];
		for (int i = 0; i < numPlanes; i++)
		{
			short index = dis.readShort();
			float d = dis.readFloat();
			float x = normals[index].getX();
			float y = normals[index].getY();
			float z = normals[index].getZ();
			this.planes[i] = new PlaneF(x, y, z, d);
		}
	}
	
	public void dumpInfo(BufferedWriter bw) throws IOException
	{
		bw.write("<ForceField>");
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public ColorF getColor()
	{
		return this.color;
	}
	
	public String[] getTriggers()
	{
		return this.triggers;
	}
	
	public Box3F getBoundingBox()
	{
		return this.boundingBox;
	}
	
	public SphereF getBoundingSphere()
	{
		return this.boundingSphere;
	}
	
	public PlaneF[] getPlanes()
	{
		return this.planes;
	}
	
	public Point3F[] getPoints()
	{
		return this.points;
	}
	
	public BSPNode[] getBSPNodes()
	{
		return this.bspNodes;
	}
	
	public BSPSolidLeaf[] getBSPSolidLeaves()
	{
		return this.bspSolidLeaves;
	}
	
	public int[] getSolidLeafSurfaces()
	{
		return this.solidLeafSurfaces;
	}
	
	public boolean getPreppedForRender()
	{
		return this.preppedForRender;
	}
	
	public int[] getWindings()
	{
		return this.windings;
	}
	
	public Surface[] getSurfaces()
	{
		return this.surfaces;
	}
}
