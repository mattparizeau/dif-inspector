package com.matt.difinspector.dts;

import com.matt.difinspector.util.Util;

public enum ShapeFlags
{
	UniformScale(Util.BIT(0)),
	AlignedScale(Util.BIT(1)),
	ArbitraryScale(Util.BIT(2)),
	Blend(Util.BIT(3)),
	Cyclic(Util.BIT(4)),
	MakePath(Util.BIT(5)),
	IflInit(Util.BIT(6)),
	HasTranslucency(Util.BIT(7)),
	AnyScale(UniformScale.getValue() | AlignedScale.getValue() | ArbitraryScale.getValue());
	
	protected int value;
	private ShapeFlags(int value)
	{
		this.value = value;
	}
	
	public int getValue()
	{
		return this.value;
	}
}
