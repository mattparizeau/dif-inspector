package com.matt.difinspector.io;

import java.io.IOException;
import java.io.InputStream;

public class ArrayInputStream<T> extends InputStream
{
	private T[] array;
	private int index;
	
	public ArrayInputStream(T[] array)
	{
		this.array = array;
		this.index = 0;
	}
	
	@Override
	public int read() throws IOException
	{
		int result = (int) this.array[this.index];
		this.index++;
		return result;
	}
	
}
