package com.matt.difinspector.io;

public class ArrayReader
{
	private int[] intArray;
	private short[] shortArray;
	private byte[] byteArray;
	
	private int index;
	
	public ArrayReader(int[] intArray, short[] shortArray, byte[] byteArray)
	{
		this.intArray = intArray;
		this.shortArray = shortArray;
		this.byteArray = byteArray;
	}
	
	public int readInt()
	{
		int result = this.intArray[index];
		index++;
		return result;
	}
	
	public short readShort()
	{
		short result = this.shortArray[index];
		index++;
		return result;
	}
	
	public byte readByte()
	{
		byte result = this.byteArray[index];
		index++;
		return result;
	}
}
