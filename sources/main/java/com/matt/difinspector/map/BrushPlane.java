package com.matt.difinspector.map;

import java.util.List;

import com.matt.difinspector.math.PlaneF;
import com.matt.difinspector.math.Point3F;

public class BrushPlane
{
	
	protected List<Integer> indices; 
	
	private PlaneF plane;
	private Point3F point1;
	private Point3F point2;
	private Point3F point3;
	
	private String texture;
	
	private PlaneF texPlane1;
	private PlaneF texPlane2;
	
	private float textureRotation;
	private float textureScaleX;
	private float textureScaleY;
	
	public BrushPlane(List<Integer> indices, Point3F point1, Point3F point2, Point3F point3, String texture, PlaneF texPlane1, PlaneF texPlane2, float textureRotation, float textureScaleX, float textureScaleY)
	{
		this.indices = indices;
		this.point1 = point1;
		this.point2 = point2;
		this.point3 = point3;
		
		this.plane = new PlaneF(this.point1, this.point2, this.point3);
		
		this.texture = texture;
		
		this.texPlane1 = texPlane1;
		this.texPlane2 = texPlane2;
		
		this.textureRotation = textureRotation;
		this.textureScaleX = textureScaleX;
		this.textureScaleY = textureScaleY;
	}
	
	public BrushPlane(List<Integer> indices, Point3F point1, Point3F point2, Point3F point3, PlaneF plane, String texture, PlaneF texPlane1, PlaneF texPlane2, float textureRotation, float textureScaleX, float textureScaleY)
	{
		this.indices = indices;
		this.point1 = point1;
		this.point2 = point2;
		this.point3 = point3;
		
		this.plane = plane;
		
		this.texture = texture;
		
		this.texPlane1 = texPlane1;
		this.texPlane2 = texPlane2;
		
		this.textureRotation = textureRotation;
		this.textureScaleX = textureScaleX;
		this.textureScaleY = textureScaleY;
	}
	
	public BrushPlane(List<Integer> indices, PlaneF plane, String texture, PlaneF texPlane1, PlaneF texPlane2, float textureRotation, float textureScaleX, float textureScaleY)
	{
		this.indices = indices;
		this.plane = plane;
		
		//float d = plane.getD() * 32;
		
		/*this.point1 = plane.mul(new Point3F(d)).add(d);
		this.point2 = plane.cross(this.point1).mul(-1);
		this.point3 = plane.cross(this.point2).mul(-1);
		
		this.point1 = this.point1.add(new Point3F(0, 0, d));
		this.point2 = this.point2.add(new Point3F(0, 0, d));
		this.point3 = this.point3.add(new Point3F(0, 0, d));*/
		
		/*this.point1 = this.point1.add(new Point3F(0, 0, d));
		this.point2 = this.point2.add(new Point3F(-this.point1.z, 0, -d));
		this.point3 = this.point3.add(new Point3F(-this.point1.z, 0, -d));
		this.point1 = this.point1.sub(new Point3F(0, 0, d * 2));*/
		
		//this.point1 = plane.mul(-d);
		//this.point2 = this.point1.swap(d);
		//this.point3 = new Point3F(d);
		
		//this.point1 = this.point1.fill(d);
		//this.point2 = this.point2.fill(d);
		//this.point3 = this.point3.fill(d);
		
		this.texture = texture;
		
		this.texPlane1 = texPlane1;
		this.texPlane2 = texPlane2;
		
		this.textureRotation = textureRotation;
		this.textureScaleX = textureScaleX;
		this.textureScaleY = textureScaleY;
	}
	
	public List<Integer> getIndices() {
		return this.indices;
	}
	
	public PlaneF getPlane()
	{
		return this.plane;
	}
	
	public Point3F getPoint1()
	{
		return this.point1;
	}
	
	public Point3F getPoint2()
	{
		return this.point2;
	}
	
	public Point3F getPoint3()
	{
		return this.point3;
	}
	
	public String getTexture()
	{
		return this.texture;
	}
	
	public PlaneF getTexturePlane1()
	{
		return this.texPlane1;
	}
	
	public PlaneF getTexturePlane2()
	{
		return this.texPlane2;
	}
	
	public float getTextureRotation()
	{
		return this.textureRotation;
	}
	
	public float getTextureScaleX()
	{
		return this.textureScaleX;
	}
	
	@Override
	public String toString()
	{
		return "( " + this.point1.getX() + " " + this.point1.getY() + " " + this.point1.getZ() + " ) ( " + this.point2.getX() + " " + this.point2.getY() + " " + this.point2.getZ() + " ) ( " + this.point3.getX() + " " + this.point3.getY() + " " + this.point3.getZ() + " ) " + this.texture + " [ " + this.texPlane1.getX() + " " + this.texPlane1.getY() + " " + this.texPlane1.getZ() + " " + this.texPlane1.getD() + " ] [ " + this.texPlane2.getX() + " " + this.texPlane2.getY() + " " + this.texPlane2.getZ() + " " + this.texPlane2.getD() + " ] " + this.textureRotation + " " + this.textureScaleX + " " + this.textureScaleY;
	}
	
	public float getTextureScaleY()
	{
		return this.textureScaleY;
	}
}
