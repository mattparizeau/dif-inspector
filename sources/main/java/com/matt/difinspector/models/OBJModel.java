package com.matt.difinspector.models;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.matt.difinspector.math.Point2F;
import com.matt.difinspector.math.Point3F;

public class OBJModel
{
	protected File file;
	protected List<Point3F> positions;
	protected List<Point2F> texCoords;
	protected List<Point3F> normals;
	protected List<String> textures;
	protected List<OBJIndex> indices;
	protected boolean hasTexCoords;
	protected boolean hasNormals;
	
	public OBJModel(File file, float scale) throws IOException
	{
		this.file = file;
		this.positions = new ArrayList<Point3F>();
		this.texCoords = new ArrayList<Point2F>();
		this.normals = new ArrayList<Point3F>();
		this.indices = new ArrayList<OBJIndex>();
		this.textures = new ArrayList<String>();
		this.hasTexCoords = false;
		this.hasNormals = false;
		
		String currentTexture = "";
		
		BufferedReader meshReader = new BufferedReader(new FileReader(file));
		String line;
		
		while((line = meshReader.readLine()) != null)
		{
			String[] tokens = line.trim().split(" +");
			//System.out.println(Util.getArrayString(tokens));
			
			if (tokens.length == 0 || tokens[0].equals("#"))
				continue;
			else if (tokens[0].equals("v"))
			{
				this.positions.add(new Point3F(Float.valueOf(tokens[1]), Float.valueOf(tokens[2]), Float.valueOf(tokens[3])));
			} else if (tokens[0].equals("vt"))
			{
				float token1;
				float token2;
				if (tokens[1].equalsIgnoreCase("nan") || tokens[1].isEmpty())
					token1 = Float.NaN;
				else
					token1 = Float.valueOf(tokens[1]);
				if (tokens[2].equalsIgnoreCase("nan") || tokens[2].isEmpty())
					token2 = Float.NaN;
				else
					token2 = Float.valueOf(tokens[2]);
				this.texCoords.add(new Point2F(token1, 1.0f - token2));
			} else if (tokens[0].equals("vn"))
			{
				this.normals.add(new Point3F(Float.valueOf(tokens[1]), Float.valueOf(tokens[2]), Float.valueOf(tokens[3])));
			} else if (tokens[0].equals("f"))
			{
				/*if (tokens.length > 4)
				{
					System.out.println("WARNING: Face Needs To Be Triangulated!");
				}*/
				
				for (int i = 0; i < tokens.length - 3; i++)
				{
					this.indices.add(parseOBJIndex(tokens[1], currentTexture));
					this.indices.add(parseOBJIndex(tokens[2 + i], currentTexture));
					this.indices.add(parseOBJIndex(tokens[3 + i], currentTexture));
				}
			} else if (tokens[0].equals("usemtl"))
			{
				currentTexture = tokens[1];
				if (!this.textures.contains(currentTexture))
					this.textures.add(currentTexture);
			}
		}
		
		meshReader.close();
		
		System.out.println("Reversing Vertex Order");
		List<OBJIndex> newIndices = new ArrayList<OBJIndex>();
		
		for (int i = this.indices.size() - 1; i >= 0; i--)
		{
			newIndices.add(this.indices.get(i));
		}
		
		this.indices = newIndices;
		
		for (int i = 0; i < this.positions.size(); i++)
		{
			Point3F pos = this.positions.get(i);
			
			pos = pos.mul(scale);
			//pos = pos.mul(0.01f); // Blender + 100 scale
			//pos = pos.mul(0.001f); // Sketchup
			//pos = pos.rotate(new Point3F(0, 1, 0), (float)Math.toRadians(90));
			pos = pos.rotate(new Point3F(0, 0, 1), (float)Math.toRadians(180));
			
			this.positions.set(i, pos);
		}
	}
	
	private OBJIndex parseOBJIndex(String token, String texture)
	{
		String[] values = token.split("/");
		
		OBJIndex result = new OBJIndex();
		if (texture.isEmpty())
		{
			texture = "NULL";
			this.textures.add(texture);
		}
		result.setTextureIndex(this.textures.indexOf(texture));
		result.setVertexIndex(Integer.parseInt(values[0]) - 1);
		
		if (values.length > 1)
		{
			if (!values[1].isEmpty())
			{
				this.hasTexCoords = true;
				result.setTexCoordIndex(Integer.parseInt(values[1]) - 1);
			}
			
			if (values.length > 2)
			{
				this.hasNormals = true;
				result.setNormalIndex(Integer.parseInt(values[2]) - 1);
			}
		}
		
		return result;
	}
	
	public static class OBJIndex
	{
		protected int vertexIndex;
		protected int texCoordIndex;
		protected int normalIndex;
		protected int textureIndex;
		
		public OBJIndex()
		{
			this.texCoordIndex = -1;
		}

		public int getVertexIndex()
		{
			return vertexIndex;
		}

		public void setVertexIndex(int vertexIndex)
		{
			this.vertexIndex = vertexIndex;
		}

		public int getTexCoordIndex()
		{
			return texCoordIndex;
		}

		public void setTexCoordIndex(int texCoordIndex)
		{
			this.texCoordIndex = texCoordIndex;
		}

		public int getNormalIndex()
		{
			return normalIndex;
		}

		public void setNormalIndex(int normalIndex)
		{
			this.normalIndex = normalIndex;
		}

		public int getTextureIndex()
		{
			return textureIndex;
		}

		public void setTextureIndex(int textureIndex)
		{
			this.textureIndex = textureIndex;
		}
	}

	public List<Point3F> getPositions()
	{
		return positions;
	}

	public void setPositions(List<Point3F> positions)
	{
		this.positions = positions;
	}

	public List<Point2F> getTexCoords()
	{
		return texCoords;
	}

	public void setTexCoords(List<Point2F> texCoords)
	{
		this.texCoords = texCoords;
	}

	public List<Point3F> getNormals()
	{
		return normals;
	}

	public void setNormals(List<Point3F> normals)
	{
		this.normals = normals;
	}

	public List<OBJIndex> getIndices()
	{
		return indices;
	}

	public void setIndices(List<OBJIndex> indices)
	{
		this.indices = indices;
	}

	public boolean hasTexCoords()
	{
		return hasTexCoords;
	}

	public void setHasTexCoords(boolean hasTexCoords)
	{
		this.hasTexCoords = hasTexCoords;
	}

	public boolean hasNormals()
	{
		return hasNormals;
	}

	public void setHasNormals(boolean hasNormals)
	{
		this.hasNormals = hasNormals;
	}

	public List<String> getTextures()
	{
		return textures;
	}

	public void setTextures(List<String> textures)
	{
		this.textures = textures;
	}
	
}

/*package com.matt.difinspector.models;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.matt.difinspector.structures.Point2F;
import com.matt.difinspector.structures.Point3F;
import com.matt.difinspector.structures.Point3I;

public class OBJModel
{
	protected File file;
	protected String materialFile;
	protected List<Point3F> vertices;
	protected List<Point2F> vertexTextures;
	protected List<OBJMesh> meshes;
	
	public OBJModel(File file) throws IOException
	{
		this.file = file;
		this.vertices = new ArrayList<Point3F>();
		this.vertexTextures = new ArrayList<Point2F>();
		this.meshes = new ArrayList<OBJMesh>();
		this.readOBJ();
	}
	
	private void readOBJ() throws IOException
	{
		FileInputStream fis = new FileInputStream(this.file);
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
		
		String line;
		
		String group = null;
		String usemtl = null;
		OBJMesh currentMesh = new OBJMesh("obj");
		this.meshes.add(currentMesh);
		
		while ((line = br.readLine()) != null)
		{
			// Remove Extra White Space
			line = line.trim();
			
			// Comments
			if (line.startsWith("#"))
				continue;
			
			line = line.replace("  ", " ");
			
			String[] ln = line.split(" ");
			if (ln.length <= 0)
				continue;
			
			if (ln[0].equals("mtllib") && ln.length >= 2)
			{
				this.materialFile = ln[1];
			}
			
			if(ln[0].equals("v") && ln.length >= 4)
			{
				float x = Float.parseFloat(ln[1]);
				float y = Float.parseFloat(ln[2]);
				float z = Float.parseFloat(ln[3]);
				
				Point3F v = new Point3F(x, y, z);
				
				this.vertices.add(v);
			}
			
			if(ln[0].equals("vt") && ln.length >= 3)
			{
				float x;
				if(ln[1].equalsIgnoreCase("nan"))
					x = Float.NaN;
				else
					x = Float.parseFloat(ln[1]);
				float y;
				if(ln[2].equalsIgnoreCase("nan"))
					y = Float.NaN;
				else
					y = Float.parseFloat(ln[2]);
				
				Point2F uv = new Point2F(x, y);
				
				this.vertexTextures.add(uv);
			}
			
			if(ln[0].equals("s") && ln.length >= 1)
			{
				System.out.println("Smoothing Group: " + ln[1]);
			}
			
			if(ln[0].equals("g") && ln.length >= 2)
			{
				group = ln[1];
				if (currentMesh.faces.size() > 0)
				{
					currentMesh = new OBJMesh(group);
					this.meshes.add(currentMesh);
				}
			}
			
			if (ln[0].equals("usemtl") && ln.length >= 2)
			{
				usemtl = ln[1];
				//System.out.println(usemtl);
			}
			
			if (ln[0].equals("f") && ln.length >= 3)
			{
				String[] xx = ln[1].split("/");
				String[] yy = ln[2].split("/");
				String[] zz = ln[3].split("/");
				
				int xIndex = Integer.parseInt(xx[0]);
				int yIndex = Integer.parseInt(yy[0]);
				int zIndex = Integer.parseInt(zz[0]);
				
				int textureXIndex = 0;
				int textureYIndex = 0;
				int textureZIndex = 0;
				
				if (xx.length > 1)
					textureXIndex = Integer.parseInt(xx[1]);
				
				if (yy.length > 1)
					textureYIndex = Integer.parseInt(yy[1]);
				
				if (zz.length > 1)
					textureZIndex = Integer.parseInt(zz[1]);
				
				Point3I vertIndex = new Point3I(xIndex, yIndex, zIndex);
				Point3I textureIndex = new Point3I(textureXIndex, textureYIndex, textureZIndex);
				OBJFace index = new OBJFace(vertIndex, textureIndex, usemtl);
				
				currentMesh.addFace(index);
			}
		}
		
		br.close();
	}
	
	public static class OBJMesh
	{
		protected List<OBJFace> faces;
		protected String name;
		
		public OBJMesh(String name)
		{
			this.name = name;
			this.faces = new ArrayList<OBJFace>();
		}
		
		public void addFace(OBJFace face)
		{
			this.faces.add(face);
		}
		
		public List<OBJFace> getFaces()
		{
			return this.faces;
		}
	}
	
	public static class OBJFace
	{
		protected Point3I vertexIndex;
		protected Point3I textureIndex;
		protected String material;
		
		public OBJFace(Point3I vertexIndex, Point3I textureIndex, String material)
		{
			this.vertexIndex = vertexIndex;
			this.textureIndex = textureIndex;
			this.material = material;
		}

		public Point3I getVertexIndex()
		{
			return vertexIndex;
		}

		public void setVertexIndex(Point3I vertexIndex)
		{
			this.vertexIndex = vertexIndex;
		}

		public Point3I getTextureIndex()
		{
			return textureIndex;
		}

		public void setTextureIndex(Point3I textureIndex)
		{
			this.textureIndex = textureIndex;
		}

		public String getMaterial()
		{
			return material;
		}

		public void setMaterial(String material)
		{
			this.material = material;
		}
	}

	public String getMaterialFile()
	{
		return materialFile;
	}

	public void setMaterialFile(String materialFile)
	{
		this.materialFile = materialFile;
	}

	public List<Point3F> getVertices()
	{
		return vertices;
	}

	public void setVertices(List<Point3F> vertices)
	{
		this.vertices = vertices;
	}

	public List<Point2F> getVertexTextures()
	{
		return vertexTextures;
	}

	public void setVertexTextures(List<Point2F> vertexTextures)
	{
		this.vertexTextures = vertexTextures;
	}

	public List<OBJMesh> getMeshes()
	{
		return meshes;
	}

	public void setMeshes(List<OBJMesh> meshes)
	{
		this.meshes = meshes;
	}
}*/
