package com.matt.difinspector.structures;

import java.io.IOException;

import com.matt.difinspector.io.ReverseDataInputStream;
import com.matt.difinspector.materials.MaterialList;

public class TSMaterialList extends MaterialList
{
	protected int fileVersion;
	protected int[] flags;
	protected int[] reflectanceMaps;
	protected int[] bumpMaps;
	protected int[] detailMaps;
	protected float[] detailScales;
	protected float[] reflectionAmounts;
	
	protected boolean namesTransformed;
	
	public TSMaterialList()
	{
		this(25);
	}
	
	public TSMaterialList(int fileVersion)
	{
		this.fileVersion = fileVersion;
	}
	
	@Override
	public boolean read(ReverseDataInputStream dis) throws IOException
	{
		if (!super.read(dis))
			return false;
		
		int numMaterials = this.materials.size();
		
		this.flags = new int[numMaterials];
		if (this.fileVersion < 2)
		{
			for (int i = 0; i < numMaterials; i++)
			{
				//this.flags[i] = S_Wrap | T_Wrap;
			}
		} else {
			for (int i = 0; i < numMaterials; i++)
			{
				this.flags[i] = dis.readInt();
			}
		}
		
		if (this.fileVersion < 5)
		{
			this.reflectanceMaps = new int[numMaterials];
			for (int i = 0; i < numMaterials; i++)
			{
				this.reflectanceMaps[i] = i;
			}
			
			this.bumpMaps = new int[numMaterials];
			for (int i = 0; i < numMaterials; i++)
			{
				this.bumpMaps[i] = 0xFFFFFFFF;
			}
			
			this.detailMaps = new int[numMaterials];
			for (int i = 0; i < numMaterials; i++)
			{
				this.detailMaps[i] = 0xFFFFFFFF;
			}
		} else {
			this.reflectanceMaps = new int[numMaterials];
			for (int i = 0; i < numMaterials; i++)
			{
				this.reflectanceMaps[i] = dis.readInt();
			}
			
			this.bumpMaps = new int[numMaterials];
			for (int i = 0; i < numMaterials; i++)
			{
				this.bumpMaps[i] = dis.readInt();
			}
			
			this.detailMaps = new int[numMaterials];
			for (int i = 0; i < numMaterials; i++)
			{
				this.detailMaps[i] = dis.readInt();
			}
		
			if (this.fileVersion == 25)
			{
				for (int i = 0; i < numMaterials; i++)
				{
					// Dummy
					dis.readInt();
				}
			}
		}
		
		this.detailScales = new float[numMaterials];
		if (this.fileVersion > 11)
		{
			for (int i = 0; i < numMaterials; i++)
			{
				this.detailScales[i] = dis.readFloat();
			}
		} else {
			for (int i = 0; i < numMaterials; i++)
			{
				this.detailScales[i] = 1.0f;
			}
		}
		
		this.reflectionAmounts = new float[numMaterials];
		if (this.fileVersion > 20)
		{
			for (int i = 0; i < numMaterials; i++)
			{
				this.reflectionAmounts[i] = dis.readFloat();
			}
		} else {
			for (int i = 0; i < numMaterials; i++)
			{
				this.reflectionAmounts[i] = 1.0f;
			}
		}
		
		if (this.fileVersion < 16)
		{
			for (int i = 0; i < numMaterials; i++)
			{
				//if (this.flags[i] & Translucent)
				//	this.flags[i] |= NeverEnvMap;
			}
		}
		
		return true;
	}
	
}
