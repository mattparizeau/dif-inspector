package com.matt.difinspector.structures;

public class Edge
{
	int vertex1, vertex2;
	int face1, face2;
	int normal1, normal2;
	
	public Edge(int vertex1, int vertex2, int face1, int face2)
	{
		this.vertex1 = vertex1;
		this.vertex2 = vertex2;
		this.face1 = face1;
		this.face2 = face2;
	}
	
	public Edge(int vertex1, int vertex2, int face1, int face2, int normal1, int normal2)
	{
		this.vertex1 = vertex1;
		this.vertex2 = vertex2;
		this.face1 = face1;
		this.face2 = face2;
		this.normal1 = normal1;
		this.normal2 = normal2;
	}
	
	public int getVertex1()
	{
		return this.vertex1;
	}
	
	public int getVertex2()
	{
		return this.vertex2;
	}
	
	public int getFace1()
	{
		return this.face1;
	}
	
	public int getFace2()
	{
		return this.face2;
	}
	
	public int getNormal1()
	{
		return this.normal1;
	}
	
	public int getNormal2()
	{
		return this.normal2;
	}
	
	@Override
	public String toString()
	{
		return "(Edge){vertex1: " + this.vertex1 + ", vertex2: " + this.vertex2 + ", face1: " + this.face1 + ", face2: " + this.face2 + ", normal1: " + this.normal1 + ", normal2: " + this.normal2 + "}";
	}
	
}
