package com.matt.difinspector.structures;

import com.matt.difinspector.math.PlaneF;
import com.matt.difinspector.math.Point2I;

public class Primitive
{
	private boolean alpha;
	private int texS, texT;
	private int diffuseIndex, lightMapIndex;
	private int start, count;
	
	private PlaneF lightMapEquationX, lightMapEquationY;
	private Point2I lightMapOffset, lightMapSize;
	
	public Primitive()
	{
		this.alpha = false;
		this.texS = -1; // GFXAddressWrap?
		this.texT = -1; // GFXAddressWrap?
		this.diffuseIndex = 0;
		this.lightMapIndex = 0;
		this.start = 0;
		this.count = 0;
		this.lightMapEquationX = new PlaneF(0, 0, 0, 0);
		this.lightMapEquationY = new PlaneF(0, 0, 0, 0);
		this.lightMapOffset = new Point2I(0, 0);
		this.lightMapSize = new Point2I(0, 0);
	}
	
	public Primitive(boolean alpha, int texS, int texT, int diffuseIndex, int lightMapIndex, int start, int count, PlaneF lightMapEquationX, PlaneF lightMapEquationY, Point2I lightMapOffset, Point2I lightMapSize)
	{
		this.alpha = alpha;
		this.texS = texS;
		this.texT = texT;
		this.diffuseIndex = diffuseIndex;
		this.lightMapIndex = lightMapIndex;
		this.start = start;
		this.count = count;
		this.lightMapEquationX = lightMapEquationX;
		this.lightMapEquationY = lightMapEquationY;
		this.lightMapOffset = lightMapOffset;
		this.lightMapSize = lightMapSize;
	}
	
	public boolean getAlpha()
	{
		return this.alpha;
	}
	
	public int getTexS()
	{
		return this.texS;
	}
	
	public int getTexT()
	{
		return this.texT;
	}
	
	public int getDiffuseIndex()
	{
		return this.diffuseIndex;
	}
	
	public int getLightMapIndex()
	{
		return this.lightMapIndex;
	}
	
	public int getStart()
	{
		return this.start;
	}
	
	public int getCount()
	{
		return this.count;
	}
	
	public PlaneF getLightMapEquationX()
	{
		return this.lightMapEquationX;
	}
	
	public PlaneF getLightMapEquationY()
	{
		return this.lightMapEquationY;
	}
	
	public Point2I getLightMapOffset()
	{
		return this.lightMapOffset;
	}
	
	public Point2I getLightMapSize()
	{
		return this.lightMapSize;
	}
	
	@Override
	public String toString()
	{
		return "(Primitive){alpha: " + this.alpha + ", texS: " + this.texS + ", texT: " + this.texT + ", diffuseIndex: " + this.diffuseIndex + ", lightMapIndex: " + this.lightMapIndex + ", start: " + this.start + ", count: " + this.count + ", lightMapEquationX: " + this.lightMapEquationX + ", lightMapEquationY: " + this.lightMapEquationY + ", lightMapOffset: " + this.lightMapOffset + ", lightMapSize: " + this.lightMapSize + "}";
	}
}
