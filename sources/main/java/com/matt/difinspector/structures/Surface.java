package com.matt.difinspector.structures;

import com.matt.difinspector.math.Point3F;

public class Surface
{
	private int windingStart;
	private short planeIndex;
	private short textureIndex;
	private int texGenIndex;
	private short lightCount;
	private byte surfaceFlags;
	private int windingCount;
	private int fanMask;
	private int lightStateInfoStart;
	private int mapOffsetX;
	private int mapOffsetY;
	private int mapSizeX;
	private int mapSizeY;
	
	private Point3F T, B, N;
	private Point3F normal;
	
	private boolean unused;
	
	private int brushId;
	
	//private boolean flipped;
	
	private String texture;
	private TexGenPlanes texGen;
	
	public Surface(int windingStart, short planeIndex, short textureIndex, int texGenIndex, short lightCount, byte surfaceFlags, int windingCount, int fanMask, int lightStateInfoStart, int mapOffsetX, int mapOffsetY, int mapSizeX, int mapSizeY, Point3F T, Point3F B, Point3F N, Point3F normal, boolean unused, int brushId)
	{
		this.windingStart = windingStart;
		this.planeIndex = planeIndex;
		this.textureIndex = textureIndex;
		this.texGenIndex = texGenIndex;
		this.lightCount = lightCount;
		this.surfaceFlags = surfaceFlags;
		this.windingCount = windingCount;
		this.fanMask = fanMask;
		this.lightStateInfoStart = lightStateInfoStart;
		this.mapOffsetX = mapOffsetX;
		this.mapOffsetY = mapOffsetY;
		this.mapSizeX = mapSizeX;
		this.mapSizeY = mapSizeY;
		this.T = T;
		this.B = B;
		this.N = N;
		this.normal = normal;
		this.unused = unused;
		this.brushId = brushId;
	}
	
	public Surface(int windingStart, short planeIndex, short textureIndex, int texGenIndex, short lightCount, byte surfaceFlags, int windingCount, int fanMask, int lightStateInfoStart, int mapOffsetX, int mapOffsetY, int mapSizeX, int mapSizeY, boolean unused, int brushId)//, boolean flipped)
	{
		this.windingStart = windingStart;
		this.planeIndex = planeIndex;
		this.textureIndex = textureIndex;
		this.texGenIndex = texGenIndex;
		this.lightCount = lightCount;
		this.surfaceFlags = surfaceFlags;
		this.windingCount = windingCount;
		this.fanMask = fanMask;
		this.lightStateInfoStart = lightStateInfoStart;
		this.mapOffsetX = mapOffsetX;
		this.mapOffsetY = mapOffsetY;
		this.mapSizeX = mapSizeX;
		this.mapSizeY = mapSizeY;
		this.T = new Point3F(0, 0, 0);
		this.B = new Point3F(0, 0, 0);
		this.N = new Point3F(0, 0, 0);
		this.normal = new Point3F(0, 0, 0);
		this.unused = unused;
		this.brushId = brushId;
		//this.flipped = flipped;
	}
	
	public Surface(int windingStart, int fanMask, short planeIndex, byte windingCount, byte surfaceFlags, boolean flipped)
	{
		this.windingStart = windingStart;
		this.fanMask = fanMask;
		this.planeIndex = planeIndex;
		this.windingCount = windingCount;
		this.surfaceFlags = surfaceFlags;
		//this.flipped = flipped;
	}
	
	public String getTexture()
	{
		return texture;
	}

	public void setTexture(String texture)
	{
		this.texture = texture;
	}

	public TexGenPlanes getTexGen()
	{
		return texGen;
	}

	public void setTexGen(TexGenPlanes texGen)
	{
		this.texGen = texGen;
	}

	public int getWindingStart()
	{
		return this.windingStart;
	}
	
	public short getPlaneIndex()
	{
		return this.planeIndex;
	}
	
	public void setTextureIndex(short index)
	{
		this.textureIndex = index;
	}
	
	public short getTextureIndex()
	{
		return this.textureIndex;
	}
	
	public void setTexGenIndex(int index)
	{
		this.texGenIndex = index;
	}
	
	public int getTexGenIndex()
	{
		return this.texGenIndex;
	}
	
	public short getLightCount()
	{
		return this.lightCount;
	}
	
	public byte getSurfaceFlags()
	{
		return this.surfaceFlags;
	}
	
	public int getWindingCount()
	{
		return this.windingCount;
	}
	
	public int getFanMask()
	{
		return this.fanMask;
	}
	
	public int getLightStateInfoStart()
	{
		return this.lightStateInfoStart;
	}
	
	public int getMapOffsetX()
	{
		return this.mapOffsetX;
	}
	
	public int getMapOffsetY()
	{
		return this.mapOffsetY;
	}
	
	public int getMapSizeX()
	{
		return this.mapSizeX;
	}
	
	public int getMapSizeY()
	{
		return this.mapSizeY;
	}
	
	public void setMapSizeX(int mapSizeX)
	{
		this.mapSizeX = mapSizeX;
	}

	public void setMapSizeY(int mapSizeY)
	{
		this.mapSizeY = mapSizeY;
	}

	public void setMapOffsetX(int mapOffsetX)
	{
		this.mapOffsetX = mapOffsetX;
	}

	public void setMapOffsetY(int mapOffsetY)
	{
		this.mapOffsetY = mapOffsetY;
	}

	public Point3F getT()
	{
		return this.T;
	}
	
	public Point3F getB()
	{
		return this.B;
	}
	
	public Point3F getN()
	{
		return this.N;
	}
	
	public Point3F getNormal()
	{
		return this.normal;
	}
	
	public boolean getUnused()
	{
		return this.unused;
	}
	
	public int getBrushId()
	{
		return this.brushId;
	}
	
	/*public void setPlaneFlipped(boolean flipped)
	{
		this.flipped = flipped;
	}*/
	
	public boolean isPlaneFlipped()
	{
		return (this.planeIndex >> 15 != 0);//this.flipped;
	}
	
	public void flip()
	{
		//this.flipped = !this.flipped;
		//if (this.flipped)
		if (this.isPlaneFlipped())
		{
			this.planeIndex &= ~0x8000;
		} else {
			this.planeIndex |= 0x8000;
		}
		
		//System.out.println(this.isPlaneFlipped() ? "Flipped" : "Normal");
		//System.out.println(this.planeIndex);
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof Surface))
			return false;
		
		Surface other = (Surface)o;
		
		boolean check = this.T == null ? other.T == null : this.T.equals(other.T);
		check = check && this.B == null ? other.B == null : this.B.equals(other.B);
		check = check && this.N == null ? other.N == null : this.N.equals(other.N);
		check = check && this.normal == null ? other.normal == null : this.normal.equals(other.normal);
		
		return this.windingStart == other.windingStart && this.planeIndex == other.planeIndex && this.textureIndex == other.textureIndex && this.texGenIndex == other.texGenIndex && this.lightCount == other.lightCount && this.surfaceFlags == other.surfaceFlags && this.windingCount == other.windingCount && this.fanMask == other.fanMask && this.lightStateInfoStart == other.lightStateInfoStart && this.mapOffsetX == other.mapOffsetX && this.mapOffsetY == other.mapOffsetY && this.mapSizeX == other.mapSizeX && this.mapSizeY == other.mapSizeY && this.unused == other.unused && this.brushId == other.brushId && check;
	}
	
	@Override
	public String toString()
	{
		return "(Surface){windingStart: " + this.windingStart + ", planeIndex: " + this.planeIndex + ", textureIndex: " + this.textureIndex + ", texGenIndex: " + this.texGenIndex + ", lightCount: " + this.lightCount + ", surfaceFlags: " + this.surfaceFlags + ", windingCount: " + this.windingCount + ", fanMask: " + this.fanMask + ", lightStateInfoStart: " + this.lightStateInfoStart + ", mapOffsetX: " + this.mapOffsetX + ", mapOffsetY: " + this.mapOffsetY + ", mapSizeX: " + this.mapSizeX + ", mapSizeY: " + this.mapSizeY + ", T: " + this.T + ", B: " + this.B + ", N: " + this.N + ", normal: " + this.normal + ", unused: " + (this.unused ? "true" : "false") + ", brushId: " + this.brushId + "}";
	}
}
