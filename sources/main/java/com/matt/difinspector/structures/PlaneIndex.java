package com.matt.difinspector.structures;

public class PlaneIndex
{
	private short index;
	private float d;
	
	public PlaneIndex(short index, float d)
	{
		this.index = index;
		this.d = d;
	}
	
	public short getIndex()
	{
		return this.index;
	}
	
	public float getD()
	{
		return this.d;
	}
}
