package com.matt.difinspector.structures;

import com.matt.difinspector.math.PlaneF;
import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.math.TriangleF;

public class Face
{
	private PlaneF plane;
	private TriangleF triangle;
	private String texture;
	private TexGenPlanes texGen;
	
	public Face(PlaneF plane, TriangleF triangle, String texture, TexGenPlanes texGen)
	{
		this.plane = plane;
		this.triangle = triangle;
		this.texture = texture;
		this.texGen = texGen;
	}
	
	public Face(Point3F p1, Point3F p2, Point3F p3, String texture, TexGenPlanes texGen)
	{
		this.plane = new PlaneF(p1, p2, p3);
		this.triangle = new TriangleF(p1, p2, p3);
		this.texture = texture;
		this.texGen = texGen;
	}

	public PlaneF getPlane()
	{
		return plane;
	}

	public void setPlane(PlaneF plane)
	{
		this.plane = plane;
	}

	public TriangleF getTriangle()
	{
		return triangle;
	}

	public void setTriangle(TriangleF triangle)
	{
		this.triangle = triangle;
	}

	public String getTexture()
	{
		return texture;
	}

	public void setTexture(String texture)
	{
		this.texture = texture;
	}

	public TexGenPlanes getTexGen()
	{
		return texGen;
	}

	public void setTexGen(TexGenPlanes texGen)
	{
		this.texGen = texGen;
	}
}
