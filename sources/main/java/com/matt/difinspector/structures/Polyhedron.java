package com.matt.difinspector.structures;

import com.matt.difinspector.math.PlaneF;
import com.matt.difinspector.math.Point3F;

public class Polyhedron
{
	private Point3F[] pointList;
	private PlaneF[] planeList;
	private Edge[] edgeList;
	
	public Polyhedron(Point3F[] pointList, PlaneF[] planeList, Edge[] edgeList)
	{
		this.pointList = pointList;
		this.planeList = planeList;
		this.edgeList = edgeList;
	}
	
	public Point3F[] getPointList()
	{
		return this.pointList;
	}
	
	public PlaneF[] getPlaneList()
	{
		return this.planeList;
	}
	
	public Edge[] getEdgeList()
	{
		return this.edgeList;
	}
}
