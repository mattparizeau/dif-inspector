package com.matt.difinspector.structures;

public class TriFan
{
	private int windingStart, windingCount;
	
	public TriFan(int windingStart, int windingCount)
	{
		this.windingStart = windingStart;
		this.windingCount = windingCount;
	}
	
	public int getWindingStart()
	{
		return this.windingStart;
	}
	
	public int getWindingCount()
	{
		return this.windingCount;
	}
	
	@Override
	public String toString()
	{
		return "(TriFan){windingStart: " + this.windingStart + ", windingCount: " + this.windingCount + "}";
	}
}
