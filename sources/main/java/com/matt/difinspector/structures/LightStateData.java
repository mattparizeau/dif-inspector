package com.matt.difinspector.structures;

public class LightStateData
{
	private int surfaceIndex;
	private int mapIndex;
	private short lightStateIndex;
	
	public LightStateData(int surfaceIndex, int mapIndex, short lightStateIndex)
	{
		this.surfaceIndex = surfaceIndex;
		this.mapIndex = mapIndex;
		this.lightStateIndex = lightStateIndex;
	}
	
	public int getSurfaceIndex()
	{
		return this.surfaceIndex;
	}
	
	public int getMapIndex()
	{
		return this.mapIndex;
	}
	
	public short getLightStateIndex()
	{
		return this.lightStateIndex;
	}
	
	@Override
	public String toString()
	{
		return "(LightStateData){surfaceIndex: " + this.surfaceIndex + ", mapIndex: " + this.mapIndex + ", lightStateIndex: " + this.lightStateIndex + "}";
	}
}
