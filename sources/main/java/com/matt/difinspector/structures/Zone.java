package com.matt.difinspector.structures;

public class Zone
{
	private short portalStart, portalCount;
	private int surfaceStart, planeStart;
	private short surfaceCount, planeCount;
	private int staticMeshStart, staticMeshCount;
	private short flags, zoneId;
	
	public Zone(short portalStart, short portalCount, int surfaceStart, int planeStart, short surfaceCount, short planeCount, int staticMeshStart, int staticMeshCount, short flags, short zoneId)
	{
		this.portalStart = portalStart;
		this.portalCount = portalCount;
		this.surfaceStart = surfaceStart;
		this.planeStart = planeStart;
		this.surfaceCount = surfaceCount;
		this.planeCount = planeCount;
		this.staticMeshStart = staticMeshStart;
		this.staticMeshCount = staticMeshCount;
		this.flags = flags;
		this.zoneId = zoneId;
	}
	
	public short getPortalStart()
	{
		return this.portalStart;
	}
	
	public short getPortalCount()
	{
		return this.portalCount;
	}
	
	public int getSurfaceStart()
	{
		return this.surfaceStart;
	}
	
	public int getPlaneStart()
	{
		return this.planeStart;
	}
	
	public short getSurfaceCount()
	{
		return this.surfaceCount;
	}
	
	public short getPlaneCount()
	{
		return this.planeCount;
	}
	
	public int getStaticMeshStart()
	{
		return this.staticMeshStart;
	}
	
	public int getStaticMeshCount()
	{
		return this.staticMeshCount;
	}
	
	public short getFlags()
	{
		return this.flags;
	}
	
	public short getZoneId()
	{
		return this.zoneId;
	}
	
	@Override
	public String toString()
	{
		return "(Zone){portalStart: " + this.portalStart + ", portalCount: " + this.portalCount + ", surfaceStart: " + this.surfaceStart + ", planeStart" + this.planeStart + ", surfaceCount: " + this.surfaceCount + ", planeCount: " + this.planeCount + ", staticMeshStart: " + this.staticMeshStart + ", staticMeshCount: " + this.staticMeshCount + ", flags: " + this.flags + ", zoneId: " + this.zoneId + "}";
	}
}
