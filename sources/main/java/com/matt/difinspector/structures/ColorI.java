package com.matt.difinspector.structures;

public class ColorI
{
	private byte red, green, blue, alpha;
	
	public ColorI(byte red, byte green, byte blue, byte alpha)
	{
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.alpha = alpha;
	}
	
	public byte getRed()
	{
		return this.red;
	}
	
	public byte getGreen()
	{
		return this.green;
	}
	
	public byte getBlue()
	{
		return this.blue;
	}
	
	public byte getAlpha()
	{
		return this.alpha;
	}
	
	@Override
	public String toString()
	{
		return "(ColorI){red: " + this.red + ", green: " + this.green + ", blue: " + this.blue + ", alpha: " + this.alpha + "}";
	}
}
