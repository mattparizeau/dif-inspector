package com.matt.difinspector.structures;

public class Portal
{
	private short planeIndex;
	private short triFanCount;
	private int triFanStart;
	private short zoneFront;
	private short zoneBack;
	
	public Portal(short planeIndex, short triFanCount, int triFanStart, short zoneFront, short zoneBack)
	{
		this.planeIndex = planeIndex;
		this.triFanCount = triFanCount;
		this.triFanStart = triFanStart;
		this.zoneFront = zoneFront;
		this.zoneBack = zoneBack;
	}
	
	public short getPlaneIndex()
	{
		return this.planeIndex;
	}
	
	public short getTriFanCount()
	{
		return this.triFanCount;
	}
	
	public int getTriFanStart()
	{
		return this.triFanStart;
	}
	
	public short getZoneFront()
	{
		return this.zoneFront;
	}
	
	public short getZoneBack()
	{
		return this.zoneBack;
	}
	
	@Override
	public String toString()
	{
		return "(Portal){planeIndex: " + this.planeIndex + ", triFanCount: " + this.triFanCount + ", triFanStart: " + this.triFanStart + ", zoneFront: " + this.zoneFront + ", zoneBack: " + this.zoneBack + "}";
	}
}
