package com.matt.difinspector.structures;

public class NullSurface
{
	private int windingStart;
	private short planeIndex;
	private byte surfaceFlags;
	private int windingCount;
	
	public NullSurface(int windingStart, short planeIndex, byte surfaceFlags, int windingCount)
	{
		this.windingStart = windingStart;
		this.planeIndex = planeIndex;
		this.surfaceFlags = surfaceFlags;
		this.windingCount = windingCount;
	}
	
	public int getWindingStart()
	{
		return this.windingStart;
	}
	
	public short getPlaneIndex()
	{
		return this.planeIndex;
	}
	
	public byte getSurfaceFlags()
	{
		return this.surfaceFlags;
	}
	
	public int getWindingCount()
	{
		return this.windingCount;
	}
	
	@Override
	public String toString()
	{
		return "(NullSurface){windingStart: " + this.windingStart + ", planeIndex: " + this.planeIndex + ", surfaceFlags: " + this.surfaceFlags + ", windingCount: " + this.windingCount + "}";
	}
}
