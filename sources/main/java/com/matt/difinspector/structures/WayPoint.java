package com.matt.difinspector.structures;

import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.math.QuatF;

public class WayPoint
{
	private Point3F pos;
	private QuatF rot;
	private int msToNext;
	private int smoothingType;
	
	public WayPoint(Point3F pos, QuatF rot, int msToNext, int smoothingType)
	{
		this.pos = pos;
		this.rot = rot;
		this.msToNext = msToNext;
		this.smoothingType = smoothingType;
	}
	
	public Point3F getPos()
	{
		return this.pos;
	}
	
	public QuatF getRot()
	{
		return this.rot;
	}
	
	public int getMsToNext()
	{
		return this.msToNext;
	}
	
	public int getSmoothingType()
	{
		return this.smoothingType;
	}
}
