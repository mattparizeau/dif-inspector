package com.matt.difinspector.structures;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;

public class ColorF
{
	public static final ColorF WHITE = new ColorF(1.0f, 1.0f, 1.0f);
	
	private float red, green, blue, alpha;
	
	public ColorF(float red, float green, float blue, float alpha)
	{
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.alpha = alpha;
	}
	
	public ColorF(float red, float green, float blue)
	{
		this(red, green, blue, 1.0f);
	}
	
	public FloatBuffer toFloatBuffer(boolean useAlpha, boolean useExtra)
	{
		int size = useExtra ? 8 : 4;
		FloatBuffer buffer = BufferUtils.createFloatBuffer(size);
		buffer.put(this.red);
		buffer.put(this.green);
		buffer.put(this.blue);
		
		if (useAlpha)
			buffer.put(this.alpha);
		
		return buffer;
	}
	
	public float getRed()
	{
		return this.red;
	}
	
	public float getGreen()
	{
		return this.green;
	}
	
	public float getBlue()
	{
		return this.blue;
	}
	
	public float getAlpha()
	{
		return this.alpha;
	}
}
