package com.matt.difinspector.structures;

import com.matt.difinspector.math.PlaneF;
import com.matt.difinspector.math.Point2F;
import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.util.Util;

public class TexGenPlanes
{
	private PlaneF planeX, planeY;
	
	public TexGenPlanes(PlaneF planeX, PlaneF planeY)
	{
		this.planeX = planeX;
		this.planeY = planeY;
	}
	
	public TexGenPlanes(Point3F normal, Point2F offset, float rotate, Point2F scale)
	{
		float xOffset = offset.getX();
		float yOffset = offset.getY();
		float xScale = scale.getX();
		float yScale = scale.getY();
		
		float[][] vecs = Util.getTextureAxis(normal);
		
		//System.out.println("(0): " + Util.getArrayString(vecs[0]));
		//System.out.println("(1): " + Util.getArrayString(vecs[1]));
		
		int sv;
		int tv;
		
		float ns;
		float nt;
		
		float angle = (float) (rotate / 180 * Math.PI);
		float sinv = (float) Math.sin(angle);
		float cosv = (float) Math.cos(angle);
		
		if (vecs[0][0] != 0.0f)
		{
			sv = 0;
		} else if (vecs[0][1] != 0.0f)
		{
			sv = 1;
		} else {
			sv = 2;
		}
		
		if (vecs[1][0] != 0.0f)
		{
			tv = 0;
		} else if (vecs[1][1] != 0.0f)
		{
			tv = 1;
		} else {
			tv = 2;
		}
		
		for (int i = 0; i < 2; i++)
		{
			ns = cosv * vecs[i][sv] - sinv * vecs[i][tv];
			nt = sinv * vecs[i][sv] + cosv * vecs[i][tv];
			vecs[i][sv] = ns;
			vecs[i][tv] = nt;
		}
		
		float ux = vecs[0][0] / xScale;
		float uy = vecs[0][1] / xScale;
		float uz = vecs[0][2] / xScale;
		float ud = xOffset;
		this.planeX = new PlaneF(ux, uy, uz, ud);
		
		float vx = vecs[1][0] / yScale;
		float vy = vecs[1][1] / yScale;
		float vz = vecs[1][2] / yScale;
		float vd = yOffset;
		this.planeY = new PlaneF(vx, vy, vz, vd);
	}
	
	/*public TexGenPlanes(Point3F normal, Point3F point, Point2F uv)
	{
		this();	
	}*/
	
	public TexGenPlanes()
	{
		this.planeX = new PlaneF(0.5f, 0.0f, 0.0f, 1.0f);
		this.planeY = new PlaneF(0.0f, 0.5f, 0.0f, 1.0f);
	}
	
	public void set(PlaneF planeX, PlaneF planeY)
	{
		this.planeX = planeX;
		this.planeY = planeY;
	}
	
	public PlaneF getPlaneX()
	{
		return this.planeX;
	}
	
	public void setPlaneX(PlaneF planeX)
	{
		this.planeX = planeX;
	}
	
	public PlaneF getPlaneY()
	{
		return this.planeY;
	}
	
	public void setPlaneY(PlaneF planeY)
	{
		this.planeY = planeY;
	}
	
	public void scaleUp(float amount)
	{
		this.planeX.scaleUp(amount);
		this.planeY.scaleUp(amount);
	}
	
	public void scaleUp(float xAmount, float yAmount)
	{
		this.planeX.scaleUp(xAmount);
		this.planeY.scaleUp(yAmount);
	}
	
	public void scaleDown(float amount)
	{
		this.planeX.scaleDown(amount);
		this.planeY.scaleDown(amount);
	}
	
	public void scaleDown(float xAmount, float yAmount)
	{
		this.planeX.scaleDown(xAmount);
		this.planeY.scaleDown(yAmount);
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof TexGenPlanes))
			return false;
		
		TexGenPlanes planes = (TexGenPlanes)o;
		
		boolean xPlane = false;
		boolean yPlane = false;
		
		if ((this.planeX == null && planes.planeX == null) || this.planeX.equals(planes.planeX))
		{
			xPlane = true;
		}
		
		if (this.planeY == null && planes.planeY == null || this.planeY.equals(planes.planeY))
		{
			yPlane = true;
		}
		
		return xPlane && yPlane;
	}
	
	@Override
	public String toString()
	{
		return "(TexGenPlanes){xPlane: " + this.planeX + ", yPlane: " + this.planeY + "}";
	}
}
