package com.matt.difinspector.structures;

public class CoordBin
{
	private int binStart, binCount;
	
	public CoordBin(int binStart, int binCount)
	{
		this.binStart = binStart;
		this.binCount = binCount;
	}
	
	public int getBinStart()
	{
		return this.binStart;
	}
	
	public int getBinCount()
	{
		return this.binCount;
	}
	
	@Override
	public String toString()
	{
		return "(CoordBin){binStart: " + this.binStart + ", binCount: " + this.binCount + "}";
	}
}
