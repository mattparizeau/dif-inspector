package com.matt.difinspector.util;

public class Dumper
{
	protected int index;
	protected String name;
	protected DumpBlock dumpedInfo;
	
	public Dumper(String name)
	{
		this.name = name;
		this.dumpedInfo = new DumpBlock("Dump[" + this.name + "]");
	}
	
	public void add(String text)
	{
		this.dumpedInfo.add(new DumpBlock(text));
	}
	
	public void add(Object[] array)
	{
		this.dumpedInfo.add(new DumpBlock(array));
	}
	
	public void add(DumpBlock dumpBlock)
	{
		this.dumpedInfo.add(dumpBlock);
	}
	
	@Override
	public String toString()
	{
		return this.dumpedInfo.toString();
	}
	
}
