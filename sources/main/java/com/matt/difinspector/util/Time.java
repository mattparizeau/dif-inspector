package com.matt.difinspector.util;

public class Time
{
	private static final long SECOND = 1000000000L;
	
	public static final double getTime()
	{
		return (double)System.nanoTime()/(double)Time.SECOND;
	}
}
