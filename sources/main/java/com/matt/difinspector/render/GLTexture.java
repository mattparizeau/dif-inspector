package com.matt.difinspector.render;

import static org.lwjgl.opengl.GL11.*;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.SGISGenerateMipmap;
import org.lwjgl.util.glu.MipMap;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import com.matt.difinspector.structures.ColorF;
import com.matt.difinspector.util.ImageData;
import com.matt.difinspector.util.Util;

public class GLTexture
{
	private String path;
	private int id;
	private int width;
	private int height;
	private float shininess;
	private ColorF specularColor;
	private ColorF diffuseColor;
	
	public GLTexture(String path)
	{
		this.path = path;
		this.id = -1;
		this.load();
		
		this.shininess = 15.0f;
		//this.shininess = 128.0f;
		this.diffuseColor = new ColorF(1.0f, 1.0f, 1.0f);
		this.specularColor = new ColorF(1.0f, 0.980392f, 0.549020f, 1.0f);
		//this.specularColor = new ColorF(1.0f, 1.0f, 1.0f, 1.0f);
	}
	
	private void load()
	{
		Texture texture = null;
		try
		{
			InputStream is = GLTexture.class.getResourceAsStream("/" + this.path + ".png");
			if (is == null)
			{
				is = GLTexture.class.getResourceAsStream("/" + this.path + ".jpg");
				if (is == null)
				{
					System.out.println("Unable to load texture: " + this.path);
					return;
				}
				texture = TextureLoader.getTexture("JPG", is);
			} else {
				texture = TextureLoader.getTexture("PNG", is);
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		
		if (texture == null)
			return;
		
		this.id = texture.getTextureID();
		this.width = texture.getImageWidth();
		this.height = texture.getImageHeight();
		
		//this.generateMipmaps(false, 4);
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		
		GL30.glGenerateMipmap(GL_TEXTURE_2D);
	}
	
	public void generateMipmaps(boolean blur, int level)
	{
		ImageData imgData = ImageData.loadImage(this.path);
		if (imgData == null)
			return;
		BufferedImage image = imgData.getImage();
		int[][] data = new int[level + 1][];
		data[0] = new int[image.getWidth() * image.getHeight()];
		image.getRGB(0, 0, image.getWidth(), image.getHeight(), data[0], 0, image.getWidth());
		
		//this.setBlurMipmap(false, level > 0);
		
		data = Util.generateMipmapData(level, this.width, data);
		Util.uploadTextureMipmap(data, this.width, this.height, 0, 0, false, false);
	}
	
	public void setBlurMipmap(boolean blur, boolean mipmap)
	{
		int min = -1;
		int mag = -1;
		
		if (blur)
		{
			min = mipmap ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR;
			mag = GL_LINEAR;
		} else {
			min = mipmap ? GL_NEAREST_MIPMAP_LINEAR : GL_NEAREST;
			mag = GL_NEAREST;
		}
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mag);
	}
	
	public void bind()
	{
		glBindTexture(GL_TEXTURE_2D, this.id);
		
		glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, this.shininess);
		glMaterial(GL_FRONT_AND_BACK, GL_SPECULAR, this.specularColor.toFloatBuffer(true, true));
		
		glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
		glColor4f(this.diffuseColor.getRed(), this.diffuseColor.getGreen(), this.diffuseColor.getBlue(), this.diffuseColor.getAlpha());
	}
	
	public void unbind()
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	public boolean isValid()
	{
		return this.id != -1;
	}
	
	public int getWidth()
	{
		return this.width;
	}
	
	public int getHeight()
	{
		return this.height;
	}
}
