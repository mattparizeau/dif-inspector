package com.matt.difinspector.render;

import static com.matt.difinspector.render.GLUtil.*;
import static org.lwjgl.opengl.GL11.*;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;

import com.matt.difinspector.math.Point2F;
import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.structures.ColorF;

public class RenderTri
{
	protected boolean selected;
	protected String texture;
	protected Point3F normal1, normal2, normal3, point1, point2, point3;
	protected Point2F uv1, uv2, uv3;
	
	protected ColorF highlightColor;
	
	public RenderTri(boolean selected, String texture, Point3F normal1, Point3F normal2, Point3F normal3, Point3F point1, Point3F point2, Point3F point3, Point2F uv1, Point2F uv2, Point2F uv3)
	{
		this.selected = selected;
		this.texture = texture;
		this.normal1 = normal1;
		this.normal2 = normal2;
		this.normal3 = normal3;
		this.point1 = point1;
		this.point2 = point2;
		this.point3 = point3;
		this.uv1 = uv1;
		this.uv2 = uv2;
		this.uv3 = uv3;
		
		this.highlightColor = new ColorF(0.0f, 0.0f, 1.0f, 0.75f);
	}
	
	public void render()
	{
		InteriorRender.bindTexture(this.texture);
		
		ColorF color = ColorF.WHITE;
		
		if (this.selected)
			color = this.highlightColor;
		
		glColor(color);
		
		glEnableClientState(GL_NORMAL_ARRAY);
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		
		FloatBuffer normals = BufferUtils.createFloatBuffer(3 * 3).put(this.normal1.toArray()).put(this.normal2.toArray()).put(this.normal3.toArray());
		normals.flip();
		
		FloatBuffer vertices = BufferUtils.createFloatBuffer(3 * 3).put(this.point1.toArray()).put(this.point2.toArray()).put(this.point3.toArray());
		vertices.flip();
		
		IntBuffer indices = BufferUtils.createIntBuffer(3 * 2).put(new int[]{0, 1, 2});
		indices.flip();
		
		FloatBuffer texCoords = BufferUtils.createFloatBuffer(2 * 3).put(this.uv1.toArray()).put(this.uv2.toArray()).put(this.uv3.toArray());
		texCoords.flip();
		
		glNormalPointer(0, normals);
		glVertexPointer(3, 0, vertices);
		glTexCoordPointer(2, 0, texCoords);
		
		glDrawElements(GL_TRIANGLES, indices);

		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);
	}

	public boolean isSelected()
	{
		return selected;
	}

	public void setSelected(boolean selected)
	{
		this.selected = selected;
	}

	public String getTexture()
	{
		return texture;
	}

	public void setTexture(String texture)
	{
		this.texture = texture;
	}

	public Point3F getNormal1()
	{
		return normal1;
	}

	public void setNormal1(Point3F normal1)
	{
		this.normal1 = normal1;
	}

	public Point3F getNormal2()
	{
		return normal2;
	}

	public void setNormal2(Point3F normal2)
	{
		this.normal2 = normal2;
	}

	public Point3F getNormal3()
	{
		return normal3;
	}

	public void setNormal3(Point3F normal3)
	{
		this.normal3 = normal3;
	}

	public Point3F getPoint1()
	{
		return point1;
	}

	public void setPoint1(Point3F point1)
	{
		this.point1 = point1;
	}

	public Point3F getPoint2()
	{
		return point2;
	}

	public void setPoint2(Point3F point2)
	{
		this.point2 = point2;
	}

	public Point3F getPoint3()
	{
		return point3;
	}

	public void setPoint3(Point3F point3)
	{
		this.point3 = point3;
	}

	public Point2F getUv1()
	{
		return uv1;
	}

	public void setUv1(Point2F uv1)
	{
		this.uv1 = uv1;
	}

	public Point2F getUv2()
	{
		return uv2;
	}

	public void setUv2(Point2F uv2)
	{
		this.uv2 = uv2;
	}

	public Point2F getUv3()
	{
		return uv3;
	}

	public void setUv3(Point2F uv3)
	{
		this.uv3 = uv3;
	}
}
