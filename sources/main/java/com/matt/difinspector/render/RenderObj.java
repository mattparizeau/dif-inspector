package com.matt.difinspector.render;

import static com.matt.difinspector.render.GLUtil.*;
import static org.lwjgl.opengl.GL11.*;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;

import com.matt.difinspector.math.Point2F;
import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.structures.ColorF;

public class RenderObj
{
	protected static final ColorF highlightColor = new ColorF(0.0f, 0.0f, 1.0f, 0.75f);
	
	protected boolean selected;
	protected String texture;
	protected Point3F[] normals;
	protected Point3F[] points;
	protected Point2F[] uvs;
	protected int[] indices;
	
	public RenderObj(boolean selected, String texture, Point3F[] normals, Point3F[] points, int[] indices, Point2F[] uvs)
	{
		this.selected = selected;
		this.texture = texture;
		this.normals = normals;
		this.points = points;
		this.uvs = uvs;
		this.indices = indices;
	}
	
	public RenderObj(String texture, RenderTri[] triangles)
	{
		this.selected = false;
		this.texture = texture;

		List<Point3F> normals = new ArrayList<Point3F>();
		List<Point3F> vertices = new ArrayList<Point3F>();
		List<Point2F> uvs = new ArrayList<Point2F>();
		List<Integer> indices = new ArrayList<Integer>();
		
		for (int i = 0; i < triangles.length; i++)
		{
			RenderTri triangle = triangles[i];
			
			normals.add(triangle.getNormal1());
			normals.add(triangle.getNormal2());
			normals.add(triangle.getNormal3());
			
			vertices.add(triangle.getPoint1());
			vertices.add(triangle.getPoint2());
			vertices.add(triangle.getPoint3());
			
			uvs.add(triangle.getUv1());
			uvs.add(triangle.getUv2());
			uvs.add(triangle.getUv3());
			
			indices.add(i+0);
			indices.add(i+1);
			indices.add(i+2);
		}
		
		this.normals = new Point3F[normals.size()];
		normals.toArray(this.normals);
		
		this.points = new Point3F[vertices.size()];
		vertices.toArray(this.points);
		
		this.uvs = new Point2F[uvs.size()];
		uvs.toArray(this.uvs);
		
		this.indices = new int[indices.size()];
		for (int i = 0; i < this.indices.length; i++)
		{
			this.indices[i] = indices.get(i);
		}
	}
	
	public RenderObj(String texture, List<RenderTri> triangles)
	{
		this.selected = false;
		this.texture = texture;

		List<Point3F> normals = new ArrayList<Point3F>();
		List<Point3F> vertices = new ArrayList<Point3F>();
		List<Point2F> uvs = new ArrayList<Point2F>();
		List<Integer> indices = new ArrayList<Integer>();
		
		for (int i = 0; i < triangles.size(); i++)
		{
			RenderTri triangle = triangles.get(i);
			
			normals.add(triangle.getNormal1());
			normals.add(triangle.getNormal2());
			normals.add(triangle.getNormal3());
			
			vertices.add(triangle.getPoint1());
			vertices.add(triangle.getPoint2());
			vertices.add(triangle.getPoint3());
			
			uvs.add(triangle.getUv1());
			uvs.add(triangle.getUv2());
			uvs.add(triangle.getUv3());
			
			indices.add(i+0);
			indices.add(i+1);
			indices.add(i+2);
		}
		
		this.normals = new Point3F[normals.size()];
		normals.toArray(this.normals);
		
		this.points = new Point3F[vertices.size()];
		vertices.toArray(this.points);
		
		this.uvs = new Point2F[uvs.size()];
		uvs.toArray(this.uvs);
		
		this.indices = new int[indices.size()];
		for (int i = 0; i < this.indices.length; i++)
		{
			this.indices[i] = indices.get(i);
		}
	}
	
	public void render()
	{
		if (this.selected)
			glColor(RenderObj.highlightColor);
		else
			glColor(ColorF.WHITE);
		InteriorRender.bindTexture(this.texture);
		
		glEnableClientState(GL_NORMAL_ARRAY);
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		
		FloatBuffer normals = BufferUtils.createFloatBuffer(this.normals.length * 3);
		for (Point3F normal : this.normals)
			normals.put(normal.toArray());
		normals.flip();
		
		FloatBuffer vertices = BufferUtils.createFloatBuffer(this.points.length * 3);
		for (Point3F point : this.points)
			vertices.put(point.toArray());
		vertices.flip();
		
		IntBuffer indices = BufferUtils.createIntBuffer(this.indices.length).put(this.indices);
		indices.flip();
		
		FloatBuffer texCoords = BufferUtils.createFloatBuffer(this.uvs.length * 2);
		for (Point2F uv : this.uvs)
			texCoords.put(uv.toArray());
		texCoords.flip();
		
		glNormalPointer(0, normals);
		glVertexPointer(3, 0, vertices);
		glTexCoordPointer(2, 0, texCoords);
		
		glDrawElements(GL_TRIANGLES, indices);
		
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);
	}

	public boolean isSelected()
	{
		return selected;
	}

	public void setSelected(boolean selected)
	{
		this.selected = selected;
	}

	public String getTexture()
	{
		return texture;
	}

	public void setTexture(String texture)
	{
		this.texture = texture;
	}

	public Point3F[] getNormals()
	{
		return normals;
	}

	public void setNormals(Point3F[] normals)
	{
		this.normals = normals;
	}

	public Point3F[] getPoints()
	{
		return points;
	}

	public void setPoints(Point3F[] points)
	{
		this.points = points;
	}

	public Point2F[] getUvs()
	{
		return uvs;
	}

	public void setUvs(Point2F[] uvs)
	{
		this.uvs = uvs;
	}
}
