package com.matt.difinspector.render;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

public class GLWindow
{
	protected String title;
	protected int width, height;
	
	public GLWindow(String title, int width, int height)
	{
		this.title = title;
		this.width = width;
		this.height = height;
	}
	
	public void create()
	{
		try
		{
			Display.setDisplayMode(new DisplayMode(this.width, this.height));
			Display.create();
			Keyboard.create();
			Mouse.create();
		} catch (LWJGLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void update()
	{
		Display.update();
	}
	
	public void dispose()
	{
		Mouse.destroy();
		Keyboard.destroy();
		Display.destroy();
	}
	
	public String getTitle()
	{
		return this.title;
	}
	
	public int getPreferredWidth()
	{
		return this.width;
	}
	
	public int getPreferredHeight()
	{
		return this.height;
	}
	
	public int getWidth()
	{
		return Display.getWidth();
	}
	
	public int getHeight()
	{
		return Display.getHeight();
	}
	
	public boolean shouldClose()
	{
		return Display.isCloseRequested();
	}
}
