package com.matt.difinspector.render;

import org.lwjgl.input.Keyboard;

import com.matt.difinspector.main.DifInspector;
import com.matt.difinspector.math.Point2F;
import com.matt.difinspector.math.Point3F;

public class Camera extends SceneObject
{
	//protected MatrixF projection;
	
	protected float sensitivity;
	
	public Camera()//MatrixF projection)
	{
		//this.projection = projection;
		this.sensitivity = 0.5f;
	}
	
	@Override
	public void update()
	{
		this.transform.update();
		float moveAmount = 1f;
		
		if (Input.getKeyHeld(Keyboard.KEY_W))
		{
			//this.transform.setPosition(this.transform.getPosition().add(new Point3F(0, 1, 0)));
			this.move(this.transform.getRotation().getForward(), -moveAmount);//new Point3F(0, 1, 0));
		} else if (Input.getKeyHeld(Keyboard.KEY_S))
		{
			this.move(this.transform.getRotation().getBack(), -moveAmount);//new Point3F(0, 1, 0));
			//this.transform.setPosition(this.transform.getPosition().add(new Point3F(0, -1, 0)));
		}
		
		if (Input.getKeyHeld(Keyboard.KEY_A))
		{
			//this.move(new Point3F(-1, 0, 0));
			this.move(this.transform.getRotation().getLeft(), moveAmount);//new Point3F(0, 1, 0));
			//this.transform.setPosition(this.transform.getPosition().add(new Point3F(-1, 0, 0)));
		} else if (Input.getKeyHeld(Keyboard.KEY_D))
		{
			//this.move(new Point3F(1, 0, 0));
			this.move(this.transform.getRotation().getRight(), moveAmount);//new Point3F(0, 1, 0));
			//this.transform.setPosition(this.transform.getPosition().add(new Point3F(1, 0, 0)));
		}
		
		/*if (Input.getKeyHeld(Keyboard.KEY_SPACE))
		{
			this.move(new Point3F(0, 0, 1));
			//this.transform.setPosition(this.transform.getPosition().add(new Point3F(-1, 0, 0)));
		} else if (Input.getKeyHeld(Keyboard.KEY_LSHIFT))
		{
			this.move(new Point3F(0, 0, -1));
			//this.transform.setPosition(this.transform.getPosition().add(new Point3F(1, 0, 0)));
		}*/
		
		/*if (Input.getKeyHeld(Keyboard.KEY_UP))
		{
			this.transform.rotate(new Point3F(-1, 0, 0), (float)Math.toRadians(10));//.setRotation(this.transform.getRotation().add(new QuatF(new Point3F(-1, 0, 0))));
		} else if (Input.getKeyHeld(Keyboard.KEY_DOWN))
		{
			this.transform.rotate(new Point3F(1, 0, 0), (float)Math.toRadians(10));
			//this.transform.setRotation(this.transform.getRotation().add(new QuatF(new Point3F(1, 0, 0))));
		}
		
		if (Input.getKeyHeld(Keyboard.KEY_LEFT))
		{
			this.transform.rotate(new Point3F(0, 0, -1), (float)Math.toRadians(10));
			//this.transform.setRotation(this.transform.getRotation().add(new QuatF(new Point3F(0, 0, -1))));
		} else if (Input.getKeyHeld(Keyboard.KEY_RIGHT))
		{
			this.transform.rotate(new Point3F(0, 0, 1), (float)Math.toRadians(10));
			//this.transform.setRotation(this.transform.getRotation().add(new QuatF(new Point3F(0, 0, 1))));
		}*/
		
		GLRender render = DifInspector.getInstance().getRender();
		GLWindow window = render.getWindow();
		
		Point2F centerPosition = new Point2F(window.getWidth() / 2, window.getHeight() / 2);
		
		if (Input.getMouseDown(1))
		{
			Input.setCursor(false);
			Input.setMousePosition(centerPosition);
		} else if (Input.getMouseUp(1))
		{
			Input.setCursor(true);
		}
		
		if (Input.getMouseHeld(1))
		{
			Point2F deltaPos = Input.getMousePosition().sub(centerPosition);
			
			boolean rotY = deltaPos.getX() != 0;
			boolean rotX = deltaPos.getY() != 0;
			
			if (rotY)
				this.transform.rotate(new Point3F(0, 0, 1), (float)Math.toRadians(-deltaPos.getX() * sensitivity));
			if (rotX)
				this.transform.rotate(this.transform.getRotation().getRight(), (float)Math.toRadians(deltaPos.getY() * sensitivity));
			
			/*if (rotY || rotX)
			{
				Point3F euler = new AngAxisF(this.transform.getRotation()).toEuler();
				
				if (euler.getX() < 0)
					euler.setX(0);
				if (euler.getX() > 180)
					euler.setX(180);
				
				this.transform.setRotation(new QuatF(euler.toRadians()));
				
				System.out.println("Euler: " + euler);
			}*/
			
			//Point3F euler = this.transform.getRotation().toEuler().toDegrees();
			//System.out.println(euler);
			//euler = euler.toRadians();
			/*if (euler.getX() < (float)Math.toRadians(0))
				euler.setX((float)Math.toRadians(0));
			if (euler.getX() > (float)Math.toRadians(180))
				euler.setX((float)(float)Math.toRadians(180));*/
			
			//this.transform.setRotation(new QuatF(euler));
			
			if (rotY || rotX)
				Input.setMousePosition(centerPosition);
		}
	}
	
	/*public MatrixF getViewProjection()
	{
		MatrixF cameraRotation = this.transform.getTransformedRotation().conjugate().toRotationMatrix();
		Point3F cameraPos = this.transform.getTransformedPosition().mul(-1);
		
		MatrixF cameraTranslation = new MatrixF().initTranslation(cameraPos.getX(), cameraPos.getY(), cameraPos.getZ());
		
		return projection.mul(cameraRotation.mul(cameraTranslation));
	}*/
	
	protected void move(Point3F direction, float amount)
	{
		//MatrixF mat = this.getTransform().getRotation().toRotationMatrix();
		//Point3F p = mat.transform(move);
		//this.transform.setPosition(this.transform.getPosition().add(p));
		
		//Point3F forward = this.transform.getRotation().getForward();
		
		//Point3F moveDir = forward.mul(move);
		
		//this.transform.setPosition(this.transform.getPosition().add(moveDir));
		
		this.transform.setPosition(this.transform.getPosition().add(direction.mul(amount)));
	}

	@Override
	public void render()
	{
		
	}
}
