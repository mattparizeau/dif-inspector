package com.matt.difinspector.render;

import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.structures.ColorF;

import static org.lwjgl.opengl.GL11.*;
import static com.matt.difinspector.render.GLUtil.*;

public class Cube
{
	protected Point3F pos;
	protected Point3F scale;
	protected ColorF color;
	
	public Cube(Point3F pos, Point3F scale, ColorF color)
	{
		this.pos = pos;
		this.scale = scale;
		this.color = color;
	}
	
	public void render()
	{
		glColor(this.color);
		
		Point3F min = this.scale.mul(-1).add(this.pos);
		Point3F max = this.scale.add(this.pos);
		
		// Top
		Point3F p1 = new Point3F(min.getX(), min.getY(), max.getZ());
		Point3F p2 = new Point3F(max.getX(), min.getY(), max.getZ());
		Point3F p3 = new Point3F(max.getX(), max.getY(), max.getZ());
		Point3F p4 = new Point3F(min.getX(), max.getY(), max.getZ());
		
		// Bottom
		Point3F p5 = new Point3F(min.getX(), max.getY(), min.getZ());
		Point3F p6 = new Point3F(max.getX(), max.getY(), min.getZ());
		Point3F p7 = new Point3F(max.getX(), min.getY(), min.getZ());
		Point3F p8 = new Point3F(min.getX(), min.getY(), min.getZ());
		
		// Front
		Point3F p9 = new Point3F(min.getX(), min.getY(), min.getZ());
		Point3F p10 = new Point3F(max.getX(), min.getY(), min.getZ());
		Point3F p11 = new Point3F(max.getX(), min.getY(), max.getZ());
		Point3F p12 = new Point3F(min.getX(), min.getY(), max.getZ());

		// Back
		Point3F p13 = new Point3F(min.getX(), max.getY(), max.getZ());
		Point3F p14 = new Point3F(max.getX(), max.getY(), max.getZ());
		Point3F p15 = new Point3F(max.getX(), max.getY(), min.getZ());
		Point3F p16 = new Point3F(min.getX(), max.getY(), min.getZ());

		// Left
		Point3F p17 = new Point3F(min.getX(), min.getY(), max.getZ());
		Point3F p18 = new Point3F(min.getX(), max.getY(), max.getZ());
		Point3F p19 = new Point3F(min.getX(), max.getY(), min.getZ());
		Point3F p20 = new Point3F(min.getX(), min.getY(), min.getZ());
		
		// Right
		Point3F p21 = new Point3F(max.getX(), min.getY(), min.getZ());
		Point3F p22 = new Point3F(max.getX(), max.getY(), min.getZ());
		Point3F p23 = new Point3F(max.getX(), max.getY(), max.getZ());
		Point3F p24 = new Point3F(max.getX(), min.getY(), max.getZ());
		
		glBegin(GL_QUADS);
		{
			// Top
			glVertex(p1);
			glVertex(p2);
			glVertex(p3);
			glVertex(p4);
			
			// Bottom
			glVertex(p5);
			glVertex(p6);
			glVertex(p7);
			glVertex(p8);
			
			// Front
			glVertex(p9);
			glVertex(p10);
			glVertex(p11);
			glVertex(p12);

			// Back
			glVertex(p13);
			glVertex(p14);
			glVertex(p15);
			glVertex(p16);
			
			// Left
			glVertex(p17);
			glVertex(p18);
			glVertex(p19);
			glVertex(p20);
			
			// Right
			glVertex(p21);
			glVertex(p22);
			glVertex(p23);
			glVertex(p24);
		}
		glEnd();
	}
}
