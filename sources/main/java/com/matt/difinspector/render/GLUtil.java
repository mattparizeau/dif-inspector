package com.matt.difinspector.render;

import static org.lwjgl.opengl.GL11.*;

import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.structures.ColorF;

public final class GLUtil
{
	public static final void glVertex(Point3F point)
	{
		glVertex3f(point.getX(), point.getY(), point.getZ());
	}
	
	public static final void glColor(ColorF color)
	{
		glColor4f(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
	}
}
