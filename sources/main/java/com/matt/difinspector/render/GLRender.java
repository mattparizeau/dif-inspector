package com.matt.difinspector.render;

import static org.lwjgl.opengl.GL11.*;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBShadow;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL40;
import org.lwjgl.opengl.GL43;
import org.lwjgl.opengl.GL44;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.opengl.NVFogDistance;
import org.lwjgl.util.glu.GLU;

import com.matt.difinspector.interior.InteriorResource;
import com.matt.difinspector.math.AngAxisF;
import com.matt.difinspector.math.QuatF;
import com.matt.difinspector.structures.ColorF;
import com.matt.difinspector.util.Time;

public class GLRender
{
	private GLWindow window;
	private boolean running;
	
	private InteriorRender interiorRender;
	
	//private boolean highlight;
	//private boolean highlightHulls;
	//private boolean highlightSurfaces;
	//private EnumSelectionMode selectionMode;
	private int highlightIndex;
	private boolean autoRotate;
	
	private double frameTime;
	
	private int frameRate;
	private int updateRate;
	
	private Camera camera;
	
	public GLRender(double frameRate, int width, int height)
	{
		this.window = new GLWindow("Preview", width, height);
		this.autoRotate = false;
		
		this.camera = new Camera();//new MatrixF().initPerspective((float)Math.toRadians(90.0f), (float) this.window.getWidth() / (float) this.window.getHeight(), 0.01f, 1000.0f));
		this.camera.getTransform().getPosition().set(0, 0, 40);
		this.camera.getTransform().setRotation(new QuatF(new AngAxisF(1, 0, 0, 45)));
		//this.camera.setTransform(new Transform(new Point3F(0, 0, 40), new Point3F(-60, 0, 0)));
		
		this.frameTime = 1.0/frameRate;
		
		//this.selectionMode = EnumSelectionMode.NONE;
	}
	
	public void start()
	{
		if (this.running)
			return;
				
		this.window.create();
		
		this.init();
		
		this.run();
	}
	
	private void init()
	{
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		GLU.gluPerspective(45.0f, this.window.getWidth() / this.window.getHeight(), 0.001f, 1000000.0f);//0.001f, 1000.0f);
		glMatrixMode(GL_MODELVIEW);
		//System.out.printf("Remaining: %d\n", lightAmbient.position());
		
		// Enable Shading
		glEnable(GL_SHADE_MODEL);
		glShadeModel(GL_SMOOTH);
		
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		
		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_LIGHTING);
		glEnable(GL_TEXTURE_2D);
		
		//Hide Invisible Faces
		glCullFace(GL_BACK);
		glEnable(GL_CULL_FACE);	
		//glFrontFace(GL_CW);
		
		// Init Color Materials
		glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
		glEnable(GL_COLOR_MATERIAL);
		
		glEnable(GL_FOG);
		//glFogi(GL_FOG_MODE, ?);
		FloatBuffer fogColor = new ColorF(0.82f, 0.98f, 1.0f).toFloatBuffer(true, true);
		fogColor.flip();
		
		glFog(GL_FOG_COLOR, fogColor);
		glFogf(GL_FOG_MODE, GL_LINEAR);
		glFogf(GL_FOG_DENSITY, 2.0f);
		glFogf(GL_FOG_START, 0.0f);
		glFogf(GL_FOG_END, 500.0f);
		
		//glFogi(GL11.GL_FOG)
		if (GLContext.getCapabilities().GL_NV_fog_distance)
		{
			glFogi(NVFogDistance.GL_FOG_DISTANCE_MODE_NV, NVFogDistance.GL_EYE_RADIAL_NV);
		}
		
		//if (GLContext.getCapabilities().GL_ARB_shadow)
		//{
			//org.lwjgl.opengl.ARBShadow.
			//glShadeModel(ARBShadow.GL_TEXTURE_COMPARE_MODE_ARB);
			//glEnable(GL_DEPTH_FUNC);
			//glDepthFunc(func);
		//}
		
		// Setup Clearing
		glClearColor(0, 0, 0, 0);
		glClearStencil(0);
		glClearDepth(1.0f);
		glDepthFunc(GL_LEQUAL);
		
		// Lighting
		
		FloatBuffer lightAmbient = BufferUtils.createFloatBuffer(8).put(new float[]{0.2f, 0.2f, 0.2f, 1.0f});
		lightAmbient.flip();
		FloatBuffer lightDiffuse = BufferUtils.createFloatBuffer(8).put(new float[]{0.7f, 0.7f, 0.7f, 1.0f});
		lightDiffuse.flip();
		FloatBuffer lightSpecular = BufferUtils.createFloatBuffer(8).put(new float[]{1.0f, 1.0f, 1.0f, 1.0f});
		lightSpecular.flip();
		FloatBuffer lightPos = BufferUtils.createFloatBuffer(8).put(new float[]{0.0f, 0.0f, 20.0f, 1.0f});
		lightPos.flip();
		
		glLight(GL_LIGHT0, GL_AMBIENT, lightAmbient);
		glLight(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
		glLight(GL_LIGHT0, GL_SPECULAR, lightSpecular);
		
		glLight(GL_LIGHT0, GL_POSITION, lightPos);
		
		glEnable(GL_LIGHT0);
		
		// Transparency
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	
	public void run()
	{
		this.running = true;

		int updates = 0;
		int frames = 0;
		double frameCounter = 0;
		
		double lastTime = Time.getTime();
		double unprocessedTime = 0;
		
		while (this.running)
		{
			double startTime = Time.getTime();
			double passedTime = startTime - lastTime;
			lastTime = startTime;
			
			unprocessedTime += passedTime;
			frameCounter += passedTime;
			
			while (unprocessedTime > frameTime)
			{
				unprocessedTime -= frameTime;
				if (this.window.shouldClose())
				{
					this.stop();
					this.cleanup();
					return;
				}
				this.update();
				updates++;
				Input.update();
				
				if (frameCounter >= 1.0)
				{
					this.frameRate = frames;
					this.updateRate = updates;
					//System.out.println("UPS: " + updates + " | FPS: " + frames);
					frameCounter = 0;
					updates = 0;
					frames = 0;
				}
			}
			
			this.render();
			frames++;
			this.window.update();
		}
		this.stop();
		this.cleanup();
	}
	
	float rot = 0;
	
	public void update()
	{
		if (this.interiorRender != null)
			this.interiorRender.update();
		
		this.camera.update();
		
		if (autoRotate)
			rot += 1.0f;
		else
			rot = 0.0f;
		
		if (Input.getMouseDown(0))
		{
			//DifInspector.getInstance().editSurface();
			System.out.println("X: " + Input.getMouseX() + ", Y: " + Input.getMouseY());
		}
	}
	
	public void render()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();
		
		if (autoRotate)
		{
			glTranslatef(-this.camera.getTransform().getPosition().getX(), -this.camera.getTransform().getPosition().getY(), -this.camera.getTransform().getPosition().getZ());
			//glTranslatef(0, 0, -40);
			glRotatef(60, -1, 0, 0);
			glRotatef(rot, 0, 0, 1);
			//glTranslatef(-this.camera.getTransform().getPosition().getX(), -this.camera.getTransform().getPosition().getY(), -this.camera.getTransform().getPosition().getZ());
		} else {
			//glRotatef(this.camera.getTransform().getRotation().getX(), 1, 0, 0);
			//glRotatef(this.camera.getTransform().getRotation().getY(), 0, 1, 0);
			//glRotatef(this.camera.getTransform().getRotation().getZ(), 0, 0, 1);
			QuatF rotation = this.camera.getTransform().getRotation();
			AngAxisF angAxis = new AngAxisF(rotation);
			
			glRotatef(-angAxis.getAngle(), angAxis.getAxis().getX(), angAxis.getAxis().getY(), angAxis.getAxis().getZ());
			glTranslatef(-this.camera.getTransform().getPosition().getX(), -this.camera.getTransform().getPosition().getY(), -this.camera.getTransform().getPosition().getZ());
		}
		
		if (this.interiorRender != null)
			this.interiorRender.render();
	}
	
	public Camera getCamera()
	{
		return this.camera;
	}
	
	public void setInterior(InteriorResource interior)
	{
		this.interiorRender = new InteriorRender(interior);
		
		this.interiorRender.setSelection(EnumSelectionMode.NONE, this.highlightIndex);
	}
	
	/*public void setHighlight(boolean highlight, boolean highlightHulls, boolean highlightSurfaces, int highlightIndex)
	{
		this.highlight = highlight;
		this.highlightHulls = highlightHulls;
		this.highlightSurfaces = highlightSurfaces;
		this.highlightIndex = highlightIndex;
		
		if (this.interiorRender != null)
			this.interiorRender.setHighlight(this.highlight, this.highlightHulls, this.highlightSurfaces, this.highlightIndex);
	}*/
	
	/*public void setHighlight(boolean highlight)
	{
		this.highlight = highlight;
		
		if (this.interiorRender != null)
			this.interiorRender.setHighlight(this.highlight, this.highlightHulls, this.highlightSurfaces, this.highlightIndex);
	}*/
	
	public void setSelected(EnumSelectionMode mode, int index)
	{
		//this.selectionMode = mode;
		this.highlightIndex = index;
		
		if (this.interiorRender != null)
			this.interiorRender.setSelection(mode, index);
			//this.interiorRender.setHighlight(this.highlight, this.highlightHulls, this.highlightSurfaces, this.highlightIndex);
	}
	
	public void setAutoRotate(boolean flag)
	{
		this.autoRotate = flag;
	}
	
	public void stop()
	{
		if (!this.running)
			return;
		
		this.running = false;
	}
	
	public void cleanup()
	{
		this.window.dispose();
	}
	
	public int getFPS()
	{
		return this.frameRate;
	}
	
	public int getUPS()
	{
		return this.updateRate;
	}
	
	public GLWindow getWindow()
	{
		return this.window;
	}
}
